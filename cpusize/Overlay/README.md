##  About this directory

Working directory is `/group/ilc/users/miyamoto/mcprod/180620/Overlay`.

This directory contains tools to run test jobs at KEKCC,
to estimate CPU time and data size for production.

## How to use

- Prepare a list of input generator files in `stdhep_list` directory.
- Generator meta data should be prepared in `genmetaByID.json`, which stored
  the meta data as a python dictionary object. `process_id` is used as a key.
  An example to see a content of this file is
```
	jsonread genmetaByID.json [process_id]
```
  
- Initialize ilcsoft-02-00-01 environment
  Edit `version.py` and set version numbers, path to the ilcsoft, etc.
  Note that `version.py` is symbolic linked to `version-02-00-01.py`. This should be modified in future release.

- Using `make_rundb.py`, create `runcond.json`, which contains 
  the information such as the local path to the background files,  IP offset and smearing, which are 
  used by the jobs which will submitted by `runsimrec.py`

## Run ddsim and marlin

An example to submit jobs for ddsim and marlin is,

```
    ./runsimrec.py --dolist stdhep-list/1-calib.uds.list --outdir jobs-uds 2>&1 | tee bsub-uds.log
```

If `calib` exists in the name of input generator file, it is considered as 
a calibration file and no background overlay and no crossing angle.
Otherwise  backgrounds are overlaid and crossing angle is conisdered.
Background files should be present under a directory, `bkgfiles`.

  
```
    usage: runsimrec.py [-h] [--dofile AFILE] [--dolist ALIST] [--outdir OUTDIR]
                    [--numevents NUMEVENTS] [--dry]

    Do submit and run jobs to estimate CPU time and file size
    Examples:
      ./runsimrec.py --dolist stdhep_lists/500-TDR_ws.1f.list --outdir jobs-1f 2>&1 | tee bsub-1f.log
      ./runsimrec.py --dofile sample_generator_file.stdhep --outdir jobs-test
    If --dry is not given, submit job immedieately.
    Note:
      500 GeV IP position smearing and overlay of aa_lowpt and selected_pairs included.

    optional arguments:
      -h, --help            show this help message and exit
      --dofile AFILE        Just do one stdhep file of ddsim and marlin
      --dolist ALIST        Sub many jobs for many files.
      --outdir OUTDIR       Top level job exec directory.
      --numevents NUMEVENTS
                            number of events of each jobs.
      --dry                 just create a bsub script.
```

A file, `suball.sh`, has been used to submit many jobs using a list in `stdhep_list` directory.


## Create a data base summarzing CPU time and data size

Do `./cpusize.py`.  This command scans directory with names, `jobs-*`, and create 
the summary table in `cpusize.json`.

Do `cd ../ProdPara` to create a parameter file for production.


