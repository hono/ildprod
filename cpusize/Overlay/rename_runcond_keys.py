#!/bin/env python 

import json

if __name__ == "__main__":
    jdict = json.load(open("runcond.json"))

    odict = {}
    orig = "v01-19-05-p01"
    replace = "head"

    for k,v in jdict.items():
       knew = k.replace(orig, replace)
       odict[knew] = v

    json.dump(odict, open("newcond.json","w"))


