#!/bin/bash 
#
#  Create scripts for background files.
#

prodname="examples"
proddir=${PRODTASKDIR}/${prodname}

if [ ! -e ${proddir} ] ; then 
  mkdir -v ${proddir} || my_abort "Failed to create directory" 
fi


# ###########################################################
# Format of xxx-list.txt file.
# <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
#############################################################
testname="devel4"
cat > ${prodname}-list.txt <<EOF
2f_Z_hadronic.bWW:_${testname}
EOF

# ( cd ${proddir} && rm -rfv *.bWW.[01]lep_${testname} )

# export GENSPLIT_DEFAULT_NPROCS=5

excel_file=/group/ilc/users/miyamoto/mcprod/210914-cpusize-500GeV/cpusize/ProdPara/500-2f4f.xlsx

cmd="init_production.py --workdir ${proddir} \
  --excel_file ${excel_file} \
  --prodlist ${prodname}-list.txt \
  --gensplit_numprocs 5 \
  --dstonly \
  --test \
  --noelog \
  --mcprod_config mcprod-ilcsoft-v02-02-03.py \
  --se_for_data KEK-DISK \
  --se_for_gensplit KEK-DISK \
  --se_for_logfiles KEK-DISK \
  --se_for_dstm_replicates DESY-SRM \
  --nw_perfile 100 \
  --ngenfile_max 2 \
  --split_nbfiles 4 \
  --sim_nbtasks 5 --rec_nbtasks 5 \
  --production_type sim:nobg " 

echo ${cmd}

echo " "
${cmd} \
  || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
  && mv -v ${prodname}-list.txt ${proddir}

################################################################################
# init_production.py help # See init_production.help
################################################################################
