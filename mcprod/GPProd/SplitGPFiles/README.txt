A directory to create splitted GuineaPig file and upload to dirac.

How to do 

(1) Edit parameter in gp-split.sh, then do 
  gp-split.sh | tee gp-split.log
  
  Splitted files are written to the separate directory

(2) Run gp-get-nevents.sh to create a file, evsum.txt.
  This file is used by mk_input_genfiles.sh to create input_genfiles.sh
  which is used when submitting the User jobs.

(3) Upload gensplitted files to Dirac. This can be done in ul_meta directory.

