#!/bin/bash 
#
#  Create scripts for background files.
#
# ###########################################################
# Create production scripts
# Usage:
#   create_scripts <procname> <processID> <norder> <nseq_from> <ngenfile> <simse> 
# ###########################################################
create_scripts(){
  procname=$1
  processID=$2
  norder=$3
  nseq_from=$4
  ngenfile=$5
  simse_now=$6

  prodname="aa_2f_h"
  proddir=${PRODTASKDIR}/${prodname}

  if [ ! -e ${proddir} ] ; then 
    mkdir -pv ${proddir} || my_abort "Failed to create directory" 
  fi
  # ###########################################################
  # Format of xxx-list.txt file.
  # <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
  #############################################################
cat > ${prodname}-list.txt <<EOF
${procname}:_I${processID}_${norder}/I${processID}
EOF

  excel_file="prodpara/250-SetA-aa_2f.xlsx"
  banned_sites=`joinlines banned_sites.txt`
  export GENSPLIT_DEFAULT_NPROCS=8
  options=" --nodry --noPrompt"
  serial=$((${processID}*1000+${norder}))
  
  cmd="init_production.py --workdir ${proddir} \
    --xml_serial ${serial} \
    --excel_file  ${excel_file} \
    --prodlist ${prodname}-list.txt \
    --dstonly --recrate -1 \
    --split_nseq_from ${nseq_from} --ngenfile_max ${ngenfile} \
    --delfiles sim --se_for_sim ${simse_now} \
    --sim_banned_sites ${banned_sites} --rec_banned_sites ${banned_sites} \
    --sim_nbtasks 1000 --rec_nbtasks 1000 \
    --production_type sim:ovl \
    --step4_options ${options} "
  
  echo ${cmd}
  
  echo " "
  ${cmd} \
    || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
    && mv -v ${prodname}-list.txt ${proddir}
  
}

###########################################################
# main part of this script
###########################################################
#  2f_I500006 and 2f_I500008 Large cross section processes.
#
#   create_scripts <procname> <processID> <norder> <nseq_from> <ngenfile> <simse> 
#
########################################################################################
###############################################################################
# Maximum nb of jobs per transformation would around 25000
# process_type  pol     process_id      nbfile_genuse   nbjobs  # of trans      # of f/t        # of job/trans
# aa_2f_Z_leptonic  eB.pW  I500054  617  123,400  10  62  12,340
# aa_2f_Z_leptonic  eW.pB  I500055  529  123,433  10  53  12,343
# aa_2f_Z_leptonic  eW.pW  I500056  446  133,800  10  45  13,380
# aa_2f_Z_hadronic  eB.pW  I500058  452  60,267  10  45  6,027
# aa_2f_Z_hadronic  eW.pB  I500059  451  60,133  10  45  6,013
# aa_2f_Z_hadronic  eW.pW  I500060  358  71,600  10  36  7,160
# #######################################################################################
#
# aa_2f_Z_hadronic  eB.pW  I500058  452 (0-451)  60,267  10  45  6,027
#
nseq_from0=45
num_files=45
for i in `seq 2 11` ; do
   simse="KEK-DISK"
   n0=$[${nseq_from0}+(${i}-2)*${num_files}]
   num=${num_files}
   [ $[${n0}+${num_files}-1] -gt 451 ] && num=$[451-${n0}+1]
   echo "create_scripts aa_2f_Z_hadronic.bBW 500058 ${i} ${n0} ${num} ${simse}"
   create_scripts aa_2f_Z_hadronic.bBW 500058 ${i} ${n0} ${num} ${simse}
done

# #######################################################################################
#
# aa_2f_Z_hadronic  eW.pB  I500059  451 (0-450)  60,133  10  45  6,013
#
nseq_from0=45
num_files=45
for i in `seq 2 11` ; do
   simse="KEK-DISK"
   n0=$[${nseq_from0}+(${i}-2)*${num_files}]
   num=${num_files}
   [ $[${n0}+${num_files}-1] -gt 450 ] && num=$[450-${n0}+1]
   echo "create_scripts aa_2f_Z_hadronic.bWB 500059 ${i} ${n0} ${num} ${simse}"
   create_scripts aa_2f_Z_hadronic.bWB 500059 ${i} ${n0} ${num} ${simse}
done

# #######################################################################################
#
# aa_2f_Z_hadronic  eW.pW  I500060  358 (0-357)  71,600  10  36  7,160
#
nseq_from0=36
num_files=36
for i in `seq 2 10` ; do
   simse="KEK-DISK"
   n0=$[${nseq_from0}+(${i}-2)*${num_files}]
   num=${num_files}
   [ $[${n0}+${num_files}-1] -gt 357 ] && num=$[357-${n0}+1]
   echo "create_scripts aa_2f_Z_leptonic.bWW 500060 ${i} ${n0} ${num} ${simse}"
   create_scripts aa_2f_Z_hadronic.bWW 500060 ${i} ${n0} ${num} ${simse}
done
