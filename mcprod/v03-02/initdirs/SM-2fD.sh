#!/bin/bash 
#
#  Create scripts for background files.
#
# ###########################################################
# Create production scripts
# Usage:
#   create_scripts <procname> <processID> <norder> <nseq_from> <ngenfile> <simse> 
# ###########################################################
create_scripts(){
  procname=$1
  processID=$2
  norder=$3
  nseq_from=$4
  ngenfile=$5
  simse_now=$6

  prodname="SM-2fD"
  proddir=${PRODTASKDIR}/${prodname}

  if [ ! -e ${proddir} ] ; then 
    mkdir -v ${proddir} || my_abort "Failed to create directory" 
  fi
  # ###########################################################
  # Format of xxx-list.txt file.
  # <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
  #############################################################
cat > ${prodname}-list.txt <<EOF
${procname}:_I${processID}_${norder}/I${processID}
EOF

  excel_file="/home/ilc/miyamoto/ILDProd/mcprod/v03-02/initdirs/prodpara/250-SetA-SM-2f.xlsx"
  banned_sites="LCG.Bristol.uk,LCG.Cracow.pl,LCG.JINR-LCG2.ru,LCG.RAL-LCG2.uk,LCG.LAPP.fr,LCG.QMUL.uk,LCG-UKI-LT2-IC-HEP.uk,LCG.UKI-SOUTHGRID-RALPP.uk"
  export GENSPLIT_DEFAULT_NPROCS=8
  options=" --nodry --noPrompt -N ${norder} "
  
  cmd="init_production.py --workdir ${proddir} \
    --excel_file  ${excel_file} \
    --prodlist ${prodname}-list.txt \
    --dstonly --recrate -1 \
    --split_nseq_from ${nseq_from} --ngenfile_max ${ngenfile} \
    --delfiles sim --se_for_sim ${simse_now} \
    --sim_banned_sites ${banned_sites} --rec_banned_sites ${banned_sites} \
    --sim_nbtasks 1000 --rec_nbtasks 1000 \
    --production_type sim:ovl \
    --step4_options ${options} "
  
  echo ${cmd}
  
  echo " "
  ${cmd} \
    || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
    && mv -v ${prodname}-list.txt ${proddir}
  
}

###########################################################
# main part of this script
###########################################################
#  2f_I500006 and 2f_I500008 Large cross section processes.
#
#   create_scripts <procname> <processID> <norder> <nseq_from> <ngenfile> <simse> 
#
# #######################################################################################
# 2f_leptonic I500006 : Ser 107 to 212 : 
   create_scripts 2f_Z_leptonic.bWW 500006 3 107 53 DESY-SRM
   create_scripts 2f_Z_leptonic.bWW 500006 4 160 53 DESY-SRM


# 2f_leptonic I500008 : Ser 82 to 163 : 
   create_scripts 2f_Z_leptonic.bWW 500008 3  82 41 DESY-SRM
   create_scripts 2f_Z_leptonic.bWW 500008 4 123 41 DESY-SRM

