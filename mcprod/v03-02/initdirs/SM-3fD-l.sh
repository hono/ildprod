#!/bin/bash 
#
#  Create scripts for background files.
#

# ###########################################################
# Create production scripts
# Usage:
#   create_scripts [norder] [nstep] [proddir]
# Argeuments:
#   [evttype] : event type and pol, like 3f.bBW
#   [procid] : ProcessID
#   [norder] : Production sequence number
#   [simse] : SE for SIM data output
#   [proddir] : Directories of production
# ###########################################################
# I500014(0-162, 1.92ab^-1), I500016(0-162, 1.92 ab^-1). 
# For 1 ab^-1, 85 files, 69 files more. 
# Seq from 16 , 35 files(nstep),  
create_scripts(){
  nstep=35
  evttype=$1
  procid=$2
  norder=$3
  simse_now=$4
  proddir_now=$5
  nseq_from=$[16+(${norder}-1)*${nstep}]

  # ###########################################################
  # Format of xxx-list.txt file.
  # <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
  #############################################################
cat > ${prodname}-list.txt <<EOF
${evttype}:_${procid}_${norder}/${procid}
EOF


  excel_file="/home/ilc/miyamoto/ILDProd/mcprod/v03-02/initdirs/prodpara/250-SetA-SM-3f.xlsx"
  banned_sites="LCG.Bristol.uk,LCG.Cracow.pl,LCG.JINR-LCG2.ru,LCG.RAL-LCG2.uk,LCG.LAPP.fr"
  export GENSPLIT_DEFAULT_NPROCS=8
  options=" --nodry --noPrompt -N ${norder} "
  
  cmd="init_production.py --workdir ${proddir_now} \
    --excel_file  ${excel_file} \
    --prodlist ${prodname}-list.txt \
    --dstonly --recrate -1 \
    --split_nseq_from ${nseq_from} --ngenfile_max ${nstep} \
    --delfiles sim --se_for_sim ${simse_now} \
    --sim_banned_sites ${banned_sites} --rec_banned_sites ${banned_sites} \
    --sim_nbtasks 1000 --rec_nbtasks 1000 \
    --production_type sim:ovl \
    --step4_options ${options} "
  
  echo ${cmd}
  
  echo " "
  ${cmd} \
    || ( echo "Failed to produce scripts in ${proddir_now}" && exit 1 ) \
    && mv -v ${prodname}-list.txt ${proddir_now}
  
}


prodname="SM-3fD"
proddir=${PRODTASKDIR}/${prodname}

if [ ! -e ${proddir} ] ; then 
  mkdir -v ${proddir} || my_abort "Failed to create directory" 
fi

#   [evttype] : event type and pol, like 3f.bBW
#   [procid] : ProcessID
#   [norder] : Production sequence number
#   [simse] : SE for SIM data output
#   [proddir] : Directories of production

create_scripts 3f.bWB I500014 1 DESY-SRM ${proddir}

create_scripts 3f.bWB I500014 2 KEK-DISK ${proddir}

create_scripts 3f.bWB I500016 1 DESY-SRM ${proddir}

create_scripts 3f.bWB I500016 2 KEK-DISK ${proddir}

