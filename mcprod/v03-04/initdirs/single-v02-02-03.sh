#!/bin/bash 
#
#  Create scripts for the production of calibration samples.
#
my_abort ()
{
  echo "$@" 1>&2
  exit 1
}

prodname="single-v02-02-03"
proddir=${PRODTASKDIR}/${prodname}

if [ ! -e ${proddir} ] ; then 
  mkdir -vp ${proddir} || my_abort "Failed to create directory" 
fi

# ###########################################################
# Format of xxx-list.txt file.
# <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
#############################################################

cat > ${prodname}-list.txt <<EOF
single.b00
EOF

export GENSPLIT_DEFAULT_NPROCS=6

banned_sites="LCG.Bristol.uk,LCG.Cracow.pl,LCG.JINR-LCG2.ru,LCG.RAL-LCG2.uk,LCG.LAPP.fr,LCG.QMUL.uk,LCG-UKI-LT2-IC-HEP.uk,LCG.UKI-SOUTHGRID-RALPP.uk"
excel_file="/home/ilc/miyamoto/ILDProd/mcprod/v03-02/initdirs/prodpara/single.xlsx"

cmd="init_production.py --workdir ${proddir} --excel_file ${excel_file} \
  --prodlist ${prodname}-list.txt \
  --sim_models "ILD_l5_v02" \
  --sim_banned_sites ${banned_sites} \
  --rec_banned_sites ${banned_sites} \
  --sim_nbtasks 100 --rec_nbtasks 100 \
  --production_type sim:nobg "


echo ${cmd}

echo " "
${cmd} \
  || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
  && mv -v ${prodname}-list.txt ${proddir}

################################################################################
# init_production.py help # See init_production.help
################################################################################
