#!/bin/bash 
#
#  Create production scripts
#
my_abort ()
{
  echo "$@" 1>&2
  exit 1
}
prodname="600-Test_hh"
proddir=${PRODTASKDIR}/${prodname}
if [ ! -e ${proddir} ] ; then 
  mkdir -vp ${proddir} || my_abort "Failed to create directory" 
fi

# ###########################################################
# Format of xxx-list.txt file.
# <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
#############################################################
cat > ${prodname}-list.txt <<EOF
hhnn.bWW
hhe1e1.bWW
hhqq.bWW
hhll.bWW
EOF

simse="KEK-DISK"
excel_file="prodpara/600-Test_hh.xlsx"
banned_sites=`joinlines banned_sites.txt`
export GENSPLIT_DEFAULT_NPROCS=8
norder=1

options=" --nodry --noPrompt"

cmd="init_production.py --workdir ${proddir} \
  --excel_file ${excel_file} \
  --xml_serial ${norder} \
  --prodlist ${prodname}-list.txt \
  --dstonly --recrate 0.10 \
  --split_nseq_from 0 \
  --se_for_sim  ${simse} \
  --se_for_data KEK-DISK --se_for_gensplit KEK-DISK --se_for_logfiles KEK-DISK \
  --sim_banned_sites ${banned_sites} --rec_banned_sites ${banned_sites} \
  --sim_nbtasks 1000 --rec_nbtasks 1000 \
  --production_type sim:ovl \
  --step4_options ${options} "

echo ${cmd}

#    --ngenfile_max ${ngenfile_max} \
#    --split_nseq_from ${nsplit_nseq_from} \
#    --test --se_for_data KEK-DISK --se_for_gensplit KEK-DISK --se_for_logfiles KEK-DISK \
#    --nw_perfile 10 --split_nbfiles 10 \
#

  echo " "
  ${cmd} \
    || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
    && mv -v ${prodname}-list.txt ${proddir}


########################################################################
