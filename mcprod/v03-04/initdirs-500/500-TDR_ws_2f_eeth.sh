#!/bin/bash 
#
#  Create production scripts
#
my_abort ()
{
  echo "$@" 1>&2
  exit 1
}
prodname="500-2f_eeth"
proddir=${PRODTASKDIR}/${prodname}
if [ ! -e ${proddir} ] ; then 
  mkdir -vp ${proddir} || my_abort "Failed to create directory" 
fi

# ###########################################################
# Format of xxx-list.txt file.
# <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
#############################################################
cat > ${prodname}-list.txt <<EOF
2f_eeth_eL_pR.bWW
2f_eeth_eL_pL.bWW
2f_eeth_eR_pL.bWW
2f_eeth_eR_pR.bWW
EOF

#simse="DESY-SRM"
simse="KEK-DISK"
excel_file="prodpara/500-TDR_ws_2f_eeth.xlsx"
banned_sites=`joinlines banned_sites.txt`
export GENSPLIT_DEFAULT_NPROCS=8
#norder=1

ngenfile_max=2
nsplit_nseq_from=0

#options=" --nodry --noPrompt -N ${norder} "
options=" --nodry --noPrompt"

cmd="init_production.py --workdir ${proddir} \
  --se_for_data KEK-DISK --se_for_gensplit KEK-DISK --se_for_logfiles KEK-DISK \
  --excel_file ${excel_file} \
  --prodlist ${prodname}-list.txt \
  --dstonly --recrate 0.10 \
  --split_nseq_from 0 \
  --delfiles sim --se_for_sim  ${simse} \
  --sim_banned_sites ${banned_sites} --rec_banned_sites ${banned_sites} \
  --sim_nbtasks 1000 --rec_nbtasks 1000 \
  --production_type sim:ovl \
  --step4_options ${options} "

echo ${cmd}

#    --ngenfile_max ${ngenfile_max} \
#    --split_nseq_from ${nsplit_nseq_from} \
#    --test --se_for_data KEK-DISK --se_for_gensplit KEK-DISK --se_for_logfiles KEK-DISK \
#    --nw_perfile 10 --split_nbfiles 10 \
#

  echo " "
  ${cmd} \
    || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
    && mv -v ${prodname}-list.txt ${proddir}

########################################################################
