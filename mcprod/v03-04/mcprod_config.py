#
# Config file for ILD MCProduction
#
# Parameters constant to whole set is defined here
# 
# Parameters for the production with v02-02-03 
# This is default setting. 
# If "ThisProd_config.py" exists in 4_subProd, it overwrites the setting here.

MCProd_Sim_Version = "ILCSoft-02-02-03_cc7"
MCProd_Sim_ILDConfig = "v02-02-03"
MCProd_Sim_Steering = "ddsim_steer.py"
MCProd_Rec_Version = "ILCSoft-02-02-03_cc7"
MCProd_Rec_ILDConfig = "v02-02-03"
MCProd_OverlayInput_ILDConfig = "v02-02-03"

# Required for DSTM job 
MCProd_init_ilcsoft = os.environ["ILCSOFT_INIT_SCRIPT"]

# Default output directory
MCProd_MCOpt_DirTape = "mc-2020"
MCProd_MCOpt_DirDisk = "mc-2020"

# MCProd_OverlayInput_BasePath = "/ilc/prod/ilc/%s/ild/sim" % MCProd_MCOpt_DirTape
MCProd_OverlayInput_BasePath = "/ilc/prod/ilc/mc-2020/ild/sim"


# Machine dependant IP smearing/offset parameters
# See ~/ILDProd/gensamples/20180627-aalowpt/beam_conditions.txt ( a mail from Mikael )
# 
MCProd_ZOffset_And_ZSigma = {}
# 500-TDR_ws
Z0_SZ = { "WW":[0.0, 0.1968], "BB":[0.0, 0.16988], 
           "WB":[-0.04222, 0.186], "BW":[0.00422, 0.186] }

MCProd_ZOffset_And_ZSigma["500-TDR_ws"] = {
    "LL":Z0_SZ["WW"], "LR":Z0_SZ["WW"], "RL":Z0_SZ["WW"], "RR":Z0_SZ["WW"], 
    "LW":Z0_SZ["WW"], "LB":Z0_SZ["WB"], "RW":Z0_SZ["WW"], "RB":Z0_SZ["WB"], 
    "WL":Z0_SZ["WW"], "BL":Z0_SZ["BW"], "WR":Z0_SZ["WW"], "BR":Z0_SZ["BW"], 
    "WW":Z0_SZ["WW"], "BB":Z0_SZ["BB"], "WB":Z0_SZ["WB"], "BW":Z0_SZ["BW"]  }

# 1000-B1b_ws parameters
Z0_SZ = { "WW":[0.0, 0.1468], "BB":[0.0, 0.1260], 
           "WB":[-0.0352, 0.1389], "BW":[0.0357, 0.1394] }

MCProd_ZOffset_And_ZSigma["1000-B1b_ws"] = {
    "LL":Z0_SZ["WW"], "LR":Z0_SZ["WW"], "RL":Z0_SZ["WW"], "RR":Z0_SZ["WW"], 
    "LW":Z0_SZ["WW"], "LB":Z0_SZ["WB"], "RW":Z0_SZ["WW"], "RB":Z0_SZ["WB"], 
    "WL":Z0_SZ["WW"], "BL":Z0_SZ["BW"], "WR":Z0_SZ["WW"], "BR":Z0_SZ["BW"], 
    "WW":Z0_SZ["WW"], "BB":Z0_SZ["BB"], "WB":Z0_SZ["WB"], "BW":Z0_SZ["BW"]  }

# 250-SetA parameters
Z0_SZ = { "WW":[0.0, 0.202], "BB":[0.0, 0.171], 
           "WB":[-0.0389, 0.191], "BW":[0.0380, 0.191] }

MCProd_ZOffset_And_ZSigma["250-SetA"] = {
    "LL":Z0_SZ["WW"], "LR":Z0_SZ["WW"], "RL":Z0_SZ["WW"], "RR":Z0_SZ["WW"], 
    "LW":Z0_SZ["WW"], "LB":Z0_SZ["WB"], "RW":Z0_SZ["WW"], "RB":Z0_SZ["WB"], 
    "WL":Z0_SZ["WW"], "BL":Z0_SZ["BW"], "WR":Z0_SZ["WW"], "BR":Z0_SZ["BW"], 
    "WW":Z0_SZ["WW"], "BB":Z0_SZ["BB"], "WB":Z0_SZ["WB"], "BW":Z0_SZ["BW"]  }

# 91-nobeam parameters ( uses values same as 250-SetA )
MCProd_ZOffset_And_ZSigma["91-nobeam"] = MCProd_ZOffset_And_ZSigma["250-SetA"]

# 550-Test parameters (use same values as 500-TDR_ws)
MCProd_ZOffset_And_ZSigma["550-Test"] = MCProd_ZOffset_And_ZSigma["500-TDR_ws"]

# 600-Test parameters (use same values as 500-TDR_ws)
MCProd_ZOffset_And_ZSigma["600-Test"] = MCProd_ZOffset_And_ZSigma["500-TDR_ws"]

###################################################################################
# Data for background overlay
MCProd_Overlay_Data = {}
# For re-created backgrounds on 31-Jan-2021, 3 times statistics than before.
MCProd_Overlay_Data["250-SetA"] = { 
  "ILD_l5_v02": { "BGOverlayWW": { "evttype":"aa_lowpt_WW",  "expBG":0.1256495436,  "ProdID": 15330 },
                  "BGOverlayWB": { "evttype":"aa_lowpt_WB",  "expBG":0.297459204,   "ProdID": 15325 },
                  "BGOverlayBW": { "evttype":"aa_lowpt_BW",  "expBG":0.29722665,    "ProdID": 15324 },
                  "BGOverlayBB": { "evttype":"aa_lowpt_BB",  "expBG":0.829787658,   "ProdID": 15323 },
                "PairBgOverlay": { "evttype":"seeablepairs", "expBG":1.0,           "ProdID": 15322 } } }

MCProd_Overlay_Data["91-nobeam"] = { 
  "ILD_l5_v02": { "BGOverlayWW": { "evttype":"aa_lowpt_WW",  "expBG":0.1256495436,  "ProdID": 15330, "EMpara":"250-SetA"},
                  "BGOverlayWB": { "evttype":"aa_lowpt_WB",  "expBG":0.297459204,   "ProdID": 15325, "EMpara":"250-SetA"},
                  "BGOverlayBW": { "evttype":"aa_lowpt_BW",  "expBG":0.29722665,    "ProdID": 15324, "EMpara":"250-SetA"},
                  "BGOverlayBB": { "evttype":"aa_lowpt_BB",  "expBG":0.829787658,   "ProdID": 15323, "EMpara":"250-SetA"},
                "PairBgOverlay": { "evttype":"seeablepairs", "expBG":1.0,           "ProdID": 15322, "EMpara":"250-SetA"} } }

#
# #############################################################################################
# Overlay setting for MC-2020 (ILDConfig-v02-02-03)
# #############################################################################################
MCProd_Overlay_Data["500-TDR_ws"] = { 
  "ILD_l5_v02": { "BGOverlayWW": { "evttype":"aa_lowpt_WW",  "expBG":0.211,   "ProdID": 15721 },
                  "BGOverlayWB": { "evttype":"aa_lowpt_WB",  "expBG":0.24605, "ProdID": 15720 },
                  "BGOverlayBW": { "evttype":"aa_lowpt_BW",  "expBG":0.243873,"ProdID": 15719 },
                  "BGOverlayBB": { "evttype":"aa_lowpt_BB",  "expBG":0.35063, "ProdID": 15718 },
                  "PairBgOverlay": { "evttype":"seeablepairs", "expBG":1.0,     "ProdID": 15717 } } }

# #############################################################################################
# Overlay setting for MC-2020 (ILDConfig-v02-02-03)
# #############################################################################################
MCProd_Overlay_Data["550-Test"] = { 
  "ILD_l5_v02": { "BGOverlayWW": { "evttype":"aa_lowpt_WW",  "expBG":0.211,   "ProdID": 15702 , "EMpara":"500-TDR_ws"},
                  "BGOverlayWB": { "evttype":"aa_lowpt_WB",  "expBG":0.24605, "ProdID": 15701 , "EMpara":"500-TDR_ws"},
                  "BGOverlayBW": { "evttype":"aa_lowpt_BW",  "expBG":0.243873,"ProdID": 15698 , "EMpara":"500-TDR_ws"},
                  "BGOverlayBB": { "evttype":"aa_lowpt_BB",  "expBG":0.35063, "ProdID": 15697 , "EMpara":"500-TDR_ws"},
                "PairBgOverlay": { "evttype":"seeablepairs", "expBG":1.0,     "ProdID": 15696 , "EMpara":"500-TDR_ws"} } }

# #############################################################################################
# Overlay setting for MC-2020 (ILDConfig-v02-02-03)
# #############################################################################################
MCProd_Overlay_Data["600-Test"] = { 
  "ILD_l5_v02": { "BGOverlayWW": { "evttype":"aa_lowpt_WW",  "expBG":0.211,   "ProdID": 15702 , "EMpara":"500-TDR_ws"},
                  "BGOverlayWB": { "evttype":"aa_lowpt_WB",  "expBG":0.24605, "ProdID": 15701 , "EMpara":"500-TDR_ws"},
                  "BGOverlayBW": { "evttype":"aa_lowpt_BW",  "expBG":0.243873,"ProdID": 15698 , "EMpara":"500-TDR_ws"},
                  "BGOverlayBB": { "evttype":"aa_lowpt_BB",  "expBG":0.35063, "ProdID": 15697 , "EMpara":"500-TDR_ws"},
                "PairBgOverlay": { "evttype":"seeablepairs", "expBG":1.0,     "ProdID": 15696 , "EMpara":"500-TDR_ws"} } }
