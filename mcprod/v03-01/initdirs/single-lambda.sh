#!/bin/bash 
#
#  Create scripts for the production of calibration samples.
#

prodname="calib"
proddir=${PRODTASKDIR}/${prodname}

if [ ! -e ${proddir} ] ; then 
  mkdir -v ${proddir} || my_abort "Failed to create directory" 
fi

# ###########################################################
# Format of xxx-list.txt file.
# <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
#############################################################

cat > ${prodname}-list.txt <<EOF
lambda.b00
EOF
# uds.b00.0lep

export GENSPLIT_DEFAULT_NPROCS=3

banned_sites="LCG.UKI-SOUTHGRID-RALPP.uk,LCG.Brunel.uk,LCG.UKI-LT2-IC-HEP.uk,LCG.UKI-NORTHGRID-LIV-HEP.uk"

cmd="init_production.py --workdir ${proddir} --excel_file prodpara/calib-lambda.xlsx \
  --prodlist ${prodname}-list.txt \
  --sim_banned_sites ${banned_sites} --rec_banned_sites ${banned_sites} \
  --sim_models "ILD_l5_v02" \
  --sim_nbtasks 20 --rec_nbtasks 20 \
  --production_type sim:nobg "


echo ${cmd}

echo " "
${cmd} \
  || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
  && mv -v ${prodname}-list.txt ${proddir}

################################################################################
# init_production.py help # See init_production.help
################################################################################
