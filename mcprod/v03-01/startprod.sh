#!/bin/bash 

check_proxy()
{
  echo "**** Checking proxy info..."
  proxy_register=`dirac-proxy-info | grep "VOMS"`
  if [ "x${proxy_register}" == "x" ]; then
    echo "**** dirac-proxy-init is not initiated."
    echo "**** Production role is required to execute $0"
    echo "**** Need to execute % dirac-proxy-init -g ilc_prod"
    exit
  fi

  proxy_group=`dirac-proxy-info | grep "DIRAC group" | cut -d ':' -f 2 | sed s/\ //g`
  if [ "x${proxy_group}" == "xilc_prod" ]; then
    echo "**** Production role ${proxy_group} is selected."
  else
    echo "**** Selected role is ${proxy_group}"
    echo "**** Production role is required to execute $0"
    echo "**** Need to execute % dirac-proxy-init -g ilc_prod"
    echo "Exit"
    exit
  fi
}


do_onedir(){
  rdir=$1
  nsleep=$2
  par_dir=$3
  ( 
    cd ${rdir}
    dstr=`date +%Y%m%d-%H%M%S`
    echo "+++ current directory : `pwd`, `hostname` on ${dstr} "
    ( cd 1_prepProd  &&  . run_step1.cmd &&  echo "+++ task in 1_preProd completed "   )
    ( cd 1_prepProd && firstfile_on_disk.sh )
    ( echo "Sleeping ${nsleep} " 
      sleep ${nsleep}
      cd 4_subprod 
      ./task_step4.sh > run.log 2>&1 
    ) & 
    dstr=`date +%Y%m%d-%H%M%S`
    echo "+++ task in 4_subprod will start in ${nsleep}. "
    echo "+++ move to 3_gensplit and start splitting on ${dstr} "
    ( cd 3_gensplit &&  task_step3.sh > run-task_step3.log 2>&1 )
    echo "+++ Task in ${rdir} completed on ${dstr}"
  )

    # echo "# ${rdir}" >> ${par_dir}/prodids.list
    # /bin/ls recprod_*.json simprod*.json | cut -d"_" -f2 | sort >> ${par_dir}/prodids.list 
}


dirlist="startprod.list"
waitmin="10m"
myhost=`hostname`
dstr=`date +%Y%m%d-%H%M%S`
parent_dir=`pwd`

check_proxy

if [ `cat ${dirlist} | wc -l` -eq 0 ]; then
  echo "**** Need to edit ${dirlist} to initiate production"
  echo "Exit"
  exit
else
  echo "**** Do you realy start following production?"
  list=`cat ${dirlist}`
  echo ${list}
  echo -n " Select [y/n]: "
  read ANS

  case $ANS in
    [Yy]* ) echo "**** Start production"; echo ${list} ;;
    * ) echo "**** Stopped" ; exit ;;
  esac
fi

while [ `cat ${dirlist} | wc -l` -ne 0 ] ; do 
  headline=`head -1 ${dirlist}`
  tail -n +2 ${dirlist} > temp-${dirlist}
  mv temp-${dirlist} ${dirlist}
  echo "+++++ Start ${headline} production at ${myhost} on ${dstr} +++"
  topdir=`dirname ${headline}`
  wdir=`basename ${headline}`
  # echo ${headline} >> ${parent_dir}/active-prod-dir.list
  echo ${headline} >> ${parent_dir}/startprod.done
  ( 
    cd ${topdir}
    dstr=`date +%Y%m%d-%H%M%S`
    echo "+++ chdir to ${topdir} on ${dstr} +++"
    . ${MYPROD_PRODPROD} 
    do_onedir ${wdir} ${waitmin} ${parent_dir}
    if [ -e ${parent_dir}/startprod.exit ] ; then
      echo "+++ Exit by startprod.exit"
      exit
    fi 
    while [ -e ${parent_dir}/sleep.cmd ] ; do
      . ${parent_dir}/sleep.cmd
    done
    . ${MYPROD_PRODPROD} 
   )
done

dstr=`date +%Y%m%d-%H%M%S`
echo "+++ No more entry in ${dirlist} on ${dstr}"
if [ -e ${parent_dir}/post_exec.sh ] ; then 
  ${parent_dir}/post_exec.sh > ${parent_dir}/post_exec.log 2>&1 &
fi 

# echo -n "+++ sleep.cmd `date` | sleeping 10m " && sleep 15m && echo "wakeup `date` " 
