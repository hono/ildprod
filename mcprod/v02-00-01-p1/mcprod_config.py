#
# Config file for ILD MCProduction
#
# Parameters constant to whole set is defined here
# 
# Parameters for the production with v02-00-01 

MCProd_Sim_Version = "ILCSoft-02-00-01_gcc49"
MCProd_Sim_ILDConfig = "v02-00-01"
MCProd_Sim_Steering = "ddsim_steer.py"
MCProd_Rec_Version = "ILCSoft-02-00-01_gcc49"
MCProd_Rec_ILDConfig = "v02-00-01"

# Required for DSTM job
MCProd_init_ilcsoft = os.environ["ILCSOFT_INIT_SCRIPT"]

# Default output directory
MCProd_MCOpt_DirTape = "mc-opt-3"
MCProd_MCOpt_DirDisk = "mc-opt.dsk"
MCProd_OverlayInput_BasePath = "/ilc/prod/ilc/%s/ild/sim" % MCProd_MCOpt_DirTape


# Machine dependant IP smearing/offset parameters
# See ~/ILDProd/gensamples/20180627-aalowpt/beam_conditions.txt ( a mail from Mikael )
# 
MCProd_ZOffset_And_ZSigma = {}
# 500-TDR_ws
# MCProd_ZOffset_And_ZSigma["500-TDR_ws"] = {"LL":[0.0, 0.1968], "LR":[0.0, 0.1968],   "RL":[0.0, 0.1968], "RR":[0.0, 0.1968],
#                          "LW":[0.0, 0.1968], "LB":[-0.04222, 0.186], "RW":[0.0, 0.1968], "RB":[-0.04222, 0.186],
#                          "WL":[0.0, 0.1968], "BL":[0.04222, 0.186],  "WR":[0.0, 0.1968], "BR":[0.04222, 0.186],
#                          "WW":[0.0, 0.1968], "BB":[0.0, 0.16988],    "WB":[-0.04222, 0.186], "BW":[0.04222, 0.186]}

Z0_SZ = { "WW":[0.0, 0.1968], "BB":[0.0, 0.16988], 
           "WB":[-0.04222, 0.186], "BW":[0.00422, 0.186] }

MCProd_ZOffset_And_ZSigma["500-TDR_ws"] = {
    "LL":Z0_SZ["WW"], "LR":Z0_SZ["WW"], "RL":Z0_SZ["WW"], "RR":Z0_SZ["WW"], 
    "LW":Z0_SZ["WW"], "LB":Z0_SZ["WB"], "RW":Z0_SZ["WW"], "RB":Z0_SZ["WB"], 
    "WL":Z0_SZ["WW"], "BL":Z0_SZ["BW"], "WR":Z0_SZ["WW"], "BR":Z0_SZ["BW"], 
    "WW":Z0_SZ["WW"], "BB":Z0_SZ["BB"], "WB":Z0_SZ["WB"], "BW":Z0_SZ["BW"]  }

# 1000-B1b_ws parameters
Z0_SZ = { "WW":[0.0, 0.1468], "BB":[0.0, 0.1260], 
           "WB":[-0.0352, 0.1389], "BW":[0.0357, 0.1394] }

MCProd_ZOffset_And_ZSigma["1000-B1b_ws"] = {
    "LL":Z0_SZ["WW"], "LR":Z0_SZ["WW"], "RL":Z0_SZ["WW"], "RR":Z0_SZ["WW"], 
    "LW":Z0_SZ["WW"], "LB":Z0_SZ["WB"], "RW":Z0_SZ["WW"], "RB":Z0_SZ["WB"], 
    "WL":Z0_SZ["WW"], "BL":Z0_SZ["BW"], "WR":Z0_SZ["WW"], "BR":Z0_SZ["BW"], 
    "WW":Z0_SZ["WW"], "BB":Z0_SZ["BB"], "WB":Z0_SZ["WB"], "BW":Z0_SZ["BW"]  }

# 250-SetA parameters
Z0_SZ = { "WW":[0.0, None], "BB":[0.0, None], 
           "WB":[-0.0352, None], "BW":[0.0357, None] }

MCProd_ZOffset_And_ZSigma["250-SetA"] = {
    "LL":Z0_SZ["WW"], "LR":Z0_SZ["WW"], "RL":Z0_SZ["WW"], "RR":Z0_SZ["WW"], 
    "LW":Z0_SZ["WW"], "LB":Z0_SZ["WB"], "RW":Z0_SZ["WW"], "RB":Z0_SZ["WB"], 
    "WL":Z0_SZ["WW"], "BL":Z0_SZ["BW"], "WR":Z0_SZ["WW"], "BR":Z0_SZ["BW"], 
    "WW":Z0_SZ["WW"], "BB":Z0_SZ["BB"], "WB":Z0_SZ["WB"], "BW":Z0_SZ["BW"]  }


###################################################################################
# Data for background overlay
MCProd_Overlay_Data = {}
MCProd_Overlay_Data["500-TDR_ws"] = { 
  "ILD_l5_v02": { "BGOverlayWW": { "evttype":"aa_lowpt_WW",  "expBG":0.211,   "ProdID": 10237 },
                  "BGOverlayWB": { "evttype":"aa_lowpt_WB",  "expBG":0.24605, "ProdID": 10241 },
                  "BGOverlayBW": { "evttype":"aa_lowpt_BW",  "expBG":0.243873,"ProdID": 10239 },
                  "BGOverlayBB": { "evttype":"aa_lowpt_BB",  "expBG":0.35063, "ProdID": 10235 },
                "PairBgOverlay": { "evttype":"seeablepairs", "expBG":1.0,     "ProdID": 10233 } },
  "ILD_s5_v02": { "BGOverlayWW": { "evttype":"aa_lowpt_WW",  "expBG":0.211,   "ProdID": 10238 },
                  "BGOverlayWB": { "evttype":"aa_lowpt_WB",  "expBG":0.24605, "ProdID": 10242 },
                  "BGOverlayBW": { "evttype":"aa_lowpt_BW",  "expBG":0.243873,"ProdID": 10240 },
                  "BGOverlayBB": { "evttype":"aa_lowpt_BB",  "expBG":0.35063, "ProdID": 10236 },
                "PairBgOverlay": { "evttype":"seeablepairs", "expBG":1.0,     "ProdID": 10234 } } }

MCProd_Overlay_Data["1000-B1b_ws"] = { 
  "ILD_l5_v02": { "BGOverlayWW": { "evttype":"aa_lowpt_WW",  "expBG":0.526,   "ProdID": -10237 },
                  "BGOverlayWB": { "evttype":"aa_lowpt_WB",  "expBG":0.634,   "ProdID": -10241 },
                  "BGOverlayBW": { "evttype":"aa_lowpt_BW",  "expBG":0.634,   "ProdID": -10239 },
                  "BGOverlayBB": { "evttype":"aa_lowpt_BB",  "expBG":0.744,   "ProdID": -10235 },
                "PairBgOverlay": { "evttype":"seeablepairs", "expBG":1.0,     "ProdID": -10233 } },
  "ILD_s5_v02": { "BGOverlayWW": { "evttype":"aa_lowpt_WW",  "expBG":0.526,   "ProdID": -10238 },
                  "BGOverlayWB": { "evttype":"aa_lowpt_WB",  "expBG":0.634,   "ProdID": -10242 },
                  "BGOverlayBW": { "evttype":"aa_lowpt_BW",  "expBG":0.634,   "ProdID": -10240 },
                  "BGOverlayBB": { "evttype":"aa_lowpt_BB",  "expBG":0.744,   "ProdID": -10236 },
                "PairBgOverlay": { "evttype":"seeablepairs", "expBG":1.0,     "ProdID": -10234 } } }

