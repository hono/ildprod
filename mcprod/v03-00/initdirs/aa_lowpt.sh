#!/bin/bash 
#
#  Create scripts for background files.
#

prodname="aa_lowpt"
proddir=${PRODTASKDIR}/${prodname}

if [ ! -e ${proddir} ] ; then 
  mkdir -v ${proddir} || my_abort "Failed to create directory" 
fi


# ###########################################################
# Format of xxx-list.txt file.
# <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
#############################################################
testname="aa_lowpt"
cat > ${prodname}-list.txt <<EOF
aa_lowpt.bBB
aa_lowpt.bBW
aa_lowpt.bWB
aa_lowpt.bWW
EOF

# ( cd ${proddir} && rm -rfv *.bWW.[01]lep_${testname} )

export GENSPLIT_DEFAULT_NPROCS=3

# --noelog
#  --reuse_gensplit 500590 
#  --noelog --reuse_gensplit 500590 \


# %% are converted to -- in task_step4_main.sh  
#addopt=" --nodry "
#addopt+=" --EnergyParametersFile Config/ParametersUnknownGeV.xml "
#addopt+=" --BeamCalBackgroundFile HighLevelReco/BeamCalBackground/BeamCalBackground-E500-B3.5-RealisticNominalAntiDid.root "

cmd="init_production.py --workdir ${proddir} --excel_file prodpara/bkg.xlsx \
  --prodlist ${prodname}-list.txt \
  --nw_perfile 100 --ngenfile_max 1 --split_nbfiles 20 \
  --sim_nbtasks 5 --rec_nbtasks 5 \
  --production_type sim "

#   --test --se_for_data KEK-SRM --se_for_gensplit KEK-SRM --se_for_logfiles KEK-SRM \
#   --se_for_dstm_replicates DESY-SRM \
#  --step4_options ${addopt} " 
#   --genmeta_json genmetaByID/aa_lowpt.json \

#
# task_step4_main.sh -r o1 --options ' --nodry --DSTonly --SimSelectedFile 2 
#  ' --ptype nobg


echo ${cmd}

echo " "
${cmd} \
  || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
  && mv -v ${prodname}-list.txt ${proddir}

################################################################################
# init_production.py help # See init_production.help
################################################################################
