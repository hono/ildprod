#!/bin/env python 
#
# delete elog entry
#

import elog
import os
import argparse
import json
import datetime
import pprint
import glob
# import ssl
# from urllib2 import urlopen
import subprocess

import ExcelTools
import ILDLockFile
from openpyxl import Workbook
from openpyxl import load_workbook


_logbook = None
_elog_url = "https://ild.ngt.ndu.ac.jp/elog"
_elog_subdir = "opt-data"


# ######################################################
def open_logbook():
    ''' open elog logbook '''
    global _logbook, _elog_url, _elog_subdir, _do_test
   
    if _logbook == None:
        lines = open(os.environ["MY_ELOG_KEY"]).readlines()
        ( auser, apass ) = lines[0][:-1].split(' ')

        _logbook = elog.open("%s/%s/" % (_elog_url, _elog_subdir), user=auser, password=apass)

    return 

# =========================================================
if __name__ == "__main__":

    open_logbook()


    # for idel in range(200, 5, -1):
    # for idel in range(848, 0, -1):
    # for idel in range(1091, 1514):
    #    _logbook.delete(idel)
    #    print( "Elog entry "+_elog_url+"/"+_elog_subdir+"/"+str(idel)+" was deleted." )

   
