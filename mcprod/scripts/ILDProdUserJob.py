'''
ILDProdUserJob.py 

  A utility class for job submission of DDSIM and Marlin production by UserJob

  Akiya Miyamoto  17 July, 2017
'''

from __future__ import print_function
import json 
import datetime
import os
import subprocess
import pprint
import ILDIDTool
import time

from DIRAC import gLogger, S_OK, S_ERROR
from DIRAC.Core.Base.Script import Script
Script.parseCommandLine()

from ILCDIRAC.Interfaces.API.DiracILC                  import DiracILC
from ILCDIRAC.Interfaces.API.NewInterface.UserJob      import UserJob
from ILCDIRAC.Interfaces.API.NewInterface.Applications import DDSim, GenericApplication, Marlin
from ILCDIRAC.Core.Utilities.FilenameEncoder           import decodeFilename, makeFilename

####################################################
class ILDProdUserJob(object):
  ''' class ILDProdUserJob '''
  def __init__(self, isLocal=True):
    self.is_local = isLocal
    self.config = ''    
    self.do_submit = True

  def createUserJob(self, dirac_ilc, config):
    ''' createUserJob '''

    ujob = UserJob()
    ujob.dontPromptMe()

    job_setters = {"job_group_name":"setJobGroup",
                   "jobname":"setName",
                   "banned_sites":"setBannedSites",
                   "job_destination":"setDestination",
                   "max_cpu_time":"setCPUTime",
                   "inputSB":"setInputSandbox",
                   "outputSB":"setOutputSandbox"
                  }
    for aset, v in job_setters.items():
        if aset in config and job_setters[aset] != "":
            method = getattr(ujob, job_setters[aset])
            # print method.__name__
            # print aset
            # print config[aset]
            method(config[aset])
    if "ildconfig" in config:
    #   if config["inputSB_has_ildconfig"]:
        ujob.setILDConfig(config["ildconfig"])
    if "inputSB" in config:  # Make sure local files of inputSB exist
      for lfile in config["inputSB"]:
        print( "Checking path of inputSB " + lfile )
        if lfile[:4] != "LFN:": 
          if not os.path.exists(lfile):
            gLogger.error("## Fatal Error : not found a local file for InputSB, %s" % lfile )
            exit(1)         

    self.job = ujob

    return ujob

  def addStepDDSim(self, dirac_job, config):
    ''' Add ddsim step in dirac job '''

    sim = DDSim()

    sim.setVersion(config["ddsim_version"])
    sim.setInputFile(config["genfile"])
    sim.setDetectorModel(config["detector_model"])
    # Number of events should not exceed number of events in file.
    # Otherwise, G4exception is thrown

    sim.setNumberOfEvents(config["number_of_events"])
    if "ddsim_steering_file" in config:
      sim.setSteeringFile(config["ddsim_steering_file"])
    sim.setOutputFile(config["simfile"])
    if 'ddsim_startfrom' in config:
      sim.setStartFrom(int(config["ddsim_startfrom"])) # Number of events to skip before start simulation 
    if 'debug' in config:
      sim.setDebug()
    if "ddsim_random_seed" in config:
      sim.setRandomSeed(config["ddsim_random_seed"])
    if "extraCLIArguments" in config:
       sim.setExtraCLIArguments(config["extraCLIArguments"])

    res = dirac_job.append(sim)
    if not res['OK']:
      gLogger.error("Failed to append DDSim to dirac job(%s) " % res)
      exit(1)  
    return res

  def addStepMarlin(self, dirac_job, config):
    ''' Add ddmarlin step in dirac job '''
    ma = Marlin()
    ma.setDebug()
    ma.setVersion(config["marlin_version"])
    ma.setSteeringFile(config["marlin_steering_file"])
    ma.setDetectorModel(config["detector_model"])
    if "gearfile" in config:
      ma.setGearFile(config["gearfile"])
    if "siminput" in config:
      ma.setInputFile(config["siminput"])
    ma.setOutputDstFile(config["dstfile"])
    ma.setOutputRecFile(config["recfile"])
    if "extraCLIArguments" in config:
       ma.setExtraCLIArguments(config["extraCLIArguments"])
    res = dirac_job.append(ma)
    if not res['OK']:
      gLogger.error("Failed to append Marlin to dirac job(%s) " % res)
      exit(1)  
    return res

  def addStepBkgOverlay(self, dirac_job, config):
    ''' Add step to download aa_lowpt background file '''
    ov = OverlayInput()
    ov.setMachine(config["overlay_machine"])
    ov.setEnergy(config["energy"])
    ov.setNumberOfSignalEventsPerJob(config["number_of_events"])
    ov.setBXOverlay(config["overlay_BXOverlay"])
    ov.setGGToHadInt(config["overlay_GGToHadInt"])
    ov.setBkgEvtType("aa_lowpt")
    # ov.setBackgroundType("aa_lowpt")
    ov.setDetectorModel(config["detector_model"])
    res = dirac_job.append(ov)
    if not res['OK']:
      gLogger.error("Failed to append BkgOverlay to dirac job(%s) " % res)
      exit(1)  
    return res

  def addStepOneScript(self, dirac_job, script, arguments):
    ''' Append just one script to the dirac_job '''
    app = GenericApplication()
    app.setScript(script)
    app.setArguments(arguments)
    res = dirac_job.append(app)
    if not res['OK']:
      gLogger.error("Failed to append B script, %s %s, to dirac job " % (script, arguments))
      pprint.pprint(res)
      exit(1)  
    return res

  def addStepScripts(self, dirac_job, config):
    ''' Append Generic scripts to the dirac job. '''
    ''' COuld add more than one scripts '''
    app = GenericApplication()
    for script in config["jobscripts"]:
      app.setScript(script[0])
      if len(script) > 1:
        arguments = ' '.join(script[1:])
        app.setArguments(arguments)
    res = dirac_job.append(app)

    if not res['OK']:
      gLogger.error("Failed to append job scripts, %s " % config["jobscripts"])
      exit(1)
    return res

  def submitJob(self, mydirac, ujob, config, isLocal=False):
    if isLocal:
      res = ujob.submit(mydirac, mode='Local')
      if not res['OK']:
        print( "Local job was submitted. Return code was" )
        pprint.pprint(res)

    else:
      if "sleep_seconds" in config:
         print( "... sleeping %s seconds before submit" % config["sleep_seconds"] )
         time.sleep(int(config["sleep_seconds"]))

      res = ujob.submit(mydirac) if self.do_submit else {'OK':True, 'Value': "dummy_arguments"}
      if res['OK']:
         print( "Dirac job, "+str(res["Value"])+", was submitted." )
         if "configLog" in config:
            config["SubmitReturn"] = res
            json.dump(config, open(config["configLog"], "w"))
      else:
         print( "Faield to submit dirac job." )
         pprint.pprint(res)

    return res 

########################################################
def subCalibDDSim(params):
  '''
  A standard function to submit DDSim of UDS or single particle samples.

  :param dict params : dict opbjects containig job parameters
   params["jobtype"] : "Single", "UDS", or "Resonance"
   params["isLocal"] : False or True : True to run as local
   params["isTest"]  : False or True : To run a a test job, small number of events and write to a test directory.
   params["noUpload"]: False or True : True for not upload data
   params["jobgroupid"] : jobgroupid, like u025. jobgroupid should be obtained by a command, 
                     '/group/ilc/users/miyamoto/optprod/bin/ild-userprod-get-id -m "log_message" 
   params["ILDConfig"] : ILDConfig, v01-19-04_lcgeo, for example
   params["DDSIM_version" : DDSim version, ILCSoft-01-19-04_gcc49, for example
   params["detectors"] : A list of detector models, ["ILD_l5_v02", "ILD_s5_v02"]
   params["gendir"]  : Path to generator input files. valid when jobtype="Single"
   params["list_of_genfiles"] : File name of gen files list(UDS) or CSV file for Single 

  Akiya Miyamoto  22 November, 2017
                  27 December, 2017 Add Resonance jobtype
                  11 January, 2019 Rewrite for GuineaPig simulation from subCalibDDSim
  '''
  # jobtype = "Single" 
  # jobtype = "UDS"
  # isLocal = False
  # isTest = False
  # noUpload = False
  # jobgroupid = "undef" # For example, jobgrouid="u025"
  # ILDConfig = "v01-19-04_lcgeo"
  # DDSIM_version = "ILCSoft-01-19-04_gcc49"
  # list_of_detectors_input  = ["ILD_l5_v02", "ILD_s5_v02"]  # Detector models for production

  jobtype = params["jobtype"]
  isLocal = params["isLocal"]
  isTest  = params["isTest"]
  noUpload = params["noUpload"]
  jobgroupid = params["jobgroupid"]
  ILDConfig = params["ILDConfig"]
  DDSIM_version = params["DDSIM_version"]
  list_of_detectors_input = params["detectors"]
  gendir = params["gendir"]
  a_list_of_files = params["list_of_genfiles"]

  # Above parameters have to be modified
  ################################################## 
  if jobgroupid == "undef":
    print( "### Error : jobgroupid is not given yet.  Get groupid by command, " )
    print( '/group/ilc/users/miyamoto/optprod/bin/ild-userprod-get-id -m "log_message" ' ) 
    print( 'and re-run script.' )
    exit(10)


  config = {}
  config["job_group_name_format"] = "ILD"+jobtype+"-%s"
  config["jobname_format"] = jobtype.lower()+"-%s"
  # config["banned_sites"] = ["OSG.BNL.us", "LCG.QMUL.uk", "LCG.Cracow.pl", "LCG.UKI-NORTHGRID-LIV-HEP.uk",
  #                           "OSG.UCSDT2.us"]
  config["banned_sites"] = ["OSG.BNL.us", "LCG.UKI-NORTHGRID-LIV-HEP.uk", "OSG.UCSDT2.us",
                            "LCG.SCOTGRIDDURHAM.uk", "LCG.NIKHEF.nl", "LCG.UKI-SOUTHGRID-RALPP.uk"]
  #     "job_destination" : [""],
  #     "max_cpu_time":, 
  config["outputSB"] = [ "*.log", "*.py", "*.sh", "*.txt"]
  config["number_of_events"] = 1000   # Default for UDS. 10 for test. Single varies by input file.
  # 
  if ILDConfig == "v01-19-04_lcgeo":
    config["ddsim_steering_file"] = "ddsim_steer.py"
  elif ILDConfig == "v01-19-05":
    config["extraCLIArguments"] = " --steeringFile DDSim/ddsim_steer.py" 
  else:
    config["extraCLIArguments"] = " --steeringFile ddsim_steer.py" 

  if jobtype in ["Single", "Resonance", "UDS"]:
    config["extraCLIArguments"] += " --crossingAngleBoost 0.0 " # Turn off crossing angle boost
  # config["extraCLIArguments"] += params["extraCLIArguments"] if "extraCLIArguments" in params else ""
  config["upload_info"] = "upload_and_addmeta.json"
  config["configLog"] = "ddsimjob.json"

  config["ildconfig"] = ILDConfig
  config["ddsim_version"] = DDSIM_version

  config["job_group_id"] = jobgroupid
  config["job_group_name"] = config["job_group_name_format"] % config["job_group_id"]
  topdir = os.getcwd()
  workdir = topdir + "/jobs-%s" % jobgroupid

  format_simfile = params["simfile_format"] if "simfile_format" in params else "s%s.m%m.P%P.I%I.n%n.d_%d_%t.slcio"

  format_simdir = params["simdir_format"] if "simdir_format" in params else "/ilc/prod/ilc/mc-opt-3/ild/sim/%E/%C/%m/%s/%t"
  config["simSE"] = "DESY-SRM" if "simSE" not in params else params["simSE"]

  if "sleep_seconds" in params:
     config["sleep_seconds"] = params["sleep_seconds"]

  list_of_detectors = list_of_detectors_input
  if isTest:
    format_simdir = params["simdir_format"] if "simdir_format" in params else "/ilc/prod/ilc/ild/test/mc-opt-3/ild/sim/%E/%C/%m/%s/%t"
    config["simSE"] = "KEK-DISK"
    # config["simSE"] = "DESY-SRM"
    config["number_of_events"] = 10 if "number_of_events" not in params else int(params["number_of_events"])
    workdir = topdir + "/jobs-%s-test" % jobgroupid
    # a_list_of_files = a_list_of_files_test
    # list_of_detectors = [list_of_detectors_input[0]]

  ascript = "ild_upload_addmeta.py"
  # config["jobscripts"] = [ [ascript] ]
  # config["inputSB0"] = [ ascript, config["upload_info"]]
  config["jobscripts"] = [ [ascript, "\""+os.environ["MYPROD_PRODCMD"]+"\"", "\""+os.environ["MYPROD_USERCMD"]+"\"" ] ]
  config["inputSB0"] = [ ascript, config["upload_info"], os.environ["MYPROD_TOOLS"] ]
  if "InputSandBox" in params:
    config["inputSB0"] += params["InputSandBox"]

  ffile = open(a_list_of_files)
  genfiles = ffile.readlines()
  ffile.close()
  print( "### Got a list of input files from " + a_list_of_files )

  '''
  Format of single-genfiles.cvs should be 
  # filename, sub_seq_number, start_from, number_of_events
  filename, sub_seq_number, nb_skip, number_of_events
  '''

  for detector in list_of_detectors:
    config["detector_model"] = detector
    if "extraCLIArguments" in params:
       temparg = params["extraCLIArguments"].replace("%detector_model%", detector)
       config["extraCLIArguments"] += temparg

    (ild, detsub) = detector.split('_',1)
    config["gearfile"] = "gear_%s.xml" % detector
    config["inputSB"] = config["inputSB0"] 

    for line in genfiles:
      if line[0:1] == "#":
        continue
      evtclass = "uds"
      enemachine = "1-calib"
      if jobtype == "UDS":
        genfile = os.path.basename(line[:-1])
        gendir = os.path.dirname(line[:-1])
        (gpref, ftype) = genfile.split('.')
        (gen_process_name, nseqstr) = gpref[1:].split('_')
        genseq = int(nseqstr)
        config["jobname"] = "ddsim-"+gen_process_name+"-"+detsub
        seqvalue = nseqstr

      else:
        if "genfile" in line[:10]:
          continue
        if jobtype == "SelectedPairs":
           (genfile, subseq, startfrom, nevents) = line[:-1].split(',')
           print( genfile )
           filemeta = decodeFilename(genfile)
           nseqstr = filemeta["n"]
           gen_process_name = filemeta["P"]
           enemachine = filemeta["E"]
           gen_process_ID = filemeta["I"]
           print( gen_process_name )
        else:
           (genfile, subseq, startfrom, nevents) = line[:-1].split(',')
           filemeta = decodeFilename(genfile)
           gen_process_name = filemeta["n"]
           nseqstr = filemeta["n"]
 
        genseq = int(nseqstr.replace("n",""))
        seqvalue = "%s_%s" % ( nseqstr, subseq )
        config["ddsim_startfrom"] = startfrom
        if not isTest or int(params["number_of_events"]) < 0 :
          config["number_of_events"] = int(nevents)
        print( "Number of events is " + str(config["number_of_events"]) )
        if jobtype == "Single":
          pdg = gen_process_name.split('_')[1].replace("PDG","")
          momentum = gen_process_name.split('_')[2].replace("MOM","").replace("GeV","")
          simple_prname = gen_process_name.replace("mcparticles_","").replace("MOM","P").replace("GeV","")
          evtclass = "single_PDG%s" % pdg
        elif jobtype == "Resonance":
          ( simple_prname, dummy ) = gen_process_name.split('_',1)
          evtclass = "resonance"
        elif jobtype == "SelectedPairs":
           simple_prname = gen_process_name.replace("Beamstr-pairs_","GP_")
           evtclass = "SelectedPairs"
        else:
          print( "### Fatal Error : jobtype (%s) undefined." % jobtype )
          exit(-1)
        config["jobname"] = "ddsim-"+ simple_prname +'-%s_%s-' % (nseqstr, subseq)+detsub

      config["genfile"] = "LFN:"+gendir+"/"+genfile
      print( "### Input generator file is "+config["genfile"] )
  
      simtoken = {}
      simtoken["s"] = config["ildconfig"]
      simtoken["m"] = config["detector_model"]
      simtoken["E"] = enemachine
      simtoken["C"] = evtclass
      simtoken["P"] = gen_process_name
      simtoken["n"] = seqvalue
      simtoken["d"] = "sim"
      simtoken["t"] = jobgroupid
      if "I" in filemeta:
        simtoken["I"] = filemeta["I"]
      config["simfile"] = makeFilename(format_simfile, simtoken)
      config["simdir"] = makeFilename(format_simdir, simtoken)
  
      jobdir = workdir + "/m%s" % detector + ".P%s" % gpref if jobtype == "UDS" \
        else workdir + "/m%s" % detector + ".P%s" % gen_process_name+".n%s" % simtoken["n"]
      if not os.path.exists(jobdir):
        os.makedirs(jobdir)
  
      os.chdir(jobdir)
      for afile in [ascript]:
        comret = subprocess.Popen("ln -sf %s/%s %s " % (topdir, afile, afile), shell=True, stdout=subprocess.PIPE)
        comret.wait()

  
      ulinfo={}
      ulinfo[config["simfile"]] = {"lfn":'/'.join([config["simdir"], config["simfile"]]), 
  		                 "se":config["simSE"], 
                                 "filemeta":{"GenProcessName":gen_process_name, "SerialNumber":int(genseq)}, 
  		                 "ancestor":'/'.join([gendir, genfile]) }
      if jobtype != "UDS":
        ulinfo[config["simfile"]]["filemeta"].update({"SubSerialNumber":int(subseq), "StartFrom":startfrom})

      json.dump(ulinfo, open(config["upload_info"], "w") )
  
      print( "## Submitting ddsim to process : "+ gen_process_name+".n%s" % simtoken["n"] + " with "+detector )
      if isTest:
        print( "## Following config parameters are given to ILCDirac " )
        pprint.pprint(config)
  
      ildjob = ILDProdUserJob()
      mydirac = DiracILC(True, "job.repo")
      ujob = ildjob.createUserJob(mydirac, config)
      
      res = ildjob.addStepDDSim(ujob, config)
      if not res['OK']:
        print( "Fatal error .. addStepDDSim failed. Return message was " )
        pprint.pprint(res)
        exit(1)

      if not noUpload:
        res = ildjob.addStepScripts(ujob, config)
        if not res["OK"]:
          print( "Fatal error .. addStepScript failed. Return message was " )
          pprint.pprint(res)
          exit(1)
    
      res = ildjob.submitJob(mydirac, ujob, config, isLocal)
      if not res['OK']:
         print( "Fatal error .. submitJob .. return message was " )
         pprint.pprint(res)
         exit(1)

      # del ildjob
   # End of loop on genfile
  # End of loop on detector

######################################################################
def subCalibMarlin(params):
  '''
  A standard function to submit Marlin of UDS or single particle samples.
  Allways run without background overlay

  :param dict params : dict opbjects containig job parameters
   params["jobtype"] : "Single" or "UDS"
   params["isLocal"] : False or True : True to run as local
   params["isTest"]  : False or True : To run a a test job, small number of events and write to a test directory.
   params["noUpload"]: False or True : True for not upload data
   params["jobgroupid"] : jobgroupid, like u025. jobgroupid should be obtained by a command,
                     '/group/ilc/users/miyamoto/optprod/bin/ild-userprod-get-id -m "log_message"
   params["ILDConfig"] : ILDConfig, v01-19-05-p01, for example
   params["Marlin_version" : Marlin version, ILCSoft-01-19-05_gcc49, for example
   params["detector_options"] : A list of detector models, ["ILD_l5_o1_v02", "ILD_s5_o2_v02"]

  Akiya Miyamoto  22 November, 2017
  '''
  # params = {}
  # params["jobtype"] = "UDS"
  # params["isLocal"] = False
  # params["isTest"] = True
  # params["noUpload"] = False

  # params["jobgroupid"] = "u030"
  # params["ILDConfig"] = "v01-19-05-p01"
  # params["Marlin_version"] = "ILCSoft-01-19-05_gcc49"
  ## params["detectors"] = ["ILD_l5_o1_v02", "ILD_s5_o1_v02"]

  jobtype = params["jobtype"]
  isLocal = params["isLocal"]
  isTest  = params["isTest"]
  noUpload = params["noUpload"]
  jobgroupid = params["jobgroupid"]
  ILDConfig = params["ILDConfig"]
  Marlin_version = params["Marlin_version"]
  list_of_options_input = params["detector_options"]

  # Above parameters have to be modified
  ##################################################

  config = {}
  config["job_group_name_format"] = "ILDMarlin"+jobtype+"-%s"
  config["jobname_format"] = jobtype.lower()+"-%s"
  # config["banned_sites"] = ["OSG.BNL.us", "LCG.QMUL.uk", "LCG.Cracow.pl", "LCG.UKI-NORTHGRID-LIV-HEP.uk",
  #                           "OSG.UCSDT2.us"]
  config["banned_sites"] = ["OSG.BNL.us", "LCG.UKI-NORTHGRID-LIV-HEP.uk", "OSG.UCSDT2.us"]
  #     "job_destination" : [""],
  #     "max_cpu_time":,
  config["outputSB"] = [ "*.log", "MarlinStdReco.xml", "MarlinStdRecoParsed.xml", "marlin*.xml", "*.sh", "*.txt", "*.root"]
  config["marlin_steering_file"] = "MarlinStdReco.xml"
  # config["number_of_events"] = 0
  config["upload_info"] = "upload_and_addmeta.json"
  config["configLog"] = "ddsimjob.json"

  config["ildconfig"] = ILDConfig
  config["marlin_version"] = Marlin_version
  config["max_cpu_time"] = 300000 if "max_cpu_time" not in params else params["max_cpu_time"]

  config["job_group_id"] = jobgroupid
  config["job_group_name"] = config["job_group_name_format"] % config["job_group_id"]
  topdir = os.getcwd()
  workdir = topdir + "/jobs-%s" % jobgroupid

  a_list_of_simfiles = "input_simfiles.list"
  a_list_of_options = list_of_options_input
  format_dstfile = "r%r.s%s.m%m.P%P.n%n.d_%d_%t.slcio"
  format_recfile = "r%r.s%s.m%m.P%P.n%n.d_%d_%t.slcio"
  format_recdir = "/ilc/prod/ilc/mc-opt/ild/rec/calib/%C/%m/%r/%t"
  format_dstdir = "/ilc/prod/ilc/mc-opt.dsk/ild/dst/calib/%C/%m/%r/%t"
  config["dstSE"] = "DESY-SRM"
  config["recSE"] = "DESY-SRM"
  if isTest:
    format_recdir = "/ilc/prod/ilc/ild/test/mc-opt/ild/rec/calib/%C/%m/%r/%t"
    format_dstdir = "/ilc/prod/ilc/ild/test/mc-opt.dsk/ild/dst/calib/%C/%m/%r/%t"
    config["dstSE"] = "KEK-DISK"
    config["recSE"] = "KEK-DISK"
    config["number_of_events"] = 10
    a_list_of_simfiles = "input_simfiles-test.list"
    workdir = topdir + "/jobs-%s-test" % jobgroupid

  ascript = "ild_upload_addmeta.py"
  config["jobscripts"] = [ [ascript, "\""+os.environ["MYPROD_PRODCMD"]+"\"", "\""+os.environ["MYPROD_USERCMD"]+"\"" ] ]
  config["inputSB0"] = [ ascript, config["upload_info"], os.environ["MYPROD_TOOLS"] ]

  ffile = open(a_list_of_simfiles)
  simfiles = ffile.readlines()
  ffile.close()
  print( "### Got a list of input files from " + a_list_of_simfiles )

  '''
  a lisf ot simfiles can be generated, for example, by 
    dirac-dms-find-lfns Path=/ilc/prod/ilc/mc-opt/ild/sim "CreationDate>2017/07/10" "EvtClass=uds"
  File name should be a LFN absolute path
  '''

#  temp_cli = " --MyEcalBarrelDigi.Verbosity=MESSAGE "

  for detector_option in a_list_of_options:

    for line in simfiles:
      simfile = os.path.basename(line[:-1]).replace(" ","")
      simdir = os.path.dirname(line[:-1])
      simfile_split = simfile.split('.')
      simfile_for_decode = '.'.join(simfile_split[:-2]+[simfile_split[-2]+"_1", "slcio"])
      simtoken = decodeFilename(simfile_for_decode)
      # simtoken = decodeFilename(simfile.replace("d_sim_u009","d_sim_u009_01")) # Sepcial treatment to avoid error  
      sim_detector = simtoken["m"]
      simdetkeys = simtoken["m"].split("_")
      rec_detector_model = "%s_%s_%s_%s" % ( simdetkeys[0], simdetkeys[1], detector_option, simdetkeys[2])

      gen_process_name = simtoken["P"]
      genpref = gen_process_name
      genseq = simtoken["n"]
       
      config["detector_model"] = rec_detector_model
      (ild, detsub) = rec_detector_model.split('_',1)
      config["jobname"] = "marlin-"+ simtoken["P"]+'-'+detsub
      # config["gearfile"] = "gear_%s.xml" % detector
      # config["siminput"] = [ "LFN:"+line[:-1] ]
      config["inputSB"] = config["inputSB0"] 
      config["siminput"] = "LFN:"+line[:-1].replace(" ","")
      print( "RecModel="+rec_detector_model+" Siminput file is ==="+config["siminput"]+"===" )
  
      evtclass = "uds"
      if jobtype == "UDS":
        filemeta = {"SerialNumber":int(genseq)}
      else:
        process_key = gen_process_name.split('_')
        if jobtype == "Single":
          evtclass = "single_%s" % process_key[1]
        else:
          evtclass = jobtype.lower()
        gensubseq = simtoken["n"].split('_')[1]
        genseq = simtoken["n"].split('_')[0]
        filemeta = {"SerialNumber":int(genseq), "SubSerialNumber":int(gensubseq)}
 

      # ["LFN:"+line[:-1]]
      # extracli = " --global.GearXMLFile=%s " % config["gearfile"]
      #extracli += " --InitDD4hep.DD4hepXMLFile=${lcgeo_DIR}/ILD/compact/%s/%s.xml " % (detector, detector)
      extracli = " --constant.DetectorModel=%s " % rec_detector_model
      if "number_of_events" in config:
        extracli += " --global.MaxRecordNumber=%d " % config["number_of_events"]
  
      config["extraCLIArguments"] = extracli
  
      dsttoken = simtoken
      dsttoken["m"] = rec_detector_model+"_nobg"
      dsttoken["r"] = ILDConfig
      dsttoken["C"] = evtclass
      dsttoken["d"] = "dst"
      dsttoken["t"] = jobgroupid
      config["dstfile"] = makeFilename(format_dstfile, dsttoken)
      config["dstdir"] = makeFilename(format_dstdir, dsttoken)
  
      rectoken = dsttoken
      rectoken["d"] = "rec"
      config["recfile"] = makeFilename(format_recfile, rectoken)
      config["recdir"] = makeFilename(format_recdir, rectoken)
  
      jobdir = workdir + "/m%s" % rec_detector_model + ".P%s" % gen_process_name + ".n%s" % simtoken["n"]
      if not os.path.exists(jobdir):
        os.makedirs(jobdir)
  
      os.chdir(jobdir)
      for afile in [ascript]:
          comret = subprocess.Popen("ln -sf %s/%s %s " % (topdir, afile, afile), shell=True, stdout=subprocess.PIPE)
          comret.wait()
  
      ulinfo={}
      ulinfo[config["dstfile"]] = {"lfn":'/'.join([config["dstdir"], config["dstfile"]]), 
  		                 "se":config["dstSE"], 
                                   "filemeta":{"GenProcessName":genpref}, 
  		                 "ancestor":'/'.join([simdir, simfile]) }
      ulinfo[config["recfile"]] = {"lfn":'/'.join([config["recdir"], config["recfile"]]), 
  			           "se":config["recSE"], 
                                     "filemeta":{"GenProcessName":genpref}, 
  			           "ancestor":'/'.join([simdir, simfile]) }
      ulinfo[config["recfile"]]["filemeta"].update(filemeta)
      ulinfo[config["dstfile"]]["filemeta"].update(filemeta)

      json.dump(ulinfo, open(config["upload_info"], "w") )
  
      print( "## Submitting marlin to process : "+simfile )
      if isTest:
        print( "## Following config parameters are given to ILCDirac" )
        pprint.pprint(config)
  
      ildjob = ILDProdUserJob()
      mydirac = DiracILC(True, "job.repo")
      ujob = ildjob.createUserJob(mydirac, config)
      
      res = ildjob.addStepMarlin(ujob, config)
      if not noUpload:
        res = ildjob.addStepScripts(ujob, config)
    
      res = ildjob.submitJob(mydirac, ujob, config, isLocal)
  
      del ildjob
  
  print( "###### job submission completed. ###########" )

