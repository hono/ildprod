#!/bin/bash 
#
# Download std.out of error jobs and found a type of errors 
# 

dstr=`date +%Y%m%d-%H%M%S`
hostname=`hostname`
prodid=$1

if [ "x${prodid}" == "x" ] ; then 
  echo "ERROR : prodid is not given. Command usage is "
  echo "get_error_joblogs.sh [prod_id]"
  exit 18
fi

echo "### Getting job status of prodid ${prodid} on ${dstr} at host ${hostname}"
jobgroup=`printf "%8.8d" ${prodid}`
jobstatus="jobstatus-${prodid}.log"
dirac-wms-job-status -g ${jobgroup} > ${jobstatus}

nJobs=`cat ${jobstatus} | wc -l`
nDone=`grep Done ${jobstatus} | wc -l`
nRunning=`grep Running ${jobstatus} | wc -l`
nFailed=`grep Failed ${jobstatus} | wc -l`
nComplete=`grep Completed ${jobstatus} | wc -l`
nOthers=`grep -v Done ${jobstatus} | grep -v Running | grep -v Failed | grep -v Completed | wc -l`

echo "${jobstatus} : ${dstr} : ${nJobs}_jobs, ${nDone}_done, ${nRunning}_running, ${nFailed}_failed, ${nComplete}_completed, ${nOthers}_others"


grep Failed ${jobstatus} | cut -d" " -f1 | cut -d"=" -f2 > failed-jobs-all.list
mkdir -p jobout
cat failed-jobs-all.list | while read f ; do 
  (
    if [ ! -e jobout/$f ] ; then 
      echo $f
    fi
  )
done > failed-jobs-toget.list


dstr=`date +%Y%m%d-%H%M%S`
echo "`cat failed-jobs-toget.list | wc -l` failed jobs to download std.out" 
(
  cd jobout
  dirac-wms-job-get-output -f ../failed-jobs-toget.list > get-output-${dstr}.log 2>&1 
)

dstr=`date +%Y%m%d-%H%M%S`
echo "### Completed to get job output of failed jobs of production ${prodid} on ${dstr}"


