'''
ILDProdUserJob.py 

  A utility class for job submission of DDSIM and Marlin production by UserJob

  Akiya Miyamoto  1, March, 2018
'''

from ILDProdUserJob import *

import json 
import datetime
import os
import subprocess
import pprint
import sys

import ILDIDTool

########################################################



def SetDefault():

    params = {}
    params = {"prod":{}, "job":{}, "sim":{}, "rec":{}}
 
    params["prod"]["gendir"] = "/ilc/prod/ilc/mc-opt-3/generated/1-calib_IDR/2f-JER"
    params["prod"]["isLocal"] = False
    params["prod"]["isTest"] = False
    params["prod"]["noUpload"] = False
    params["prod"]["BaseDirectory"] = "/ilc/prod/ilc/ild/test"  if params["prod"]["isTest"] else "/ilc/prod/ilc"
    params["prod"]["topdir"] = os.getcwd()
    params["prod"]["link_file"] = "ild_upload_addmeta.py"

    jobtype = "qqbar"    
    params["job"]["jobtype"] = jobtype
    params["job"]["EvtClass"] = "2f-JER"
    params["job"]["jobgroupid"] = "500585"
    params["job"]["job_group_name"] = "ILD" + jobtype + "-"+params["job"]["jobgroupid"]
    params["job"]["jobname_format"] = jobtype.lower() + "-%s"
    params["job"]["ildconfig"] = "v02-00-02"
    # params["job"]["jobname"] =
    params["job"]["max_cpu_time"] = 300000
    params["job"]["banned_sites"] = ["OSG.BNL.us", "LCG.UKI-NORTHGRID-LIV-HEP.uk", "OSG.UCSDT2.us",
                                   "LCG.SCOTGRIDDURHAM.uk", "LCG.NIKHEF.nl",
                                   "LCG.UKI-SOUTHGRID-RALPP.uk", "LCG.GRIF.fr", "LCG.Manchester.uk",
                                   "LCG.UKI-LT2-IC-HEP.uk", "LCG.Weizmann.il"]
    params["job"]["upload_info"] = "upload_and_addmeta.json"
    params["job"]["inputSB"] = [params["prod"]["link_file"]]
    params["job"]["inputSB"].append(params["job"]["upload_info"])
    params["job"]["inputSB"].append(os.environ["MYPROD_TOOLS"])
    params["job"]["outputSB"] = [ "*.log", "*.py", "*.sh", "*.txt", 
            "MarlinStdReco.xml", "MarlinStdRecoParsed.xml", "marlin*.xml", "*.root" ]
    params["job"]["jobscripts"] = [ ["ild_upload_addmeta.py", 
                  "\""+os.environ["MYPROD_PRODCMD"]+"\"", "\"" + os.environ["MYPROD_USERCMD"] + "\"" ] ]
    params["job"]["configLog"] = "subjob.json"
    
    params["prod"]["jobdir"] = os.getcwd()+"/jobs-%s" % params["job"]["jobgroupid"]

    # Input for DDSim
    params["sim"]["ddsim_version"] = "ILCSoft-02-00-02_gcc49"
    params["sim"]["ILDConfig"] = "v02-00-02"
    params["sim"]["detector_model"] = "ILD_s5_v02"
    params["sim"]["ddsim_steering_file"] = "ddsim_steer.py"
    params["sim"]["simSE"] = "DESY-SRM"
    params["sim"]["number_of_events"] = 0
    params["sim"]["extraCLIArguments"] = " --crossingAngleBoost 0.0 "

    # Input for Marlin
    params["rec"]["marlin_version"] = "ILCSoft-02-00-02_gcc49"
    params["rec"]["ILDConfig"] = "v02-00-02" 
    params["rec"]["marlin_steering_file"] = "MarlinStdReco.xml"
    params["rec"]["detector_model"] = "ILD_s5_o1_v02"
    params["rec"]["model_name"] = "ILD_s5_o1_v02_nobg"
    params["rec"]["recSE"] = "DESY-SRM"
    params["rec"]["dstSE"] = "DESY-SRM"
    params["rec"]["extraCLIArguments"] = " --global.MaxRecordNumber=0 --constant.DetectorModel=%s " % params["rec"]["detector_model"]
 
    if params["prod"]["isTest"]:
        params["sim"]["simSE"] = "KEK-DISK"
        params["rec"]["recSE"] = "KEK-DISK"
        params["rec"]["dstSE"] = "KEK-DISK"
        params["sim"]["number_of_events"] = 20

    # pprint.pprint(params)

    return params


# #######################################################
def fillParams(params, line):
    ''' Create param for one job. '''

    par = params
    par["readline"] = line.replace("\n","")
    (genfile, subser, nstart, nevents,jdir) = line.replace("\n","").split(",")

    genmeta = decodeFilename(genfile)
    format_simfile = "s%s.m%m.E%E.I%I.P%P.e%e.p%p.n%n.d_sim_%t.slcio"
    format_simdir = "%B/mc-opt-3/ild/sim/%E/%C/%m/%s/%t/%J"
    format_dstfile = "r%r.s%s.m%m.E%E.I%I.P%P.e%e.p%p.n%n.d_dst_%t.slcio"
    format_recfile = "r%r.s%s.m%m.E%E.I%I.P%P.e%e.p%p.n%n.d_rec_%t.slcio"
    format_recdir = "%B/mc-opt-3/ild/rec/%E/%C/%m/%r/%t/%J"
    format_dstdir = "%B/mc-opt.dsk/ild/dst/%E/%C/%m/%r/%t/%J"

    nser = int(genmeta["n"])
    genmeta["B"] = params["prod"]["BaseDirectory"]
    genmeta["t"] = str(par["job"]["jobgroupid"])
    genmeta["n"] += "_%d" % int(subser)    
    genmeta["C"] = params["job"]["EvtClass"]
    genmeta["J"] = jdir

    par["sim"]["genfile"] = "LFN:"+params["prod"]["gendir"] + "/" + genfile
    par["sim"]["ddsim_startfrom"] = int(nstart) 
    par["sim"]["number_of_events"] = par["sim"]["number_of_events"] if params["prod"]["isTest"] else int(nevents)

    simmeta = genmeta
    simmeta["s"] = par["sim"]["ILDConfig"]
    simmeta["m"] = par["sim"]["detector_model"]
    par["sim"]["simfile"] = makeFilename(format_simfile, simmeta)
    par["sim"]["simdir"] = makeFilename(format_simdir, simmeta)

    recmeta = genmeta 
    recmeta["r"] = par["rec"]["ILDConfig"]
    recmeta["m"] = par["rec"]["model_name"]
    par["rec"]["recfile"] = makeFilename(format_recfile, recmeta)
    par["rec"]["recdir"] = makeFilename(format_recdir, recmeta)
    par["rec"]["dstfile"] = makeFilename(format_dstfile, recmeta)
    par["rec"]["dstdir"] = makeFilename(format_dstdir, recmeta)
    par["rec"]["siminput"] = par["sim"]["simfile"]

    upload = {}


    
# ################################################################################################
# Comment out to output only DST file
#
#    upload[par["sim"]["simfile"]] = {"lfn":'/'.join([par["sim"]["simdir"], par["sim"]["simfile"]]),
#         "se":par["sim"]["simSE"],
#         "filemeta":{"GenProcessName":genmeta["P"], 
#                     "SerialNumber":nser, "SubSerialNumber":int(subser)}, 
#         "ancestor":par["sim"]["genfile"].replace("LFN:/","/") }
#    upload[par["rec"]["recfile"]] = {"lfn":'/'.join([par["rec"]["recdir"], par["rec"]["recfile"]]),
#         "se":par["rec"]["recSE"],
#         "filemeta":{"GenProcessName":genmeta["P"], 
#                     "SerialNumber":nser, "SubSerialNumber":int(subser)}, 
#         "ancestor":upload[par["sim"]["simfile"]]["lfn"].replace("lfn:/","/") }
# ##################################################################################################

    upload[par["rec"]["dstfile"]] = {"lfn":'/'.join([par["rec"]["dstdir"], par["rec"]["dstfile"]]),
         "se":par["rec"]["dstSE"],
         "filemeta":{"GenProcessName":genmeta["P"], 
                     "SerialNumber":nser, "SubSerialNumber":int(subser)}}

    if par["sim"]["simfile"] in upload:
        upload[par["rec"]["dstfile"]]["ancestor"] = upload[par["sim"]["simfile"]]["lfn"].replace("lfn:/","/")
    else:
        upload[par["rec"]["dstfile"]]["ancestor"] = par["sim"]["genfile"].replace("LFN:/","/")


    par["upload"] = upload
   
    jobdet = par["sim"]["detector_model"].replace("ILD_","")
    par["prod"]["workdir"] = par["prod"]["jobdir"] + \
        "/%s/m%s.P%s.n%s" %  ( genmeta["J"], jobdet, genmeta["P"], genmeta["n"] )
    par["job"]["jobname"] = "simrec-m%s-P%s-n%s" % ( jobdet, genmeta["P"], genmeta["n"] )

    return par



# #############################################################################
def subSimRec(params):
  '''
  A standard function to submit do DDSim and Marlin and output DST

  Akiya Miyamoto  28 Feburary, 2019 Prepared for Sim+Rec prodution of qqbar samples.
  '''

  # config = {}
  # for step in ["job", "sim", "rec"]:
  #    for k, v in params[step].items():
  #        config[k] = v

  config = params

  ildjob = ILDProdUserJob()
  mydirac = DiracILC(True, "job.repo")
  ujob = ildjob.createUserJob(mydirac, config["job"])

  res = ildjob.addStepDDSim(ujob, config["sim"])
  if not res['OK']:
      print( "Fatal error .. addStepDDSim failed. Return message was " )
      pprint.pprint(res)
      exit(1)

  res = ildjob.addStepMarlin(ujob, config["rec"])
  if not res['OK']:
      print( "Fatal error .. addStepMarlin failed. Return message was " )
      pprint.pprint(res)
      exit(1)

  if not config["prod"]["noUpload"]:
      res = ildjob.addStepScripts(ujob, config["job"])
      if not res["OK"]:
          print( "Fatal error .. addStepScript failed. Return message was " )
          pprint.pprint(res)
          exit(1)

  isLocal = config["prod"]["isLocal"]
  res = ildjob.submitJob(mydirac, ujob, config["job"], isLocal)
  if not res['OK']:
     print( "Fatal error .. submitJob .. return message was " )
     pprint.pprint(res)
     exit(1)

  # pprint.pprint(config)


# ######################################
def do_submit(infile):

    # infile="input_files_test.csv"
    nbfile=0
    for line in open(infile):

        params = SetDefault()
        nbfile +=1
        print( "%4.4d-th line : %s " % ( nbfile, line ) )
        if line[0:1] == "#":
            continue
       
        if os.path.exists("stop"):
           print( "do_submit stopped, because a file, stop, exists." )
           return {"Status":"Stopped", "Message":"Stopped before a line, %s " % ( line )} 

        par = fillParams(params, line)
        # pprint.pprint(par)

        curdir = os.getcwd()
        workdir=par["prod"]["workdir"]
        workdir_dir=os.path.dirname(workdir)
        if not os.path.exists(workdir_dir):
           os.makedirs(workdir_dir)
        if os.path.exists(workdir):
           msg = "%s exists in %s. Remove it first." % ( os.path.basename(workdir), workdir_dir )
           print( msg )
           return {"Status":"Failed", "Message":msg}

        if not os.path.exists(workdir):
           os.makedirs(workdir)
        os.chdir(workdir)

        for afile in [par["prod"]["link_file"]]:
            cmd = "ln -sf %s/%s %s " % ( par["prod"]["topdir"], afile, afile )
            # print cmd
            comret = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
            comret.wait()
        json.dump(par["upload"], open(par["job"]["upload_info"], "w"))
        json.dump(par, open("subpar.json","w"))

        subSimRec(par)

        os.chdir(curdir)

    print( "### do submit completed." )
    return {"Status":"OK", "Message":"completed."}

# ######################################
if __name__ == '__main__':

    infile="input_files.csv"
    if len(sys.argv) > 1:
       infile=str(sys.argv[1])
    print( "### input file list is "+infile )

    ret = do_submit(infile)

    print( "### End of do_submit: Status=%s, Message=%s" % ( ret["Status"], ret["Message"] ) )


