#!/bin/bash
#
# A template of init_production script, followed by detail help 
# of init_production.py arguments 
#
#############################################################################
#
#
#  Create a script for a production.
#  Variables of type, __xxxxxx__ are production dependant parameters.
#

# A set of directories are created under ${proddir}/<sort_key>:<subdir>
#
prodname=__dirname__
proddir=${PRODTASKDIR}/${prodname}

if [ ! -e ${proddir} ] ; then
  mkdir -v ${proddir} || my_abort "Failed to create directory"
fi

# ###########################################################
# Format of xxx-list.txt file.
# <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
# More than one line can be specified to create multiple-directories in ${proddir}.
# Note that each line corresponding to the one set of production, one elog entry.
#############################################################
cat > ${prodname}-list.txt <<EOF
__sort_key1__:__subdir1__
EOF

banned_sites=`joinlines banned_sites.txt`
# banned_sites.txt is a list of banned sites name

cmd="init_production.py --workdir ${proddir} \
     --prodlist ${prodname}-list.txt \
     --excel_file ${excel_file} \
     --split_nseq_from <split_nseq_from>  \
     --production_type <production_type>  \
     --sim_nbtashs <sim_nbtasks>  --rec_nbtasks <rec_nbtasks> \
     --sim_banned_sites ${banned_sites} --rec_banned_sites ${banned_sites} \
     ... other options ...
    "

echo ${cmd}

echo " "
${cmd} \
  || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
  && mv -v ${prodname}-list.txt ${proddir}

###############################################################################
###############################################################################


# ##########################################
# input parameters for a script, init_production.py
# ##########################################

--prodpara_excel : A file name of prodpara excel file

--production_type : type of production(s). Possible values are
                   "sim"  for simulation
                   "nobg" for reconstruction without background overlay
                   "ovl"  for reconstruction with background overly
                   "sim:ovl"  for "sim" and "ovl"
                   "sim:nobg" for "sim" and "nobg"

--sim_nbtasks : 
--rec_nbtasks : Initial number of tasks of transformation.
		A value between 10 t0 100 is recommended to check at the begining of the production.
		If checking is not necessary, 1000 to 2000 is recommended. If not given, not limit
		is applied, namely jobs are created by transformation as long as new input file is found.
		The number of task can be increased later manually by a command, dirac-ilc-add-tasks-to-prod

--sim_banned_sites:
--rec_banned_sites: Comma separated list of sites, to be omitted from a list of sites eligible for job submission.

--gensplit_numprocs : Number of paralle processes for gensplit upload and setmeta. Default=8

# ###################################
# Other optional parameters
# ###################################

--dstonly
--recrate <tate>
           : If not given, reconstruction job output both DST and REC files.
            If given, output only DST files, unless --recrate is given. If --recrate
            is given, two transformations are created, one to output both REC and DST(recdst transformation),
            the other output only DST(dstonly transformation). SelectedFile MetaValue is attached to
            Simulated files. If it is 2 processed by RECDST transformation, 3 by DSTONLY transormation.
            SelectedFile MetaValue is attached by a script, setselectedfile.sh, which is executed
            at the end of task_step4.sh.

--delfiles <delfiles>
           : Comma separated list of file types, gensplit,sim,dst. Files of specified file types are
           removed automatically at the end of 6_dstmerge steps. Note that both dst-merged and dst files
           are removed if "dst" is specified.

# examples of these options depending on which files are kept.
(1) keep SIM/DST, but 10% of REC files, creating 2 reconstruction transformation, one keep REC, other not keep REC.
    --dstonly --recrate 0.10 
(2) Keep DST, but do not save SIM/REC
    --dstonly --recrate -1 --delfiles sim 
   1 reconstruction transformation is created. SIM files are removed at the end of DSTMerge process

--mcprod_config : A file for 4_subprod/ThisProduction.py to over-write mcprod_config.py, which defines 
                  ILCSoft/ILDConfig version, overlay background, etc.

--ngenfile_max : Max number of genenerated files to be used for gensplit
--split_nseq_from : Sequence number of the first generated file for gensplit

--sim_models : detector model for simulation ( or : separated list of it ) Default is ILD_l5_o2
--rec_options : options for reconstruction ( or : separated list of it ) Default is o1

--xml_serial <XML_SERIAL>
           : Integer number attached to the name of transformation ( a number attached to a xml file for
           transformation ). The transformation name should be uniq within ILCDirac sustem.

--multi_dir : Input data for gensplit ( generated data ) from multiple directories are used.

--elogid_old : Elog ID referenced, where past production information is obtained
--reuse_gensplit : Use this gensplit_id for new simproduction

# ####################################
# Options for test production
# ####################################
--test : Run with test production mode. Output data is written to the directory defined by
         PRODTESTDATA environment parameter

--noelog : Do not write information to ELOG

--se_for_data : Output SE for Sim, Rec, DST, DSTMerged files. Log files are DESY-SRM
--se_for_sim :  Output SE for SIM. Overwrite se_for_data setting
--se_for_rec :  Output SE for REC. Overwrite se_for_data setting
--se_for_dstmerged : Output SE for DSTMerged. Overwrite se_for_data setting
--se_for_gensplit  :  SE for gensplit files.
--se_for_logfiles  :  SE for log files.
--se_for_dstm_replicates : SE(s) where to replicates DST-Merged files. "," separated list for multiple SEs (untested).

--nw_perfile : number of events per gensplit file
--split_nbfiles : Number of gensplit files to be created from generated file. Only initial part of
                  an input generated file is used.


################################################################################
# do "init_production.py -h" for a complete list of options.
################################################################################
