#!/usr/bin/env python 
#
# Cleanup DST merge job results
#

from __future__ import print_function
import os
import subprocess
import json
import argparse
import pprint
import glob


if __name__ == '__main__':

  parser = argparse.ArgumentParser(description="Check status of DSTM jobs and do remaning tasks")
  parser.add_argument("topdir", help="Top directory of job directories")
  parser.add_argument("-g", help="glob string to search job directories", dest="glob_string", action="store", default="dstm.*")
  parser.add_argument("--test", help="test run mode, upload to test directory", dest="doDebug", action="store_true", default=False)
  parser.add_argument("-v", help="verbercity", dest="verb", action="store_false", default=True)



  args = parser.parse_args()
  topdir = args.topdir 
  origdir = os.getcwd()
  print( '### Checking job directories, in %s/%s ' % ( topdir, args.glob_string ) )
  verbercity = 'debug' if args.verb else 'none'

  if not os.path.exists(topdir):
      print( "### ERROR : %s does not exist" % topdir )
      exit(10)

  os.chdir(topdir)
  jobdirs = glob.glob(args.glob_string)
  len_jobdirs = len(jobdirs)

  nsub = 0
  jobinfo = {}
  jobid_to_jobdir = {}
  curdir = os.getcwd()
  for jobdir in jobdirs:
    os.chdir(jobdir)
    if os.path.exists("submit.json"):
      nsub +=1
      subinfo = json.load(open("submit.json"))
      jobid = subinfo['submit']['jobid']
      jobid_to_jobdir[jobid] = jobdir
      jobinfo[jobdir] = { 'jobid':subinfo['submit']['jobid'] }

    os.chdir(curdir)

  if nsub != len_jobdirs:
    print( 'There are %d job dirs, but only %d submitted.' % ( nsub, len_jobdirs ) )
    print( 'Proceed after all jobs are submitted ' )
    exit(10)

  # ###############################
  jobids = ""
  for jobdir in jobdirs:
    jobids += str(jobinfo[jobdir]['jobid'])+' '
  print( '### executing dirac-wms-job-status ' )
  cmd = 'dirac-wms-job-status ' + jobids
  cmdret = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
  nDone = 0
  for line in cmdret.split('\n'):
    sep1 = line.split(';')
    if len(sep1) < 3:
      continue
    if verbercity == 'debug':
      print( line )
    (data1, data2) = sep1[0].split(' ',1)
    (dummy, jobid) = data1.split('=')
    (dummy, status) = data2.split('=')
    jobinfo[jobid_to_jobdir[int(jobid)]]['status']=status
    if status == 'Done':
      nDone += 1
  
  if nDone != len_jobdirs:
    print( 'Only '+str(nDone)+ ' job completed, out of '+str(len_jobdirs)+ ' jobs' )
    print( 'Further processing is stopped.' )
    exit(9)

  # =====================================================
  # Output and delete output
  curdir = os.getcwd()
  for jobdir in jobdirs:
    if verbercity == 'debug':
      print( '-- job output in '+jobdir )
    os.chdir(jobdir)
    jobdata = jobinfo[jobdir]
    cmd1 = 'dirac-wms-job-get-output %d' % jobdata['jobid']
    print( cmd1 )
    out1 = subprocess.check_output(cmd1, shell=True, stderr=subprocess.STDOUT)
    print( out1 )
    cmd3 = 'tail -100 '+str(jobdata['jobid'])+'/marlin_lcio_merge.log | grep \' written to file \' '
    out3 = subprocess.check_output(cmd3, shell=True, stderr=subprocess.STDOUT)
    outsplit = out3.split(' ')
    jobinfo[jobdir]['nevents'] = outsplit[6]
    jobinfo[jobdir]['filename'] = outsplit[15].replace('\n','')
    os.chdir(curdir)
    
  # pprint.pprint(jobinfo) 

  # ======================================================
  # Output summary of DST-Merged files
  os.chdir(origdir)
  os.chdir(topdir)
  jobmakers = glob.glob('dstm-jobmaker-*.json')
  jobmaker = json.load(open(jobmakers[0]))
  detector = jobmaker['dstm']['meta']['DetectorModel']
  prodid  = jobmaker['dstm']['meta']['ProdID']
  lines = []
  for jobdir, jobdata in jobinfo.items():
    lines += ['%s %s events' % (jobdata['filename'], jobdata['nevents'])]

  dstm_summary = 'nevents-in-dstmerged-%s-%s.txt' % (str(prodid), detector)
  fout = open(dstm_summary, 'w')
  fout.write('\n'.join(lines))
  fout.close()
    
  # ======================================================
  # Write files to final clean up, delete jobs and upload log files.
  bash_cmd1 = ['#!/bin/bash', 
               '# should be executed by ilc-user role.'] 
  for jobdir in jobdirs:
    bash_cmd1 += ['dirac-wms-job-delete %s' % str(jobinfo[jobdir]['jobid']) ]
  bash_cmd1_file = 'delete-jobout.sh'
  fout=open(bash_cmd1_file,'w')
  fout.write('\n'.join(bash_cmd1))
  fout.close()
  os.chmod(bash_cmd1_file, 0o755)

  # ======================================================
  # cmd to run by prod role
  dstmlog_dir = jobmaker['conf']['__DIRECTORY_RECLOG'].replace('reclog','dstmlog')
  # dstmlog_se = jobmaker['dstm']['jobsetup']['_output_dstm_se']
  dstmlog_se = jobmaker["conf"]["__LOG_SE"] if "__LOG_SE" in jobmaker["conf"] else "DESY-SRM"

  print( '### dstm-log upload dir : '+dstmlog_dir )
  print( '### dstm-log upload se : '+dstmlog_se )

  os.chdir(origdir)
  bash_upload = 'upload-%s.sh' % topdir
  bash_cmd2 = ['#!/bin/bash',
               '# exec with dirac-prod role ', 
               ' if [ ! -e %s.tar.gz ] ; then ' % topdir,
               '   tar zcf %s.tar.gz %s' % (topdir, topdir),
               ' fi ',
               ' diracproxy=`dirac-proxy-info | grep \'DIRAC group\' | grep -c ilc_prod `',
               ' if [ ${diracproxy} -eq 0 ] ; then ',
               '   echo "proceed after switching to production role" ',
               '   exit 2',
               ' fi ',
               ' dirac-dms-add-file %s/%s.tar.gz %s.tar.gz %s' % (dstmlog_dir, topdir, topdir, dstmlog_se)
              ] 
  fout=open(bash_upload,'w')
  fout.write('\n'.join(bash_cmd2))
  fout.close()
  os.chmod(bash_upload, 0777)

    
  # ======================================================
  print( '### script completed ########################################## ' )
  print( '%s/%s and %s are created ' % ( topdir, bash_cmd1_file, bash_upload ) )
  print( 'With ilc_user role, do : %s/%s' % ( topdir, bash_cmd1_file) )
  print( 'Then with ilc_prod role, do : %s' % bash_upload )
   
