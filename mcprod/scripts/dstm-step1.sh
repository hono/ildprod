#!/bin/bash

proddir="../4_subprod"

if [ "x${DIRACOS}" == "x" ] ; then
  echo "DIRAC environment is not initialized yet."
  echo "Execute after dirac-user proxy is initialized"
  exit 10
fi

scripts_dir=${SCRIPTS_DIR}
if [ "x${scripts_dir}" == "x" ] ; then 
   echo "### ERROR : SCRIPTS_DIR environment parameter does not exist. Probably production.sh is not sourced yet."
   exit 
fi

if [ -e "dstm-step1.opt" ] ; then 
. dstm-step1.opt
(
echo "### default parameters in dstm-step1.sh was modified as follows"
cat dstm-step1.opt
echo "#########################"
) | tee -a ${logf}
fi

logf="dstm-step1.log"
if [ -e ${logf} ] ; then 
   echo "dstm-step1.sh is running already. Wait job completion" 2>&1 | tee -a ${logf}
   exit 9
fi


files=(ILD_DSTM_Job_Maker.py ILD_DSTM_Submit.py dstm-after-job-finish.py)
for f in ${files[*]}; do
   if [ ! -e $f ] ; then
     ln -sv ${scripts_dir}/${f} . 2>&1 | tee ${logf}
   fi
done

echo "### Get production job information from ${proddir} " | tee -a ${logf}

jsons=( `cd ${proddir} && /bin/ls recprod_*.json | grep -v "recprod_12345_"` )
for f in ${jsons[*]}; do
   if [ ! -e $f ] ; then 
     ln -sv ${proddir}/$f . 2>&1 | tee -a ${logf}
   fi
done

option=
for js in `/bin/ls recprod_*.json | grep -v "_12345_"` ; do
  logfile=`echo $js | sed -e "s/recprod_/jobmaker-/" -e "s/\.json//"`
  echo “Start $logfile“ 2>&1 | tee -a ${logf}
  testflag="--noTest"
  if [ "x${WEBTEST}" == "xYes" ] ; then
      testflag=" "
  fi
  if [ -e dstm_job_maker.opt ] ; then
    echo "A file, dstm_job_maker.opt, exists. The file is sourced for additional option patameter setting." | tee -a ${logf}
    cat dstm_job_maker.opt | tee -a ${logf}
    . dstm_job_maker.opt
  fi

  echo "Following command will be executed:" 2>&1 | tee -a ${logf}
  echo "python ILD_DSTM_Job_Maker.py ${testflag} --recjson ${js} ${option} > ${logfile}.log 2>&1 & " 2>&1 | tee -a ${logf}
  echo "Once dst files are scanned, dstm-job scripts can be re-created by following command for a example " 2>&1 | tee -a ${logf}
  echo "python ILD_DSTM_Job_Maker.py --noTest --scanjson dstm-scan-9626-l5_o1_v02_nobg.json --dstm_size 1500 " 2>&1 | tee -a ${logf}
  # python ILD_DSTM_Job_Maker.py ${testflag} -f --recjson ${js} ${option} 2>&1 | tee -a ${logfile}.log | tee -a ${logf}
  python ILD_DSTM_Job_Maker.py ${testflag} -f --recjson ${js} ${option} > ${logfile}.log 2>&1 &
  echo "Start ${logfile} was submitted : `date` " 2>&1 | tee -a ${logf}
done

sleep 5
ps -ef | grep ILD_DSTM_Job_Maker.py 2>&1 | tee -a ${logf}

echo 'dstm-step1.sh completed. Wait completion of ILDS_DSTM_Job_Maker, then do dstm-step2.sh for UserJob submission' 2>&1 | tee -a ${logf}
exit 0
