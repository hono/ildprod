#!/bin/bash 
# 
# Do gensplit, upload, and addmeta.
# TO be executed in the directory, 3_gensplit/IXXXXX.YYY
# 
#  Configuration file, gensplit.opt, may include following parameters.
#
#  config_json=../production.json  # Path to the json file of production parameters
#  procid=<process_id>
#  fileseq=<generator_file_sequence>
#  numprocs=<number_of procces to run in parallel>
#
#  nw_per_file, default=json["__SPLIT_NW_PER_FILE"]
#  maxread, default=json["__SPLIT_MAX_READ"]
#  nskip = json["__SPLIT_SKIP_NEVENTS"]
#  nbfiles = json["__SPLIT_NB_FILES"]
#  datadir = json["__STDHEP_LOCALBASE"]
#  filename, if present procid and fileseq is delived from the file name.
#
# 
#  command format 
#  gensplit-upload-addmeta.sh -c <option_file> -j <config_json>
# 
#  <option_file> = gensplit.opt
#  <config_json> = ../production.json ( production parameter json file. )
#

option_file=gensplit.opt
config_json=../production.json






echo started `hostname` `date` 
ln -sf ../production.json . 
nw_per_file=`jsonread production.json __SPLIT_NW_PER_FILE`
maxread=`jsonread production.json __SPLIT_MAX_READ`
nskip=`jsonread production.json __SPLIT_SKIP_NEVENTS`
nbfiles=`jsonread production.json __SPLIT_NB_FILES`
datadir=`jsonread production.json __STDHEP_LOCALBASE`
numprocs=3
if [ x == xundef ] ; then datadir=undef ; fi 
echo "gensplit.opt can be used to set nrec_per_file, maxread, nskip dependently on input file." 
if [ -e gensplit.opt ] ; then . gensplit.opt ; fi
Stdhep2LcioSplit 106519 001 -n ${nw_per_file} -m ${maxread} -s ${nskip} -N ${nbfiles} -d ${datadir} 2>&1 | tee gensplit.log
split_begin=`jsonread lciosplit.json FileSplitNumberFirst` 
split_last=`jsonread lciosplit.json FileSplitNumberLast` 
echo spit_begin=${split_begin}  split_last=${split_last} 
setMetaGenSplit -i 1
seq ${split_begin} ${split_last} | xargs -P ${numprocs} -I{} setMetaGenSplit {} 2>&1 | tee setmeta.log 
echo " ### gensplit completed `hostname` `date` " 
