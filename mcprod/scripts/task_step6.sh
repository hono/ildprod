#!/bin/bash
#
# A script for step6, create dst-merged files
#

source ${SCRIPTS_DIR}/task_step6_lib.sh

###########################################################################
####  Main part of the script
###########################################################################

check_status || exit 20

## dispatcher
[[ ! -e stdir ]] && mkdir -p stdir

st1=`get_status step1`
case "${st1}" in 
  "___")
    my_echo "dstm-step1.sh will be executed."
    touch stdir/step1.running
    dstm-step1.sh 2>&1 | tee -a ${stfile}
    ( cd stdir && rm -f step1.running )
    recprod=`/bin/ls recprod_*.json 2>/dev/null | head -1`
    if [ "x${recprod}" == "x" ] ; then
       my_echo "after dstm-step1.sh, recprod_...json not found. Something is wrong. "
       touch stdir/step1.error
       exit 23
    fi
    has_elog ${recprod} && putelog.py -c ${recprod} dstm_start 2>&1 | tee -a ${stfile}
    touch stdir/step1.done ;;
  "E__") 
    my_echo "dstm-step1.sh had failed with error" ; exit 21 ;;
  "__R") 
    my_echo "Detect that dstm-step1.sh is running" ; exit 21 ;;
  "EDR"|"E_R"|"_DR"|"ED_") 
    my_echo "Got illegal status of ${st1} in step1" ; exit 22 ;;
esac   

######################################################################
# test step2 readiness
is_step2_ready || exit 18

# start step2
st2=`get_status step2`
case "${st2}" in 
  "___") 
    my_echo "dstm-step2.sh will be executed." 
    recprod=`/bin/ls recprod_*.json 2>/dev/null | head -1`
    has_elog ${recprod} && {
      elogid=`jsonread ${recprod} conf __ELOG_ID`
      putelog.py -i ${elogid} -m "Submitting UserJobs to merge DSTs" text | tee -a ${stfile}
    }
    touch stdir/step2.running
    dstm-step2.sh 2>&1 | tee -a ${stfile}
    ( cd stdir && rm -f step2.running )
    stfail=`tail -10 dstm-step2.log | grep  "### FAILED to submit job"`
    stfail_cnt=`tail -50 dstm-step2.log | grep  "### FAILED to submit job" | wc -l`
    if [ "x${stfail}" != "x" ] ; then
      if [ ${stfail_cnt} -gt 10 ] ; then 
         my_echo "### More than 10 DSTM Submit fails. Somethin is wrong in dstm-submittion" | tee -a ${stfile}
         touch stdir/step2.error
         exut 17
      fi
      my_echo "### Submit failure found in dstm-step2.log. Will retry later" | tee -a ${stfile}
      exit 16
    fi
    sterror=`tail -10 dstm-step2.log | grep "### ERROR in dstm"`
    if [ "x${sterror}" != "x" ] ; then
      my_echo ${sterror} 
      my_echo "### Error found in dstm-step2.log. !!! Fix the error interactively!!!" | tee -a ${stfile}
      touch stdir/step2.error
      exit 17
    fi
    has_elog ${recprod} && {
      putelog.py -i ${elogid} -m \
        "DST merge job submission has completed. Waiting job finish for downloding job outputs." text | tee -a ${stfile}
    } 
    touch stdir/step2.done ;;
  "__R") my_echo "Detect that dstm-step2.sh is still running" ; exit 16 ;;
  "E__") my_echo "dstm-step2.sh had failed with error. Fix error and remove stdir/step2.error to restart." ; exit 15 ;;
  "EDR"|"E_R"|"ED_"|"_DR") 
         my_echo "Got illegal status of ${st2} in step2" ; exit 14 ;;
esac  

######################################################################
# test step3 readiness
is_step3_ready || exit 13

st3=`get_status step3`
case "${st3}" in 
  "___") 
    my_echo "dstm-step3.sh will be executed."
    touch stdir/step3.running
    dstm-step3.sh 2>&1 | tee -a ${stfile}
    ( cd stdir && rm -f step3.running )
    stword=`grep "### Some error happens while updating ELOG record " dstm-step3.log`
    if [ "x${stword}" != "x" ] ; then 
      touch stdir/step3.error
      exit 12
    fi
    stword=`grep "### All dstm-step3.sh completed." dstm-step3.log`
    if [ "x${stword}" != "x" ] ; then 
       touch stdir/step3.done
    fi
    ;;
  "__R") my_echo "Detect that dstm-step3.sh is still running. Wait running one to complete." ; exit 11 ;;
  "E__") my_echo "dstm-step3.sh had failed with error" ; exit 10 ;;
  "EDR"|"E_R"|"ED_"|"_DR") 
         my_echo "Got illegal status of ${st2} in step3" ; exit 9 ;;
esac 

is_replication_ready || exit 8 

########################################################################### 
# replicate
strep=`get_status rep_sub`
case "${strep}" in 
  "___")
     my_echo "dstm-replicate.sh will be executed."
     touch stdir/rep_sub.running
     dstm-replicate.sh 2>&1 | tee -a ${stfile}
     ( cd stdir && rm -f rep_sub.running )
     stword=`grep "### Complete dstm-replicate.sh" dstm-replicate.log 2>/dev/null`
     if [ "x${stword}" == "x" ] ; then
        my_echo "ERROR: dstm-replicate.sh does not complete though command returns."
        touch stdir/rep_sub.error
        exit 8
     fi 
     stword=`grep "### ERROR when creating create-replication-request" dstm-replicate.log 2>/dev/null`
     if [ "x${stword}" != "x" ] ; then
        my_echo "ERROR: dstm-replicate.sh. Some error when creating replication request."
        touch stdir/rep_sub.error
        exit 8
     fi 
     touch stdir/rep_sub.done
     ;;
  "__R") my_echo "Detect that dstm-replicate is running. Wait running one to complete." ; exit 7 ;;
  "E__") my_echo "dstm-replicate.sh had failed with error" ; exit 6 ;;
  "EDR"|"E_R"|"ED_"|"_DR") 
         my_echo "Got illegal status of ${strep} in dstm-replicate-submit " ; exit 5 ;;
esac 

    
done_replicate || exit 4

#########################################################################
# Send request to remove files.
#########################################################################
stdel=`get_status delrequest`
case "${stdel}" in
  "___")
     touch stdir/delrequest.running
     dellist 2>&1 | tee -a delfiles.log
     ndel=`cat delfiles.list | wc -l `
     if [ ${ndel} -ne 0 ] ; then
        create_removal_request | tee -a delfiles.log
        if [ `cat remove-request.cmd | wc -l` -ne 0 ] ; then
           if [ ! -e remove-request.skip ] ; then 
             . ${MYPROD_PRODPROD} 
             source  remove-request.cmd 2>&1 | tee -a remove-request.log
             grep "Request" remove-request.log | grep "Submitted" | cut -d" " -f2 > delfiles-request-ids.list
           fi 
        fi 
     fi
     ( cd stdir && rm -f delrequest.running )
     touch stdir/delrequest.done
     ;;
  "__R") my_echo "Detect that delrequest is creating request. Wait running one to complete." ; exit 7 ;;
  "E__") my_echo "delrequest had failed with error" ; exit 6 ;;
  "EDR"|"E_R"|"ED_"|"_DR")
         my_echo "Got illegal status of ${strep} in delrequest " ; exit 5 ;;
esac

#########################################################################
# Check completion of removal request
#########################################################################
if [ -e remove-request.log ] ; then 
  done_delfiles || exit 4
fi

#########################################################################
# Upload dstm job logs.
#########################################################################
stlog=`get_status log_make`
case "${stlog}" in 
  "___") 
    my_echo "Creating dstm job log files" 
    touch stdir/log_make.running
    dstm_make_log 2>&1 | tee -a ${stfile}
    ( cd stdir && rm -f log_make.running )
    if [ ! -e logupload-cmd.sh ] ; then 
      touch stdir/log_make.error
      my_echo "ERROR : logupload-cmd.sh does not exist after dstm_make_log " 
      exit 4 
    fi
    touch stdir/log_make.done
    ;;
  "__R") my_echo "Detect that dstm log_maker is running. Wait running one to complete." ; exit 4 ;;
  "E__") my_echo "Error to create log upload file and command. " ; exit 4 ;;
  "EDR"|"E_R"|"ED_"|"_DR") 
         my_echo "Got illegal status of ${stlog} in dstm-log_make  " ; exit 4 ;;
esac 

is_upload_ready || exit 3

stul=`get_status log_ul`
case "${stul}" in 
  "___") 
    cmd=`cat logupload-cmd.sh`
    my_echo "dstmjob log upload will start, cmd=${cmd}" | tee -a logupload.log
    touch stdir/log_ul.running
    . ${MYPROD_PRODPROD}
    ${cmd} >> logupload.log 2>&1
    retcode=$?
    ( cd stdir && rm -f log_ul.running )
    if [ ${retcode} -ne 0 ] ; then
      my_echo "Log upload failed, return code=${retcode}" 
      touch stdir/log_ul.error
      exit 2
    fi
    stword=`grep "NOTICE: Successfully uploaded file to" logupload.log 2>/dev/null`
    if [ "x${stword}" == "x" ] ; then
      my_echo "Log upload failed. No successful word in logfile, logupload.log" 
      touch stdir/log_ul.error
      exit 2
    fi
    my_echo "Log upload successfully completed." 
    recprod=`/bin/ls recprod_*.json 2>/dev/null | head -1`
    has_elog ${recprod} && { 
      elogid=`jsonread ${recprod} conf __ELOG_ID`
      putelog.py -c ${recprod} dstm_loguploaded | tee -a ${stfile}
    }
    touch stdir/log_ul.done
    ;;
    "__R") my_echo "Detect that log upload is running still. Wait running one to complete." ; exit 3 ;;
    "E__") my_echo "Error to create log upload file and command. " ; exit 3 ;;
    "EDR"|"E_R"|"ED_"|"_DR")
         my_echo "Got illegal status of ${st2} in dstm-replicate-submit " ; exit 3 ;;
esac


if [ -e stdir/log_ul.done ] ; then 
  echo "### task_step6.sh ${dstr} ${hostname} : task_step6 ALL TASKS DONE. " | tee -a ${stfile}
  echo "### task_step6.sh ${dstr} ${hostname} : task_step6 ALL TASKS DONE. " > task_step6.done
fi
exit 0




