#!/bin/env python  

import os
import json 
import pprint
import glob
import subprocess
import datetime
import argparse

# ##############################################
if __name__ == "__main__":

   parser = argparse.ArgumentParser(description="Set selected data, 2(rec/dst), 3(dst) to sim data if not set.")
   parser.add_argument("-a",
          help="Disregarding existing value, set SelectedFile to all file based on select_reckeep_byPID.json", 
          dest="allset", action="store_true", default=False)
   parser.add_argument("--selected_file_recdst", help="SelectedFile value for RECDST(keep REC) reconstruction. default=2",
          dest="selected_file_recdst", action="store", default="2")
   parser.add_argument("--selected_file_dstonly", help="SelectedFile value for DSTONLY(rm REC) reconstruction. default=3",
          dest="selected_file_dstonly", action="store", default="3")

   args = parser.parse_args()
   allset = float(args.allset)
   selected_file_recdst  = int(args.selected_file_recdst)
   selected_file_dstonly = int(args.selected_file_dstonly)

   # #####################################################################
   # Save store-rec or not in a simple format
   selectinfo_file = "../3_gensplit/select_reckeep_byPID.json"
   print("Getting recdst/dstonly definition from "+selectinfo_file)
   if not os.path.exists(selectinfo_file):
     print("### %s is not created yet. " % selectinfo_file )
     exit(8)     

   selectinfo = json.load(open(selectinfo_file))
   saverec = {}
   for k in selectinfo:
      saverec[k] = {}
      for data in selectinfo[k]:
         key=str(data[0]) + "-" + str(data[1])
         saverec[k][key] = data[3]

   # #######################################################
   # Get sim production info
   recjson = glob.glob("recprod_*.json")[0]
   print("Getting production information from "+recjson)
   recprod = json.load(open(recjson))
   recinfo = recprod["rec"]

   simpath = recinfo["matchToInput"]
   simprod = recinfo["meta"]["ProdID"]

   # #########################################################
   # Get list of simfiles registered in catalog
   dodebug = False
   if dodebug and os.path.exists("simfiles.list"):
      with open("simfiles.list", "r") as f:
         simfiles = f.read().splitlines()
   else:
      cmd="dirac-dms-find-lfns Path="+simpath+"  ProdID="+str(simprod) 
      # cmd+= " SelectedFile!=2 SelectedFile!=3 "
      # cmdout = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
      dtstr=datetime.datetime.now().isoformat().replace("T"," ").split(".")[0]
      print("At "+dtstr+", executing " + cmd)
      cmdout0 = subprocess.call(cmd + " > cmdout0.log 2>&1 ", shell=True, stderr=subprocess.STDOUT)
      dtstr=datetime.datetime.now().isoformat().replace("T"," ").split(".")[0]
      print("At "+dtstr+", executing " + cmd + " SelectedFile=%d" % selected_file_recdst)
      cmdout2 = subprocess.call(cmd+" SelectedFile=%d > cmdout23.log 2>&1 " % selected_file_recdst, 
                shell=True, stderr=subprocess.STDOUT)
      dtstr=datetime.datetime.now().isoformat().replace("T"," ").split(".")[0]
      print("At "+dtstr+", executing " + cmd + " SelectedFile=%d" % selected_file_dstonly)
      cmdout3 = subprocess.call(cmd+" SelectedFile=%d >> cmdout23.log 2>&1" % selected_file_dstonly, 
                shell=True, stderr=subprocess.STDOUT)
      dtstr=datetime.datetime.now().isoformat().replace("T"," ").split(".")[0]
      print("At "+dtstr+", completed all dirac-dms-find-lfns command.")
      simfiles=[]
      
      selfile23=[]
      if not allset:
         for line in open("cmdout23.log"):
           if "slcio" in line:
             selfile23.append(line.replace("\n",""))
      for line0 in open("cmdout0.log"):
         if "slcio" in line0:
           line = line0.replace("\n","")
           if line not in selfile23:
             simfiles.append(line)
       
      print("set_sim_selected.py found "+str(len(simfiles))+" files with SelectedFile neither 2 nor 3.") 
      with open("simfiles.list","w") as f:
         f.write("\n".join(simfiles))
      print("Found files are written in simfiles.list")


   # #########################################################
   # pprint.pprint(simfiles)
   # Create dirac catalog command to set meta SelectedFile
   outcmd = []
   for sfile in simfiles:
       fn = sfile.split("/")[-1]
       fnkeys = fn.split(".")
       genser = ""
       ikeys= ""
       for k in fnkeys:
          if k[0:1] == "n":
            (genser, subser) = k[1:].split("_")
            key_saverec = str(int(genser))+"-"+str(int(subser))
          if k[0:1] == "I":
            ikey = k[1:]

       selected_file_value = 0      
       if "I"+ikey in saverec and key_saverec in saverec["I"+ikey]:
         selected_file = saverec["I"+ikey][key_saverec]
         # print(saverec["I"+ikey][key_saverec] + "  "+fn)
         if selected_file == "dstonly":
           selected_file_value = selected_file_dstonly
         elif selected_file == "recdst":
           selected_file_value = selected_file_recdst
      

       if selected_file_value != 0:
         outcmd.append("meta set "+sfile+" SelectedFile "+str(selected_file_value))

   lencmd = len(outcmd)

   with open("setmeta_seleced_file.cmd","w") as f:
       f.write("\n".join(outcmd))
   print("### set_sim_selected.py created a file, setmeta_selected_file.cmd") 
   print("### Completed set_sim_selected.py. "+str(lencmd)+" files to set meta")

   exit(0)

