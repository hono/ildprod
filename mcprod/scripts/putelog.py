#!/bin/env python 
#
# Register elog info
#

import elog
import os
import argparse
import json
import datetime
import pprint
import glob

_logbook = None
_elog_url = "https://ild.ngt.ndu.ac.jp/elog"
_elog_subdir = "dbd-prod" if "ELOG_SUBDIR" not in os.environ else os.environ["ELOG_SUBDIR"]

_do_test = False

from ElogTools import *

# ==========================================================
def subprod_newrec(params):
    ''' Reconstruction of existing simulation.  
        Following key must be present in params
        sim_elogid : ELOG ID of existing sim ELOG 
        opt_ovl : ovl or nobg option of reconstruction
        opt_rec : reconstruction option. o2, o3, ...
    '''

    now = datetime.datetime.now()
    dstr = "%4.4d-%2.2d-%2.2d" % (now.year, now.month, now.day )
    tstr = "%2.2d:%2.2d:%2.2d" % (now.hour, now.minute, now.second)



# ==========================================================

if __name__ == "__main__":

    ret = putelog_main()

    

    exit(ret)


