#!/bin/bash 

activedir="active-prod-dir.list"

oneprod()
{
  # activedir="prod-4f_semileptonic/4f_ZZ_sermileptonic.bWW"
  activedir=$1
  cd ${activedir}
  if [ ! -e echeck ] ; then 
    mkdir -vp echeck
  fi
  cd echeck

  ( cd ../4_subprod && /bin/ls simprod_*.json recprod_*.json ) > prodjson.filename
  cat prodjson.filename | while read f ; do 
    (
      prodid=`echo $f | cut -d"_" -f2`
      prodtype=`echo $f | cut -d"_" -f1`
      if [ "${prodtype}" == "simprod" ] ; then 
        mkdir -vp er${prodid}
        cd er${prodid}
        get_error_joblogs.sh ${prodid} 2>&1 | tee ../errsum-${prodid}.log
        scan_sim_stdout_error.py 2>&1 | tee -a ../errsum-${prodid}.log
        # if [ -e exit134_jobid.list ] ; then 
        #  nexit134=`cat exit134_jobid.list | wc -l`
        #  njobs=`cat jobstatus-${prodid}.log | wc -l`
        #  nmaxerr=`echo "print \"${njobs}.0*0.10\" " | python - | cut -d"." -f1 `
        #  echo "nexit134=${next134} njobs=${njobs} nmaxerr=${nmaxerr}"
        #  if [ ${njobs} -lt 500 ] ; then 
        #     if [ ${nexit134} -gt 50 ] ; then 
        #       echo "stop ${prodid} " | dirac-transformation-cli 2>&1 | tee -a ../errsum-${prodid}.log
        #       echo "Production ${prodid} was stopped. Nexit134 >= 50" | tee -a ../errsum-${prodid}.log
        #     fi
        #  elif [ ${nexit134} -gt ${nmaxerr} ] ; then
        #     echo "stop ${prodid} " | dirac-transformation-cli 2>&1 | tee -a ../errsum-${prodid}.log
        #     echo "Production ${prodid} was stopped. Nexit134/Njob > 0.1 " | tee -a ../errsum-${prodid}.log
        #  fi
        # fi
        echo " " | tee -a ../errsum-${prodid}.log
      elif [ "${prodtype}" == "recprod" ] ; then 
        mkdir -vp em${prodid}
        cd em${prodid}
        get_error_joblogs.sh ${prodid} 2>&1 | tee ../errmar-${prodid}.log
        scan_rec_stdout_error.py 2>&1 | tee -a ../errmar-${prodid}.log
        echo " " | tee -a ../errmar-${prodid}.log
      fi
    )
  done
#
}



hostname=`hostname`
dstr=`date +%Y%m%d-%H%M%S`

errlog="errsum-all.log"
echo "### FILE_to_DOWNLOAD is `pwd`/${errorlog}" 

if [ -e error_chk.running ] ; then 
  echo "ERROR Previous error check job still running, this script is terminated."
  exit 19
fi

echo "error_chk.sh running." > error_chk.running  
echo "host : ${hostname}" >> error_chk.running  
echo "datetime : ${dstr}" >> error_chk.running  

echo "### Error status check job has started at ${hostname} on ${dstr}" | tee ${errlog}
cat ${activedir} | while read f ; do 
  if [ -e "${f}" ] ; then 
    dstr=`date +%Y%m%d-%H%M%S`
    echo "### Creating error summary of ${f} at ${hostname} on ${dstr}" | tee -a ${errlog}
    oneprod $f 2>&1 | tee -a ${errlog}
    echo "  " | tee -a ${errlog}
  else
    echo "Skipping $f" | tee -a ${errlog}
  fi
done 
rm -fv error_chk.running
echo "### All dirs in ${activedir} has completed." | tee -a ${errlog}
