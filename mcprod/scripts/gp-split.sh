#!/bin/bash 

. /cvmfs/ilc.desy.de/sw/x86_64_gcc49_sl6/v02-00-02/init_ilcsoft.sh

run()
{
    fser=`printf %4.4d $1`
    outdir=$2
    infile="eepairs-250-A/E250-SetA.PBeamstr-pairs.GGuineaPig-v1-4-4.I270000.${fser}.pairs"
    for ftype in "ge2MeV" "lt2MeV"; do
    (
       if [ "${ftype}" == "ge2MeV" ] ; then
          kmin=0.002
          kmax=10000000.0
       else
          kmin=0.0
          kmax=0.002
       fi

       python Guineapig2LcioSplit.py ${infile} --outdir ${outdir} \
      --nb_events_per_file 100000 \
      --minimum_kin_energy_in_GeV ${kmin} --maximum_kin_energy_in_GeV ${kmax} \
      --file_name_format "E%s.P%s_${ftype}.G%s.I%s.n%s.slcio"
    )
    done
    
}

outdir="out-32-100"
mkdir -p ${outdir}
# for i in `seq 20 30` ; do 
# for i in `seq 1 19 ` ; do 
# for i in `seq 32 100 ` ; do 
for i in 39 55  ; do 
  run $i ${outdir}
done


