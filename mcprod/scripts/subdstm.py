#!/bin/env python

import glob
import sys

if __name__ == "__main__":

  # dstm-I500012-n121-j318

  prodid=15275
  processID=500012
  fileno=121
  jobno=318
  detector="ILD_l5_o1_v02"

  sysarg = sys.argv
  print(sysarg)
  if len(sysarg) != 5:
     print("Usage   : subdstm.py <prodid> <processID> <fileno> <jobno> ")
     print("Function: Submit one DST merge job.")
     print(" ")
     print("<prodid> : Production ID")
     print("<processID> : Process ID")
     print("<fileno> : file number in string (incl. 0)")
     print("<jobno>  : job number")
     exit(-1)

  prodid=sys.argv[1]
  processID=sys.argv[2]
  fileno=sys.argv[3]
  jobno=sys.argv[4]

  #print("prodid="+str(prodid)+" processID="+str(processID)+" fileno="+str(fileno)+" jobno="+str(jobno))
  #exit(-1)

  # jobtop = "dstmjobs-%d-%s" % (prodid, detector)

  jobtops = glob.glob("dstmjobs-%s-ILD_*" % prodid )
  if len(jobtops) > 1 :
    print("More than one dstmjobs directory matched.  Not able to select by this script")
    print(jobtops)
    exit(-1)
  jobtop = jobtops[0]
  detector = jobtop.split("-")[2]
  dstmmaker_json = "dstm-jobmaker-%s-%s.json" % (prodid, detector[4:])

  from ILD_DSTM_Submit import *

  dirselect = jobtop + "/dstm.I%s.n%s.j%s" % ( processID, fileno, jobno )
  jobmeta = set_params(jobtop, dstmmaker_json)
  do_job_submit(jobmeta, dirselect)

