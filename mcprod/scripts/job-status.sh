#!/bin/bash 

for d in mILD* ; do 
  ( 
  cd $d
  jobid=`jsonread ddsimjob.json SubmitReturn Value`
  dirac-wms-job-status ${jobid} 2>&1 | tee job-status.log
  )
done

