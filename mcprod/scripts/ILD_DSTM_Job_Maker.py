#
#  /usr/bin/env python 

from __future__ import print_function
import os
import pprint
import json
import copy
import math
import sys

from DIRAC import gLogger, S_OK, S_ERROR
from DIRAC.Core.Base.Script import Script
# Script.parseCommandLine()
from ILCDIRAC.Core.Utilities.FilenameEncoder import decodeFilename, makeFilename

if "MCPROD_CONFIG_FILE" not in os.environ:
    print( "ERROR FATAL : MCPROD_CONFIG_FILE environmnet parameter is not defined." )
    exit(-1)

_configfile = os.environ["MCPROD_CONFIG_FILE"]
if not os.path.exists(_configfile):
    print( "ERROR FATAL : configfile " + _configfile + " not exist" )
    exit(-1)
if sys.version_info.major == 2:
    execfile(_configfile)
else:
    exec(open(_configfile).read())

_DSTM_SIZE_IN_MB = 1200.0 # Roughly maximum size of DST-Merged files.
_DSTM_NUMDST_MAX = 98  # Max. number of DST files in DST-Merged files. Limit imposed by DIRAC

class ILDDSTMaker(object):
  
  def __init__(self, debug_filename=False):

    self.debug_filename = debug_filename
    self.clip = self._Params()
    self.clip.registerSwitches()
    Script.parseCommandLine()

    self.clip.print_parameters()
    self.dstm_size_in_MB = self.clip.dstm_size_in_MB
    self.ignore_scan_error = self.clip.ignore_scan_error

    if self.clip.doScan:
      from DIRAC.Resources.Catalog.FileCatalog import FileCatalog
      from DIRAC.DataManagementSystem.Client.MetaQuery import MetaQuery, FILE_STANDARD_METAKEYS
      
      self.fc = FileCatalog()
      result = self.fc.getMetadataFields()
      if not result['OK']:
        print( '### Can not get Metadata fileds from file catalog, '+result['Message'] )
        exit(23)
  
      self.typeDict = result['Value']['FileMetaFields']
      self.typeDict.update(result['Value']['DirectoryMetaFields'])
      self.typeDict.update( FILE_STANDARD_METAKEYS )
      self.mq = MetaQuery( typeDict = self.typeDict )
  
  # ###################################
  class _Params():
    def __init__(self):
      self.doScan = True
      self.isTest = True
      self.recprod_json = ''
      self.dstmscan_json = ''
      self.debug = False
      self.dstm_size_in_MB = _DSTM_SIZE_IN_MB
      self.ignore_scan_error = False
      self.max_file_in_subdir = 1000
      self.no_dstm_subdir = False

    def set_doScan(self, opt):
      self.doScan = True
      return S_OK()

    def unset_isTest(self, opt):
      self.isTest = False
      return S_OK()

    def set_recprod_json(self, opt):
      self.recprod_json = opt
      return S_OK()
    
    def set_dstmscan_json(self, opt):
      self.dstmscan_json = opt
      self.doScan = False
      return S_OK()
    
    def set_debug(self, opt):
      self.debug = True
      self.doScan = False
      self.recprod_json = "recprod_8585_l4_v02.json"
      self.dstmscan_json = "dstm_maker_temp.json"
      return S_OK() 

    def set_dstm_size_in_MB(self, opt):
      self.dstm_size_in_MB = float(opt)
      print( "The size of DST-merged file will be "+str(self.dstm_size_in_MB) + " MB" )
      return S_OK()

    def set_ignore_scan_error(self, opt):
      self.ignore_scan_error = True
      return S_OK()

    def set_max_file(self, opt):
      self.max_file_in_subdir = int(opt)
      return S_OK()

    def set_no_dstm_subdir(self, opt):
      self.no_dstm_subdir = True
      return S_OK()

    def print_parameters(self):
      print( 'Parameters of %s ' % Script.scriptName )
      print( '  recprod_json = %s ' % self.recprod_json )
      print( '  dstmscan_json = %s ' % self.dstmscan_json )
      print( '  isTest = ' + str(self.isTest) )
      print( '  doScan = ' + str(self.doScan) )
      print( '  debug = ' + str(self.debug) )
      print( '  max_nb_files_in_subdir = ' + str(self.max_file_in_subdir) )
      print( '  no_dstm_subdir = ' + str(self.no_dstm_subdir) )

    def registerSwitches(self):
      Script.registerSwitch('','recjson=','A json file of reconstruction production (recprod_[prodid]_[detector].json)', self.set_recprod_json)
      Script.registerSwitch('','scanjson=', 'A json file containing results of DST and GEN file scan (dstm-scan-*-*.json)', self.set_dstmscan_json)
      Script.registerSwitch('','noTest', 'If specified, DST-merged files are written in an official directory', self.unset_isTest)
      Script.registerSwitch('','debugrun', 'If specified, run in debug mode ( data are taken from given files. )', self.set_debug)
      Script.registerSwitch('','dstm_size=', 'Size of DST-merged file in MB (%f MB default)' % self.dstm_size_in_MB, self.set_dstm_size_in_MB)
      Script.registerSwitch('f','ignore_scan_error', 'Proceed to job script make, even if production is not finish', self.set_ignore_scan_error)
      Script.registerSwitch('','max_file=', 'Max dst-merged files in output sub directory. Default=%d' % self.max_file_in_subdir, self.set_max_file)
      Script.registerSwitch('','no_dstm_subdir', 'If specified, not create sub d-rectory structure in DSTM directory', self.set_no_dstm_subdir)

      msg = '%s [options]\n' % Script.scriptName
      msg += 'Function: \nUsing a json file of reconstruction production, scan produced DST and GENSplit files\n'
      msg += '  If number of DST and GenSplit files are same, create job scripts for DST-merging by DIRAC user jobs.\n'
      msg += 'Usage: \n'
      msg += '  python %s --noTest --recjson [recprod-json-file] \n' % Script.scriptName
      msg += '  python %s --noTest --scanjson [previously-scanned-json-file] \n' % Script.scriptName
      msg += '  python %s --debugrun \n' % Script.scriptName

      Script.setUsageMessage(msg)
      

  # ###################################
  def meta_to_query(self, meta):
    query = []
    for key, value in meta.items():
      query += [ "%s=%s"%(key,value) ] 
    return query  

  # ###################################
  def _searchFileCatalog(self, meta, path):
    from DIRAC.Resources.Catalog.FileCatalog import FileCatalog
    from DIRAC.DataManagementSystem.Client.MetaQuery import MetaQuery, FILE_STANDARD_METAKEYS

    metaquery = self.meta_to_query(meta)
    result = self.mq.setMetaQuery( metaquery )
    if not result['OK']:
      print( '### Error: Illegal metaQuery:'+result['Message'] )
      exit(21)    
    metaDict = result['Value']
    spath = metaDict.pop('Path', path)
    # print 'spath='+spath
    result = self.fc.findFilesByMetadata(meta, spath)
    if not result['OK']:
      print( '### Error to find '+path+' using meta, ' )
      pprint.pprint(meta)
      exit(22)

    ret = {}
    print(" Got lfnList ... searching catalog")
    lfnList = sorted( result['Value'] )
    if len(lfnList) == 0:
       print("### Warning : No file found by searchFileCatalog")
       return {"No_File_Found":True}

    res = self.fc.getFileMetadata(lfnList)
    if not res['OK']:
       print("### Failed to get catalog information.  Return message was")
       print(res["Value"]["Failed"])
       exit(22)
    
    catinfo = res["Value"]["Successful"]
    print(" Got catalog information ")
    # pprint.pprint(catinfo)

    for lfn in lfnList:
#      res = self.fc.getFileMetadata(lfn)
      size = catinfo[lfn]['Size']
      
      filename = os.path.basename(lfn)
      dirname  = os.path.dirname(lfn)
      if self.debug_filename:
        print( str(size)+"  " +filename )
      token = decodeFilename(filename)

      key = 'I%s.n%s' % ( token['I'], token['n'] )      
      if key in ret:
         print( "### Warning: " + key + " exist already. file " + ret[key]["filename"] + " is not used." )

      ret[key] = { 'dirname':dirname, 'filename':filename, 'size':size, 'key':key}

    return ret 

  # ############################################
  def findFiles(self, jsfile):

    param = json.load(open(jsfile))
    prodid = param['dstm']['meta']['ProdID']
    print( '### Searching genfiles for Marlin Production ProdID '+str(prodid) )
    genmeta = param["sim"]["meta"]
    genpath = param["sim"]["matchToInput"]
    print( genmeta )
    print( genpath )
    print( param["rec"]["meta"] )
    genfiles = self._searchFileCatalog(genmeta, genpath)
    print( '    found '+str(len(genfiles))+' gen files' )

    print( '### Searching dstfiles for Marlin Production ProdID '+str(prodid) )
    dstmeta = param["dstm"]["meta"]
    dstpath = param["rec"]["dstPath"]
    dstfiles = self._searchFileCatalog(dstmeta, dstpath) 
    if "No_File_Found" in dstfiles:
       print("### No matching DST files found. DST-Merged files are not created.")
       dstfiles = []

    print( '    found '+str(len(dstfiles))+' dst files' )

    return {'genfiles':genfiles, 'dstfiles':dstfiles, 'recparam':param}

  ############################################################
  def decide_dst_in_dstm(self, mdata):
    dstm_size = float(self.dstm_size_in_MB)*1.0E6
    sdata={}

    print( "There are %d dstfiles in total" % len(mdata["dstfiles"]) )

    for key, value in sorted(mdata["dstfiles"].items()):
      (gid, nser) = key.split('.')
      if gid not in sdata:
        sdata[gid]={'size':0, 'nfile':0, 'filekey':[]}
      sdata[gid]['size'] += int(value['size'])
      sdata[gid]['nfile'] += 1
      sdata[gid]['filekey'] += [key]
  
    divinfo={}
    for gid, value in sorted(sdata.items()):
      divinfo[gid]={}
      numdsts = len(sdata[gid]['filekey'])
      xdiv0 = float(numdsts)/float(_DSTM_NUMDST_MAX)
      xdiv = float(value['size'])/dstm_size
      # xdiv = number of dst-merged files to be produced. 
      #_DSTM_NUMDST_MAX = 99  # Max. number of DST files in DST-Merged files. Limit imposed by DIRAC
      ndiv = int(xdiv + 1.0)
      nfile = int(float(numdsts)/float(ndiv)+1.0)
      if nfile > _DSTM_NUMDST_MAX:
        print( "Number of DST files to merge is limitted to " + str(_DSTM_NUMDST_MAX) )
        while nfile > _DSTM_NUMDST_MAX:
           ndiv += 1
           nfile = int(float(numdsts)/float(ndiv)+1.0)

      # Depending of # of DST merged will be, for an example, avoid to create too small dst-merged file 
      # (xdiv, ndiv) = ( 0.2, 1), (0.5, 1), (1.2, 1), (1.4, 2), (2.1, 2), (5.4, 6), ...
      # ndiv = int(xdiv+0.7) if xdiv > 0.31 else 1
      # nfile = int(float(len(sdata[gid]['filekey']))/float(ndiv))
      # if nfile > _DSTM_NUMDST_MAX:
      #   ndiv += 1
      #   nfile = int(float(len(sdata[gid]['filekey']))/float(ndiv)) 
      # nfile is the number of DST files in each dst-merged file. Should not exceed _DSTM_NUMDST_MAX
      keydata = sorted(value['filekey'])
      # print( str(gid)+" "+str(ndiv)+" "+str(xdiv)+" "+str(nfile)+" len(keydata)="+str(len(keydata)) )
      print( str(gid)+" "+str(xdiv)+" "+str(nfile)+" len(keydata)="+str(len(keydata)) )

      i = 0
      donext = True
      while donext:
        ibgn=int(i)*nfile
        ilast=int(i+1)*nfile 
        if ilast < len(keydata):
            divinfo[gid][i]=keydata[ibgn:ilast]
        else:
            donext = False 
            divinfo[gid][i]=keydata[ibgn:]
        i += 1
        if int(i)*nfile >= len(keydata):
           donext = False

    # pprint.pprint(divinfo)
    for gid, value in sorted(divinfo.items()):
       print( "%d dst-merged files for ProcessID %s will be created." % ( len(value), str(gid) ) )
       for fseq, alist in sorted(value.items()):
           if len(alist) > _DSTM_NUMDST_MAX:
              print( "ERROR : number of DSTs in for ProcessID=" + str(gid) +
                      str(fseq) + "-th DSTM files ("+str(len(alist)) + ") exceeds max of " + str(_DSTM_NUMDST_MAX) )
              print( "Reconsider to adjust parameters. Something is wrong." )
              return {"Failed":True}
              
    #       pprint.pprint(value)
    return divinfo

# ########################################################## 
  def find_file(self, path, filename):
    mypath=os.getenv(path)
    for d in mypath.split(':'):
      fna = glob.glob(d+'/'+filename)
      if len(fna) > 0:
        return fna[0]
    return ''

# #########################################################
  def setParameters(self, param):
    ''' Set parameters for DST-Merged jobs '''
    dstm_meta = param['dstm']['meta']
    detector = dstm_meta["DetectorModel"]
    param['dstm']['dstmPath']=param["conf"]["__DIRECTORY_DSTMERGED"]
    param['dstm']['SE']=param["conf"]["__DSTMERGED_SE"] if "__DSTMERGED_SE" in param["conf"] else "DESY-SRM"
    param['dstm']['replicates']=param["conf"]["__DSTM_REPLICATES"] if "__DSTM_REPLICATES" in param["conf"] else "KEK-DISK"  # replicate DSTM-merged files in after DST-merge jobs.
    param['dstm']['jobtop'] = 'dstmjobs-%s-%s' % ( str(dstm_meta['ProdID']), detector )
    

    if self.clip.isTest == True:
      param['dstm']['dstmPath']='/ilc/prod/ilc/ild/test/mc-opt/ild/'
      param['dstm']['SE'] = 'DESY-SRM'
      param['dstm']['jobtop'] = 'debug-%s-%s' % ( str(dstm_meta['ProdID']), detector )

    param['dstm']['dstmDirectory'] = param['dstm']['dstmPath']+'dst-merged/%s-%s/%s/%s/%s' % \
        ( dstm_meta['Energy'], dstm_meta['MachineParams'], dstm_meta['EvtType'], \
          dstm_meta['DetectorModel'], dstm_meta['ILDConfig'] )
    metadata=[]
    tpath = param['dstm']['dstmPath']+"dst-merged"
#        [tpath, "Datatype DST-Merged"], 
#        [tpath+"/%s-%s" % (dstm_meta['Energy'], dstm_meta['MachineParams']), 
#         "Energy %s MachineParams %s" % (dstm_meta['Energy'], dstm_meta['MachineParams'])],
    metadata=[ 
        [tpath + "/%s-%s/%s" % (dstm_meta['Energy'], dstm_meta['MachineParams'], 
          dstm_meta['EvtType']), "EvtType %s" % dstm_meta['EvtType'] ],
        [tpath + "/%s-%s/%s/%s" % (dstm_meta['Energy'], dstm_meta['MachineParams'], 
          dstm_meta['EvtType'], dstm_meta['DetectorModel']), "DetectorModel %s" % dstm_meta['DetectorModel'] ], 
        [tpath + "/%s-%s/%s/%s/%s" % (dstm_meta['Energy'], dstm_meta['MachineParams'], 
          dstm_meta['EvtType'], dstm_meta['DetectorModel'], dstm_meta['ILDConfig']), 
          "ILDConfig %s" % dstm_meta['ILDConfig'] ] ] 
  
    param['dstm']['set_dir_meta'] = metadata
    param['dstm']['symlink_files'] = ["dirac-ildapp-dstmerge.sh", "marlin_merge.xml.in"]
    jobsetup={}
    jobsetup["_output_dstm_se"] = param['dstm']['SE']
    jobsetup["_output_basedir"] = param['dstm']['dstmPath']
    jobsetup["_init_ilcsoft"]=MCProd_init_ilcsoft
    jobsetup["_marlin_template"]="marlin_merge.xml.in"
    jobsetup['_dstlist'] = 'dstlist.txt'
    jobsetup['_dstm_dirname'] = param['dstm']['dstmDirectory']
    
    
    param['dstm']['jobsetup'] = jobsetup

    return param


############################################################
  def writeConfig(self, dict_file, fileout):
    fout = open(fileout,'w')
    for key, val in dict_file.items():
      fout.write(key+'='+val+'\n')
    fout.close()

# ##########################################################
  def do_DSTScan(self):
    ''' Scan DST files and get information '''
    if self.clip.doScan:
      print( '### Searching DST files for production defined in %s ' % self.clip.recprod_json )
      mdata = maker.findFiles(self.clip.recprod_json)
      detector = mdata['recparam']['dstm']['meta']['DetectorModel']
      prodid = mdata['recparam']['dstm']['meta']['ProdID']
      dstmscan_json = 'dstm-scan-%s-%s.json' % ( str(prodid), detector.replace('ILD_','') ) 
      json.dump(mdata, open(dstmscan_json,'w'))
      print( '### DST scan results are written in '+dstmscan_json )
    else:
      print( '### loading DST scan results from %s ' % self.clip.dstmscan_json )
      if not os.path.exists(self.clip.dstmscan_json):
        print( '### DST scan results file, %s, does not exist' % self.clip.dstmscan_json )
        return { 'OK':False, 'ExitCode':10}
      mdata = json.load(open(self.clip.dstmscan_json))
      print( '### DST scan result was obtained from %s ' % self.clip.dstmscan_json )
  
    lengen = len(mdata["genfiles"])
    lendst = len(mdata["dstfiles"])

    recdata = json.load(open(self.clip.recprod_json))
    recrate = -1
    if recdata["conf"]["__RECRATE"] in recdata["conf"].values():
      recrate = float(recdata["conf"]["__RECRATE"])

    recdst = False
    if recrate > 0 and recdata["rec"]["meta"]["SelectedFile"] == "2":
      recdst = True
      print("RECDST production : __RECRATE = " + str(recrate))
      print("Rescale # of genfiles with RECRATE.")
      lengen = int(lengen*recrate)
    elif recrate > 0 and recdata["rec"]["meta"]["SelectedFile"] == "3":
      recdst = False
      print("DSTONLY production : __RECRATE = " + str(recrate))
      print("Rescale # of genfiles with RECRATE.")
      lengen = lengen-int(lengen*recrate)

    if lendst == 0:
      print("### No DST files was found. DSTM-Merged files are not created for this production.")
    elif lengen != lendst:
      print( '### # of genfiles(%d) and # of dst files(%d) is not equal yet' % ( lengen, lendst ) )
      return { 'OK':False, 'ExitCode':4, "lengen":lengen, "lendst":lendst, "mdata":mdata}
    
    return { 'OK':True, 'lengen':lengen, 'lendst':lendst, 'mdata':mdata}

  # ################################################################################
  def create_Scripts(self, divinfo, param, dstfiles):
    ''' Create job scripts '''
    dstm_meta = param['dstm']['meta']
    detector = param['dstm']['meta']["DetectorModel"]
    prodid = param['dstm']['meta']['ProdID']
    jobtop = param['dstm']['jobtop']

    if os.path.exists(jobtop):
      print( '### Error : '+jobtop+' exists. Remove it to proceed.' )
      return { 'OK':False, 'ExitCode':10}

    os.mkdir(jobtop)

    jobmaker_json = "dstm-jobmaker-%s-%s.json" % (str(prodid), detector.replace('ILD_', ''))
    json.dump(param, open('/'.join([jobtop, jobmaker_json]),'w'))
    print( '### DSTM job parameters are written in '+'/'.join([jobtop, jobmaker_json]) )
    subdir=0
    subdir_cnt=0
    subdir_maxfile = self.clip.max_file_in_subdir 

    setdirmeta = param['dstm']['set_dir_meta']
    if not self.clip.no_dstm_subdir:
       setdirmeta.append([param['dstm']['jobsetup']["_dstm_dirname"]+"/%8.8d" % int(prodid), "ProdID %d" % prodid])

    for gid, value in divinfo.items():
      for fileseq, alist in value.items():
        dstmeta = decodeFilename( dstfiles[alist[0]]['filename'] )
        genseq = dstmeta['n'].split('_')
        jobdir = jobtop+'/dstm.'+str(gid)+'.n%s.j%s' % (genseq[0], str(fileseq))
        print( "####  Creating job scripts in jobdir "+jobdir )
        os.mkdir(jobdir)

        dstlist_txt = []
        for key in alist:
          dstinfo=mdata['dstfiles'][key]
          dstlist_txt.append('/'.join([dstinfo['dirname'], dstinfo['filename']]))
        fout = open(jobdir + '/'+param['dstm']['jobsetup']['_dstlist'],'w')
        fout.write('\n'.join(dstlist_txt))
        fout.write('\n')
        fout.close()

        jobinfo = copy.deepcopy(param['dstm']['jobsetup'])
        jobinfo['_dstm_file_name'] = "r%s.s%s.m%s.E%s.I%s.P%s.e%s.p%s.n%s.d_dstm_%d_%d.slcio" \
          % (dstmeta["r"], dstmeta["s"], dstmeta["m"], dstmeta["E"], dstmeta["I"], \
             dstmeta["P"], dstmeta["e"], dstmeta["p"], genseq[0], int(prodid), int(fileseq))
        if not self.clip.no_dstm_subdir:
           subdir_cnt +=1
           if subdir_cnt > subdir_maxfile:
              subdir += 1
              subdir_cnt = 1
           jobinfo["_dstm_dirname"] += "/%8.8d/%3.3d" % ( int(prodid), int(subdir) )           
           print("      Out dir. : "+jobinfo["_dstm_dirname"])

        # print(" dstm_dirname="+jobinfo["_dstm_dirname"])
        # print(" dstm_file_name"+jobinfo['_dstm_file_name'])
        self.writeConfig(jobinfo, jobdir+'/jobinfo.txt')

        curdir = os.getcwd()
        os.chdir(jobdir)
        scriptsdir = os.environ["SCRIPTS_DIR"]
        for sl in param['dstm']['symlink_files']:
           os.symlink('%s/' % scriptsdir + sl, './'+sl)
        os.chdir(curdir)


    fout = open(jobtop + '/setdirmeta.cli', 'w')
    for line in param['dstm']['set_dir_meta']:
       fout.write("meta set " + ' '.join(line)+'\n')
    fout.close()
    print(" set meta CLI was written in "+jobtop + "/setdirmeta.cli")

    shellcmd = ["#!/bin/bash"]
    tempfile=param['dstm']['jobsetup']["_dstm_dirname"]+"/%s" % ("setdirmeta.cli") if self.clip.no_dstm_subdir else \
         param['dstm']['jobsetup']["_dstm_dirname"]+"/%8.8d/%s" % ( int(prodid), "setdirmeta.cli" )
    shellcmd += ["dirac-dms-add-file  %s %s %s" % ( tempfile, "setdirmeta.cli", param["dstm"]["SE"] ),
                 "cat setdirmeta.cli | dirac-dms-filecatalog-cli",
                 "dirac-dms-remove-files %s" % tempfile ]
    fout = open(jobtop + '/setdirmeta.sh', 'w')
    fout.write("\n".join(shellcmd))
    fout.close()
    os.chmod(jobtop + '/setdirmeta.sh', 0o755)

    return { 'OK':True, 'jobtop':jobtop, 'jobmaker_json':jobmaker_json}

  # ###############################################################################
  def create_submit_script(self, jobtop, jobmaker_json):
     ''' Create a script to submit DIRAC user jobs.'''
     dirselect = "/dstm.*"
     if self.clip.debug:
        dirselect = "/dstm.I108668.*"
     lines = ['from ILD_DSTM_Submit import *', 
             '',
             'if __name__ == \"__main__\":',
             '  jobtop = \"%s\"' % jobtop, 
             '  dstmmaker_json = \"%s\"' % jobmaker_json, 
             '  dirselect = jobtop + \"%s\"' % dirselect,
             '  jobmeta = set_params(jobtop, dstmmaker_json)',
             '  do_job_submit(jobmeta, dirselect)' ]

     fout=open(jobtop+'/dstm_sub.py','w')
     fout.write('\n'.join(lines))
     fout.close()
     print( '### Script to submit DSTMerge jobs is written in '+jobtop+'/dstm_sub.py' )
     print( '    Confirm dirselect in dstm_sub.py, set meta of output directory, then submit jobs by ' )
     print( '       ( cd ' + jobtop + ' && ./setdirmeta.sh ) ' )
     print( '        python ' + jobtop + '/dstm_sub.py' )

# #########################################################  
if __name__ == '__main__':
  
  maker = ILDDSTMaker(debug_filename=True)

  # Scan DST files and got information
  ret = maker.do_DSTScan()
  if not ret['OK'] and not maker.ignore_scan_error:
    print( '### Exit with code '+str(ret['ExitCode']) +' : DSTScan is not ready to next step ' )
    exit(ret['ExitCode'])

  if ret["lendst"] == 0:
    print("### Job script is not created because no DST files were found")
    print( '### ILD_DSTM_Job_Maker completed with code 2' )
    exit(2)

  mdata = ret['mdata']
  # Decide DST File grouping
  print( '### Deciding DST file grouping for DST-merged files.' )
  divinfo = maker.decide_dst_in_dstm( mdata )

  if "Failed" in divinfo:
     print( "### exited due to error in decide_dst_in_dstm" )
     exit(1)

  print( '### Creating job scripts to run dst-merge job by user proxy' )
  param = maker.setParameters(mdata['recparam'])  # Get default parameters for DSTM jobs.

  # Create job scripts for DST-merged jobs.
  ret = maker.create_Scripts(divinfo, param, mdata['dstfiles'] )
  if not ret['OK']:
    exit(ret['ExitCode'])

  jobtop = ret['jobtop']
  jobmaker_json = ret['jobmaker_json']
  maker.create_submit_script(jobtop, jobmaker_json)

#  print( '### All job scripts are created in %s directory ' % jobtop )
#  print( 'User jobs to create and register dst-merged jobs can be submitted by ' )
#  print( '   python '+jobtop+'/dstm_sub.py' )
#  print( '  ' )
  print( '### ILD_DSTM_Job_Maker completed with code 0' )
  exit(0)

