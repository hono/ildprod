#!/bin/bash 

dir0=`( cd ../../.. && basename ${PWD} )`
dir1=`( cd ../.. && basename ${PWD} ) `
dir2=`( cd .. && basename ${PWD} ) `

prodidfile=${MCPROD_TOP}/prodids.list
# prodidfile=/dev/null

echo "# ${dir1}/${dir2} " >> ${prodidfile}

# for line in [rs]*prod_*.json ; do 

find . -maxdepth 1 -name "[rs]*prod_*.json" -type f -print | while read line ; do
  # echo $line 
  prodid=`echo ${line} | cut -d"_" -f2`
  jsonf=${line}
  [ "x${prodid}" != "x12345" ] &&  echo "${prodid}:${jsonf}"
done | sort >> ${prodidfile}

proddir="${dir0}/${dir1}/${dir2}"
if [ "x`grep ${proddir} ${MCPROD_TOP}/startprod.done 2>/dev/null `" == "x" ] ; then 
  echo ${proddir} >> ${MCPROD_TOP}/startprod.done
  echo "Added ${proddir} to startprod.done"
fi

echo "${dir0}/${dir1}/${dir2}" >> ${MCPROD_TOP}/active-prod-dir.list
echo "Production information was added to prodids.list and active-prod-dir.list in ${MCPROD_TOP}"


