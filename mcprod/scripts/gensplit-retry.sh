#!/bin/bash 
# 
# Retry upload and setmeta of files failed in setmeta step.
# Should be executed in the directory where setmeta.log exists.
# called by genstrip-retry-all.sh 
#

opt=$1


for i in `seq 1 3` ; do 

  newlog="setmeta.log"
  grep "LFN already exists in the File Catalog" ${newlog} | cut -d"." -f2- | cut -d" " -f2 | sort | uniq > file_exist.list
  grep -i "### ERROR : dirac-dms-add-file"  ${newlog} | cut -d" " -f5 | sort | uniq > addfile_error.list 
  cat addfile_error.list | while read f ; do 
    fchk=`grep ${f} file_exist.list`
    if [ "x${fchk}" == "x" ] ; then 
       echo ${f}
    fi
  done > retry_file.list

  nerror=`cat retry_file.list | wc -l`
  [ ${nerror} -eq 0 ] && break

  echo "gensplit-retry.sh found ${nerror} failed file upload in ${i}th loop. Retrying."
  if [ ${i} -gt 1 ] ; then 
    echo "sleeping 3m before retry."
    sleep 3m
  fi
  dstr=`date +%Y%m%d-%H%M%S`

  oldlog=${newlog/.log/-${dstr}.log}

  mv ${newlog} ${oldlog}
  # grep -i "### ERROR : dirac-dms-add-file" ${oldlog} | cut -d" " -f5 

  # grep "### ERROR : dirac-dms-add-file" ${oldlog} | cut -d" " -f5 | \
  source ${MYPROD_PRODPROD} 2>&1 > proxy-init-${dstr}.log
  cat retry_file.list | \
  while read f ; do
    echo $f
    fname=`basename $f`
    fsers=`getMetaFromFilename ${fname} n` 
    subser=`echo ${fsers} | cut -d"_" -f2`
    subdir=`basename ${PWD}`
    echo "Retry ${fname}, subser=${subser}"
    if [ "x${opt}" == "xnolog" ] ; then
      setMetaGenSplit ${subser} -d ${subdir} >> ${newlog} 2>&1 
    else
      setMetaGenSplit ${subser} -d ${subdir} 2>&1 | tee -a ${newlog}
    fi
  done
  dstr=`date +%Y%m%d-%H%M%S`
  echo "gensplit-retry.sh ${i}-th loop of retry completed: ${dstr}"
done

