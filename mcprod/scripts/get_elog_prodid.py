#!/bin/env python 

import elog
import os
# from urllib2 import urlopen
import pprint
import json
import datetime
import argparse

# ==================================================================
ELOGDBD="https://ild.ngt.ndu.ac.jp/elog/dbd-prod/"

# ==================================================================
if __name__ == "__main__":
  parser = argparse.ArgumentParser(description="Get prodid from ELOG")
  parser.add_argument("elog_id", help="ELOG ID")

  args = parser.parse_args()
  eid = args.elog_id
  outvec=[]
  abook = elog.open(ELOGDBD)
  ( message, attrib, attach ) = abook.read(eid)
  if attrib["SplitID"] != "":
    outvec.append("gensplit=%s" % attrib["SplitID"].strip())
  if attrib["SimID"] != "":
    outvec.append("sim=%s" % attrib["SimID"].strip())
  recids=attrib["RecID"].split(",")
  for ent in recids:
    rid=ent.split("[")[0]
    outvec.append("dst=%s" % rid.strip())
    if "recdst" in ent:
       outvec.append("rec=%s" % rid.strip())

  print(",".join(outvec))  

