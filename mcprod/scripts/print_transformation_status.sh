#!/bin/bash 

sumwait=0
sumrun=0
sumadd=0
simwrun=0
ntrans=0
while read line ; do 
  if [ "x${line:0:1}" != "x#" ] ; then 
    trid=`echo $line | cut -d":" -f1`
    echo summaryTransformations ${trid} | dirac-transformation-cli | \
       sed -e "s/(Cmd) //g" -e "s/  */ /g" > tr-summary.dat
    trdat=( `grep ${trid} tr-summary.dat` )
    echo get ${trid} | sed -e "s/(Cmd)//g" | dirac-transformation-cli | \
       sed -e "s/(Cmd) //g" -e "s/  */ /g" > tr-info.dat
    maxtasks=`grep MaxNumberOfTasks tr-info.dat | sed -e "s/ //g" | cut -d":" -f2`
    
    ntrans=$[${ntrans}+1]
    ninp=${trdat[3]}
    nwait=${trdat[8]}
    nrun=${trdat[9]}
    nadd=$[${ninp}-${maxtasks}]

    if [ ${trdat[1]} == "MCSimulation_ILD" ] ; then 
      simwrun=$[${simwrun}+${nwait}+${nrun}]
    fi

    sumwait=$[${sumwait}+${nwait}]
    sumrun=$[${sumrun}+${nrun}]
    if [ ${nadd} -gt 0 ] ; then 
      sumadd=$[${sumadd}+${nadd}]
    fi

    # echo "${ninp} ${nwait} ${nrun} ${nadd} | ${sumwait} ${sumrun} ${sumadd} | ${simwrun} "
  fi
done < prodids.list

rm -f tr-info.dat tr-summary.dat
sumwr=$[${sumwait}+${sumrun}]
suminsys=$[${simwrun}+${sumadd}]

echo "number_of_transformation=${ntrans}"
echo "total_wait_jobs=${sumwait}"
echo "total_running_jobs=${sumrun}"
echo "wait_running_total=${sumwr}"
echo "wait_running_sim=${simwrun}"
echo "total_jobs_toadd=${sumadd}"
echo "total_jobs_insys=${suminsys}"

