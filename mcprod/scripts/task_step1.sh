#!/bin/bash
#
# A script for step1 task
#
# Create ELOG entry and ILCDirac directories for gensplit.
#

stfile="status.log"
echo "### prepProd started" 2>&1 | tee ${stfile}
LANG=C
( date && hostname ) 2>&1 | tee -a ${stfile}

echo "### putelog and set_dir starts" 2>&1 | tee -a ${stfile}

noelog=`jsonread production.json __ELOG_NOELOG 2>/dev/null`

if [ "x${noelog}" == "xTRUE" ] ; then
  echo "NOELOG specified. ELOG data for prepProd is not created " | tee -a ${stfile}
else
  putelog.py prepProd 2>&1 | tee -a ${stfile} || exit 1  
  msg=`dirname ${PWD} | sed -e "s|${HOME}/||"`
  elogid=`jsonread production.json __ELOG_ID `
  putelog.py -i ${elogid} -m "Working directory is `hostname -s`:${msg}" text 2>&1 | tee -a ${stfile} 
  #### Do not upload because of the error attaching more than 3 files  
  # elogid=`jsonread production.json __ELOG_ID`
  # ( pushd .. && tar zcf initprod.tar.gz * && popd && mv ../initprod.tar.gz . )  2>&1 | tee -a ${stfile} 
  # putelog.py -i ${elogid} -u initprod.tar.gz \
  #     -m "Production work files have been uploaded to initprod.tar.gz" upload 2>&1 | tee -a ${stfile}
fi

has_simprod=`jsonread production.json __DO_STEP productionType | grep sim`
if [ "x${has_simprod}" == "x" ] ; then 
  elogid=`jsonread production.json __ELOG_ID `
  simprodelog=`jsonread production.json __SIMPROD_ELOG`
  simprodid=`jsonread production.json __SIMPROD_ID`
  https="https://ild.ngt.ndu.ac.jp/elog/dbd-prod/${simprodelog}"
  msg="Reconstruction-only production. Input simulation : Transformation ID ${simprodid}, "
  putelog.py -i ${elogid}  -m "${msg} <a href=\"${https}\">ELOG ID ${simprodelog}</a>" text | tee -a ${stfile}
  date 2>&1 | tee -a ${stfile}
  echo "### prepProd completed" 2>&1 | tee -a ${stfile}
  exit
fi

reuse_gensplit=`jsonread production.json __GENSPLIT_REUSE 2>/dev/null`


##################################################
#  Create ILCDirac directories and set meta information.
##################################################
if [ "x${reuse_gensplit}" == "x" ] ; then 
  ./set_dir_and_meta.sh 2>&1 | tee set_dir_and_meta.log
else
  if [ ${reuse_gensplit} -gt 500000 ] ; then 
     echo "REUSE_GENSPLIT was specified. ILCDIRAC directory for gensplit is not initialized."
     echo "do ./set_dir_and_meta.sh by yourself, if necessary"
     if [ "x${noelog}" == "xTRUE" ] ; then 
        echo "NOELOG specified. Production information is not uploaded to elog." | tee -a ${stfile}
        elogidold=0
     else
        elogidold=`jsonread production.json __ELOGID_OLD `
        elogidnew=`jsonread production.json __ELOG_ID `
        [ "x${elogidold}" == "x0" ] && \
            echo "Reusing GENSPLIT and NOELOG is not set. But ELOGID_OLD is not defined in production.json" 2>&1 | tee -a ${stfile}
        echo -n "Reusing GENSPLIT files of previous production of ELOG ID=" > elogmsg.txt
        echo "<a href=\"https://ild.ngt.ndu.ac.jp/elog/dbd-prod/${elogidold}\">${elogidold}</a>" >> elogmsg.txt
        putelog.py -i ${elogidnew} -f elogmsg.txt text 2>&1 | tee -a ${stfile}
     fi
     echo "### set_dir_meta was skipped, because of reusing GenSplit ${reuse_gensplit} of previous production of Elog ID ${elogidold}" 2>&1 | tee -a ${stfile}
  else
     echo "### ERROR : Invalid gensplit ID (${reuse_gensplit}) was specified. " 2>&1 | tee -a ${stfile}
  fi
fi

date 2>&1 | tee -a ${stfile}

echo "### prepProd completed" 2>&1 | tee -a ${stfile}

