#!/bin/bash
#
# A script for step7 (Upload job log file) tasks
#

##################################################################
myecho()
{
  LANG=C
  dstr=`date +%Y%m%d-%H%M%S`
  hostname=`hostname`
  echo "### task_step7.sh ${dstr} ${hostname} : $*"
}

##################################################################
do_elog()
{
  noelog=`jsonread production.json __ELOG_NOELOG 2>/dev/null `
  if [ "x${noelog}" == "xTRUE" ] ; then 
    return 1
  fi
  return 0
}


##################################################################

stfile="status.log"

if [ ! -e ${stfile} ] ; then
  myecho ${stfile} was created. > ${stfile}
fi

if [ -e task_step7.done ] ; then
  myecho Done already. task_step7.done exists. | tee -a ${stfile}
  exit 0
fi

###########################################################################
if [ ! -e prodstatus.log ] ; then
  myecho "Production did not complete. prodstaus.log not exist." | tee -a ${stfile}
  exit 15
fi

stword=`grep "production done" prodstatus.log`
if [ "x${stword}" == "x" ] ; then
  myecho "Production did not completed yet. \"production done\" not in the file prodstatus.log " | tee -a ${stfile}
  exit 15
fi


proddir=`basename ${PWD} | sed -e "s|7_logupload|4_subprod|"`
if [ ! -e production.json ] ; then 
  if [ ! -e ../${proddir}/production.json ] ; then 
    myecho "Error : ../${proddir}/production.json doe not exist, probably entry in cronexe.list is not correct"
    exit 16
  fi
  myecho "Creating symbolic link of production.json"
  ln -sfv ../${proddir}/production.json . | tee -a ${stfile}
fi


myecho "checking system loads "
ret=0
( runtest_by_load.sh ) || ret=1
if [ "x${ret}" != "x0" ] ; then
   myecho "Found too large load on this system. quit further execution"
   exit 20
fi



##################################################################
stword=`grep "prodlog_cleanup.py has started" ${stfile}`
if [ "x${stword}" == "x" ] ; then
  myecho "prodlog_cleanup.py will be started." | tee -a ${stfile}
  echo "### task_step7.sh started" 2>&1 | tee ${stfile}
  ( LANG=C && date && hostname ) 2>&1 | tee -a ${stfile}

  do_elog &&  putelog.py logging 2>&1 | tee -a ${stfile}
  rm -f prodjson.list
  for f in ../${proddir}/simprod*.json ../${proddir}/recprod*.json ; do 
     if [ -e ${f} -a ! -h ${f} ] ; then 
        pid=`basename $f | cut -d"_" -f2`
        if [ $x{pid} != "x12345" ] ; then 
           echo $f >> prodjson.list
        fi
     fi
  done
  if [ ! -e prodjson.list ] ; then 
     myecho "ERROR : No production json files were found" | tee -a ${stfile}
     exit 13
  fi
  myecho "Log files of following production will be obtained and uploaded." | tee -a ${stfile}
  cat prodjson.list | tee -a ${stfile}
  prodlog_cleanup.py --no_confirm --list prodjson.list > prodlog_cleanup.log 2>&1 &
  myecho "prodlog_cleanup.py has started" | tee -a ${stfile}
  exit 12
fi

##################################################################
stword=`grep "### all done : log_upload_list.sh was created." prodlog_cleanup.log 2>/dev/null `
nlogfile=`cat log_upload_list.txt 2>/dev/null | wc -l`
if [ "x${stword}" == "x" ] ; then 
  myecho "prodlog_cleanup.py does not complete yet" | tee -a ${stfile}
  tail -1 `/bin/ls -tr  prod*/log/*/getjobout.log | tail -1` > getjobout.last
  var=`sed -e "s/_/_@/g" prodjson.list | cut -d"/" -f3 | cut -d"@" -f2 | sed -e "s/_/,/g" | tr -d "\n"`
  prodids=${var:0:$[${#var}-1]}
  myecho "ProdIDs are ${prodids}. Last dir now is `/bin/ls -tdv prod*/log/* | tail -1`" | tee -a ${stfile}
  myecho `tail -1 getjobout.last` | tee -a ${stfile}
  exit 10
fi

if [ "x${nlogfile}" == "x" ] ; then 
  myecho "prodlog_cleanup.py has completed, but no logfile listed in log_upload_list.txt" | tee -a ${stfile}
  exit 8 
fi


if [ ! -e log_upload.log ] ; then 
    do_elog &&  {
       elogid=`jsonread production.json __ELOG_ID`
       putelog.py -i ${elogid} -m "Production log files being obtained, they will be uploaded to ILD directory." text 2>&1 | tee -a ${stfile}
    }
    myecho "log_upload.log does not exist. log upload will start." | tee -a ${stfile}
    myecho "Found ${nlogfile} files to be uploaded. They will be uploaded." | tee -a ${stfile}
    echo "### task_step7 starts log upload  `hostname` `date` " > log_upload.status
    ./log_upload_list.sh > log_upload.log 2>&1 
    echo "### task_step7 ended log upload `hostname` `date` " >> log_upload.status

    nsuccess=`grep "Successfully uploaded file to" log_upload_list.log 2>/dev/null | wc -l`
    if [ "x${nsuccess}" != "x${nlogfile}" ] ; then 
      myecho "File upload ERROR, only ${nsuccess} files out of ${nlogfile} were uploaded." | tee -a ${stfile}  
      cp -pv load_upload_list.txt load_upload_list.notyet
      exit 10
    fi
    myecho "Succeeded to upload ${nlogfile} log files." | tee -a ${stfile}
    do_elog && putelog.py loguploaded 2>&1 | tee -a ${stfile}
    myecho "task_step7.sh Log upload ALL TASKS DONE" | tee -a ${stfile}
    myecho "task_step7.sh Log upload ALL TASKS DONE" > task_step7.done
    exit 0

fi

#############################################################
if [ -e log_upload.status ] ; then 
    lul_stat=`grep "### task_step7 ended log upload" log_upload.status 2>/dev/null | wc -l`
    if [ "x${lul_stat}" == "x0" ] ; then 
       myecho "Log upload of previous cron task_step7.sh does not completed yet." | tee -a ${stfile}
       exit 10 
    fi
fi

stword=`grep "File upload ERROR, only" ${stfile}`
stword_done=`grep "Log upload ALL TASKS DONE" ${stfile}`
nerror=`cat log_upload_list.notyet 2>/dev/null | wc -l`
#############################################################
if [ "x${stword_done}" == "x" -o "x${nerror}" != "x0" ] ; then 
   myecho "Detected some error happended in previous log file upload, retry failed file." | tee -a ${stfile}
   mkdir -p uploadlog
   dstr=`date +%Y%m%d-%H%M%S`
   cp log_upload_list.notyet temp
   cat temp | while read a b c ; do 
      filechk=`dirac-dms-lfn-replicas ${a} | grep "No such file or directory"`
      if [ "x${filechk}" == "x" ] ; then
         grep -v ${a} temp > log_upload_list.notyet
      else
         bbase=`basename $b`
         logfile="uploadlog/${dstr}-${bbase}.log"
	 . ${MYPROD_PRODPROD}
         dirac-dms-add-file -ddd ${a} ${b} ${c} > ${logfile} 2>&1 
         stword=`grep "Successfully uploaded file to" ${logfile}`
         if [ "x${stword}" != "x" ] ; then 
            grep -v ${a} temp > log_upload_list.notyet
         fi
      fi
   done
   nleft=`cat log_upload_list.notyet 2>/dev/null | wc -l`
   if [ "x${nleft}" == "x0" ] ; then 
     do_elog && putelog.py loguploaded 2>&1 | tee -a ${stfile}
     myecho "task_step7.sh Log upload ALL TASKS DONE" | tee -a ${stfile}
     myecho "task_step7.sh Log upload ALL TASKS DONE" > task_step7.done
     exit 0
   fi
fi
   
###############
myecho "Come to the end of task_step7.sh. Somthing is left to be done." | tee -a ${stfile}
exit 1

