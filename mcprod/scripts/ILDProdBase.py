'''

Create Chain of productions for ILD

:author: A.Miyamoto
:since: Aug 10, 2017

'''

#pylint: disable=invalid-name, wrong-import-position, bad-whitespace, line-too-long
#pylint: disable=bad-indentation
from __future__ import print_function

import os
import pprint
import json
import subprocess
import datetime
import glob
import copy
import sys

from DIRAC.Core.Base.Script import Script
from DIRAC import gLogger, S_OK, S_ERROR
# Script.parseCommandLine()

if "MCPROD_CONFIG_FILE" not in os.environ: 
   print("ERROR FATAL : MCPROD_CONFIG_FILE environmnet parameter is not defined.")
   exit(-1)

_configfile = os.environ["MCPROD_CONFIG_FILE"]
if not os.path.exists(_configfile):
   print("ERROR FATAL : configfile " + _configfile + " not exist")
   exit(-1)
if sys.version_info.major == 2:
   execfile(_configfile)
else:
   exec(open(_configfile).read())

_thisconfig = "ThisProd_config.py"
if os.path.exists(_thisconfig):
   print("### "+_thisconfig+" exists in current directory. It overwrites settings defined by "+_configfile)
   if sys.version_info.major == 2:
       execfile(_thisconfig)
   else:
       exec(open(_thisconfig).read())
   for line in open(_thisconfig,"r").readlines():
       if len(line.replace("\n","").replace(" ","").replace("#","")):
           print(line.replace("\n",""))
else:
   print("### "+_thisconfig+" for local setting is not defined.") 
      

####################################################################################
'''
A script to submit DDSim Simulation and Marlin reconstruction for ILD Optimization

'''
#####################################################################################

class ILDProdBase(object):

  def __init__(self):
    print( 'Initializer of ILDProdSet was called.' )
    self.clip = self._Params()
    self.clip.registerSwitches()
    Script.parseCommandLine()
    # from ILCDIRAC.Interfaces.API.NewInterface.Applications     import SLCIOSplit, StdHepSplit
    self.doTest = self.clip.doTest
    self.doPrompt = self.clip.doPrompt
    self.applications = []
    self.doOverlay = self.clip.doOverlay
    self.recOptions = self.clip.recOptions
    self.simModels = self.clip.simModels
    self.submitType = self.clip.submitType
    self.development = self.clip.development
    self.newPath = self.clip.newPath

    self.conf = self.clip.loadConfig()
    self.optionFile = self.clip.optionFile
    self.keepRecFile = self.clip.keepRecFile
    self.energyParametersFile = self.clip.energyParametersFile
    self.beamCalBackgroundFile = self.clip.beamCalBackgroundFile
    self.simSelectedFile = self.clip.simSelectedFile
    self.simProdJson = self.clip.simProdJson
    self.ipsmearZOffset = self.clip.ipsmearZOffset
    self.ipsmearZSigma = self.clip.ipsmearZSigma

  def getdoTest(self):
    return self.doTest

  def getdoPrompt(self):
    return self.doPrompt

  class _Params():
    def __init__(self):
      self.init_file = 'none'
      self.dryrun = True
      self.number = 0
      self.configfile = 'production.json'
      self.conf = {}
      import datetime
      dat = datetime.datetime.today()
      self.datestring = dat.strftime("%Y%m%d")
      self.doTest = False
      self.doPrompt = True
      self.doOverlay = False
      self.recOptions = ["o1"]
      self.simModels = ["ILD_l5_v02", "ILD_s5_v02"]
      self.submitType = ""
      self.development = False
      self.newPath = ""
      self.optionFile = ""
      self.energyParametersFile = ""
      self.beamCalBackgroundFile = ""
      self.keepRecFile = True
      self.simSelectedFile = ''
      self.simProdJson = ''
      self.ipsmearZOffset = ''
      self.ipsmearZSigma = ''

    def setConfigFile(self, opt):
      self.configfile = opt
      print( "### configfile will be " + self.configfile )
      return S_OK()
  
    def loadConfig(self):
      self.conf = json.load(open(self.configfile))
      if self.newPath != "":
        for dtype in ["SIM", "DST", "REC", "DSTMERGED", "SIMLOG", "RECLOG"]:
          keyconf = "__DIRECTORY_%s" % dtype
          self.conf[keyconf]=str(self.conf[keyconf]).replace("/ilc/prod/ilc", self.newPath)
      return self.conf
  
    def setNumber(self, opt):
      self.number = opt
      print( "### Production number is "+str(self.number) )
      return S_OK()
  
    def setDryRun(self, dummy_opt):
      self.dryrun = True
      return S_OK()
  
    def setNoDryRun(self, dummy_opt):
      self.dryrun = False
      return S_OK()
  
    def setDoTest(self, dummy_opt):
      self.doTest = True
      return S_OK()

    def setDoOverlay(self, dummy_opt):
      self.doOverlay = True
      print( "### doOverlay is set to "+str(self.doOverlay) )
      return S_OK()

    def setNoPrompt(self, dummy_opt):
      self.doPrompt = False
      return S_OK()

    def setDateString(self, opt):
      self.datestring = opt
      print( "### Production date string is "+self.datestring )
      return S_OK()

    def setRecOption(self, opt):
      if ":" in opt:
          self.recOptions = opt.split(":")
      else:
          self.recOptions = [opt]
      print( "### Rec. Options specified is " + str(self.recOptions) )
      return S_OK()

    def setSimModels(self, opt):
      if ":" in opt:
          self.simModels = opt.split(":")
      else:
          self.simModels = [opt]
      print( "### Sim models specified is " + str(self.simModels))
      return S_OK()
  
    def setSubType(self, opt):
      if opt not in ["sim", "nobg", "ovl"]:
        print( "Undefined submit type " + opt + " was specified." )
        return S_ERROR()
      self.submitType = opt
      print( "### Production submit type is " + str(self.submitType) )
      return S_OK()

    def setDevelopment(self, opt):
      self.development = True
      return S_OK()

    def setNewPath(self, opt):
      self.newPath = opt
      print( "### Production data path will be modified to " + self.newPath )
      return S_OK()

    def setOptionFile(self, opt):
      self.optionFile = opt
      print( "### Production additional parameters will be obtained from " + self.optionFile )
      return S_OK()

    def setOutputDSTOnly(self, opt):
      self.keepRecFile = False
      print( "### Production marlin does not output REC file. keepRecFile is " + str(self.keepRecFile) )
      return S_OK()

    def setEnergyParametersFile(self, opt):
      self.energyParametersFile = opt
      print( "### Production marlin EnergyParametersFile is " + self.energyParametersFile )
      return S_OK()

    def setBeamCalBackgroundFile(self, opt):
      self.beamCalBackgroundFile = opt
      print( "### Production marlin BeamCalBackgroundFile is " + self.beamCalBackgroundFile )
      return S_OK()

    def setSimSelectedFile(self, opt):
      self.simSelectedFile = opt
      print( "### Production marlin query sim SelectedFile = " + str(self.simSelectedFile) )
      return S_OK()

    def setSimProdJson(self, opt):
      self.simProdJson = opt
      print( "### Get sim production information for rec production from = " + str(self.simProdJson) )
      return S_OK()

    def setIPSmearZOffset(self, opt):
      self.ipsmearZOffset = opt
      print( "### IP smear Z offset = " + str(self.ipsmearZOffset) )
      return S_OK()

    def setIPSmearZSigma(self, opt):
      self.ipsmearZSigma = opt
      print( "### IP smear Z sigma = " + str(self.ipsmearZSigma) )
      return S_OK()

    def registerSwitches(self):
      Script.registerSwitch('N:', 'number=', 'Serial number attached to xml file name(default=0)', self.setNumber)
  #    Script.registerSwitch('I:', 'initfile=', 'A python file to set parameters (default=default_para.py)', self.setInitFile)
      Script.registerSwitch('', 'conf=', 'Configuration file name ( default=%s )' % self.configfile, self.setConfigFile)
      Script.registerSwitch('', 'dry', 'Do dry run', self.setDryRun)
      Script.registerSwitch('', 'nodry', 'Do not dry run, submit transformation', self.setNoDryRun)
      Script.registerSwitch('', 'test', 'Transformation with a test mode for code debugging (default=do Test)', self.setDoTest)
      Script.registerSwitch('Q', 'noPrompt', 'Proceed without prompt.(default=with prompt)', self.setNoPrompt)
      Script.registerSwitch('', 'doOverlay', 'Do overlay in Marlin job.(default=no_overlay)', self.setDoOverlay)
      Script.registerSwitch('', 'datestring', 'Date string attached to the xml file name(default=today)', self.setDateString)
      Script.registerSwitch('', 'devel', 'Do not execute dirac command. for code development.', self.setDevelopment)
      Script.registerSwitch('', 'submitType=', 'Type of submit job, sim, nobg, or ovl', self.setSubType)
      Script.registerSwitch('R:', 'recOptions=', 'Collon(:) separated list of options for reconstruction(default=o1)', self.setRecOption)
      Script.registerSwitch('S:', 'simModels=', 'collon(:) separated list of sim models(default=ILD_l5_v02:ILD_s5_v02', self.setSimModels)
      Script.registerSwitch('', 'DSTonly', 'Do not output rec files in marlin production', self.setOutputDSTOnly)
      Script.registerSwitch('', 'EnergyParametersFile=', 'EnergyParametersFile for marlin.', self.setEnergyParametersFile)
      Script.registerSwitch('', 'BeamCalBackgroundFile=', 'BeamCalBackgroundFile for marlin.', self.setBeamCalBackgroundFile)
      Script.registerSwitch('', 'SimSelectedFile=', 'Add SelecedFile meta of this value for sim file query.', self.setSimSelectedFile)
      Script.registerSwitch('','NewPath=', 'Overwrite /ilc/prod/ilc in config to new path', self.setNewPath) 
      Script.registerSwitch('','IPSmearZOffset=', 'Z offset for IP smear', self.setIPSmearZOffset)
      Script.registerSwitch('','IPSmearZSigma=', 'Z sigma for IP Smear', self.setIPSmearZSigma) 
      Script.registerSwitch('','OptionFile=', 'A json file, which overwrites default production parameters', self.setOptionFile) 
      Script.registerSwitch('','SimProdJson=', 'A json file for rec, containing information of sim production.', self.setSimProdJson) 
      msg = '%s [options]\n' % Script.scriptName
      msg += 'Function: Submit transformations for ILDProductionChainOpt2017.py\n'
      msg += 'Default is dry run, notest \n'
      msg += 'Example: python ILDProdBase.py --dry --conf production.json\n'
      msg += '       : python ILDProdBase.py --nodry --conf production.json'
   
      Script.setUsageMessage(msg)

# ####################################################################################
  def setDefault(self, dd4hep=True):
    ''' Set default production parameters '''

    doSim = True if self.submitType == "sim" else False
    doRec = True if self.submitType in ["ovl", "nobg"] else False

    isTest = self.doTest
    params={'analysis':'ILD-Opt',
            'sim':{}, 
            'rec':{},
            'dd4hep':dd4hep}
    #	    'banned_sites':[""], 

    params["dryrun"] = self.clip.dryrun
    params["dd4hep"] = dd4hep

    params["my_evtclass"] = self.conf["__EVTCLASS"]
    params["my_evttype"] = self.conf["__EVTTYPE"]
    params["energyMachinepara"]= self.conf["__ENERGY_MACHINEPARA"]

    params["sim"].update({"basepath":self.conf["__DIRECTORY_SIM"]})

    if doSim:
      params["sim"].update({"matchToInput":self.conf["__STDHEP_UPLOAD_DIR"]})
  
    if doRec:
      params["rec"].update({"recPath":self.conf["__DIRECTORY_REC"], 
                   "dstPath":self.conf["__DIRECTORY_DST"]})
    params["conf"] = self.conf
    energy = self.conf["__ENERGY"]

    params['energy']=energy
    if not dd4hep:
      params["doOverlay"] = self.doOverlay
      edepConfig = {'1000':{'config':'v01-16-p03',     'GGToHadInt':4.1,  'BkgEvtType':'aa_lowpt'}, 
                     '500':{'config':'v01-16-p05_500', 'GGToHadInt':1.7,  'BkgEvtType':'aa_lowpt2'}, 
                     '350':{'config':'v01-16-p09_350', 'GGToHadInt':0.33, 'BkgEvtType':'aa_lowpt'}, 
                     '250':{'config':'v01-16-p010_250','GGToHadInt':0.2,  'BkgEvtType':'aa_lowpt'}}
      simpara = {"SimVersion":"080003", 
                 "sim_steering_file":"bbudsc_3evt.steer",
                 "ILDConfig":"v01-14-01-p00",
                 "input_sand_box":[""], 
                 "output_sand_box":[""]}
      simpara["SE"] = self.conf["__SIM_SE"] if "__SIM_SE" in self.conf else "DESY-SRM"
      simpara["nbtasks"] = self.conf["__SIM_NBTASKS"] if "__SIM_NBTASKS" in self.conf else -1
      recpara = {"MarlinVersion":"ILCSoft-01-16-02-p1", 
                 "ILDConfig":edepConfig[energy]["config"],
                 "GGToHadInt":edepConfig[energy]["GGToHadInt"],
                 "BkgEvtType":edepConfig[energy]["BkgEvtType"],
                 "BXOverlay":1,
                 "marlin_steering_file":"bbudsc_3evt_stdreco.xml",
                 "gear_file":"GearOutput.xml",
                 "input_sand_box":[""],
                 "output_sand_box":["*.log", "*.sh", "bbudsc_*.xml", "marlin*.xml"]}
      recpara["SE"] = self.conf["__REC_SE"] if "__REC_SE" in self.conf else "DESY-SRM"
      recpara["nbtasks"] = self.conf["__REC_NBTASKS"] if "__REC_NBTASKS" in self.conf else -1
    else:
      vsmearopt = ""
      zoffset = ""
      zsigma = ""
      beamtype=self.conf["__EPOL"] + self.conf["__PPOL"] 
      if params["energyMachinepara"] in MCProd_ZOffset_And_ZSigma:
         zoffset_and_zsigma = MCProd_ZOffset_And_ZSigma[params["energyMachinepara"]]
         if beamtype in zoffset_and_zsigma:
            zoffset = zoffset_and_zsigma[beamtype][0] 
            zsigma = zoffset_and_zsigma[beamtype][1] 

      if self.clip.ipsmearZOffset != "" :
            zoffset = self.clip.ipsmearZOffset

      if zoffset != "" :
         vsmearopt += " --vertexOffset 0.0 0.0 %s 0.0 " % zoffset

      if self.clip.ipsmearZSigma != "" :
            zsigma = self.clip.ipsmearZSigma
      
      if zsigma != "" :
         vsmearopt += " --vertexSigma 0.0 0.0 %s 0.0 " % zsigma
      
      if vsmearopt == "" :
          if doSim:
            print( "IP is not smeared in DDSIM because Zsmear/Zoffset parameter is not available for " +
                   params["energyMachinepara"] + " or " + beamtype + " beamtype" )
      else:
          print( "IP position is smreared/offset with " + vsmearopt )

      params["doOverlay"] = self.doOverlay
      simpara = {"SimVersion":MCProd_Sim_Version, 
                 "sim_steering_file":MCProd_Sim_Steering,
                 "ILDConfig":MCProd_Sim_ILDConfig,
                 "input_sand_box":[""],
                 "output_sand_box":[""],
                 "extraCLIArguments":""}
      simpara["SE"] = self.conf["__SIM_SE"] if "__SIM_SE" in self.conf else "DESY-SRM"
      simpara["nbtasks"] = self.conf["__SIM_NBTASKS"] if "__SIM_NBTASKS" in self.conf else -1
      simpara["banned_sites"] = self.conf["__SIM_BANNED_SITES"] if "__SIM_BANNED_SITES" in self.conf else ""

      if "calib" in self.conf["__ENERGY_MACHINEPARA"].lower():
        simpara["extraCLIArguments"] += " --crossingAngleBoost 0.0 " 
      if vsmearopt:
        simpara["extraCLIArguments"] += vsmearopt

      recpara = {"MarlinVersion":MCProd_Rec_Version,
                 "ILDConfig":MCProd_Rec_ILDConfig,
                 "marlin_steering_file":"MarlinStdReco.xml",
                 "input_sand_box":[""],
                 "output_sand_box":[ "*.log", "MarlinStdReco.xml", "MarlinStdRecoParsed.xml", "marlin*.xml", "*.sh", "*.txt", "*.root"]}
      recpara["extraCLIArguments"] = " --global.MaxRecordNumber=0 " if "__REC_MAX_RECORDS" not in self.conf \
			else " --global.MaxRecordNumber=%s " % self.conf["__REC_MAX_RECORDS"]
      recpara["SE"] = self.conf["__REC_SE"] if "__REC_SE" in self.conf else "DESY-SRM"
      recpara["nbtasks"] = self.conf["__REC_NBTASKS"] if "__REC_NBTASKS" in self.conf else -1
      recpara["banned_sites"] = self.conf["__REC_BANNED_SITES"] if "__REC_BANNED_SITES" in self.conf else ""
      if int(energy) in [250, 350, 500, 1000] : 
          recpara["extraCLIArguments"] += " --constant.CMSEnergy=%s " % energy

      if self.energyParametersFile != "":
          recpara["extraCLIArguments"] += " --constant.EnergyParametersFile=%s " % self.energyParametersFile
       
      if self.beamCalBackgroundFile != "":
          recpara["extraCLIArguments"] += " --constant.BeamCalBackgroundFile=%s " % self.beamCalBackgroundFile
       
      if params["doOverlay"]:
        # ovldata = self.load_OverlayInput_Data("ovldata.csv")
        ovldata = MCProd_Overlay_Data
        if params["energyMachinepara"] in ovldata:
          recpara["OverlayInput"] = ovldata[params["energyMachinepara"]]
          recpara["OverlayInputBasePath"] = MCProd_OverlayInput_BasePath
          recpara["OverlayInputILDConfig"] = MCProd_OverlayInput_ILDConfig
          recpara["extraCLIArguments"] += " --constant.RunOverlay=true "
        else:
          print( "### ERROR : Overlay is requested, but ovldata.csv does contain data for energy%s" % energy )
          exit(-1)

      if "calib" in self.conf["__ENERGY_MACHINEPARA"] or "nobeam" in self.conf["__ENERGY_MACHINEPARA"]:
          recpara["extraCLIArguments"] += " --constant.RunBeamCalReco=false "

    if isTest:
      if doSim:
        simpara["SE"] = "KEK-DISK"
      if doRec:
        recpara["SE"] = "KEK-DISK"

    params['sim'].update(simpara)
    params['rec'].update(recpara)

    if doSim:
      meta0 = {}
      meta0['Datatype'] = 'gensplit'
      meta0['Machine'] = 'ilc'
      ( meta0['Energy'], meta0['MachineParams'] ) = self.conf['__ENERGY_MACHINEPARA'].split('-')
      meta0['ProdID'] = self.conf['__PRODID_FOR_GENSPLIT']
      meta0['SelectedFile'] = 1
      if '__GenProcessName' in self.conf:
         meta0['GenProcessName'] = self.conf['__GenProcessName']
      if '__GenProcessID' in self.conf:
         meta0['GenProcessID'] = self.conf['__GenProcessID']
      params['sim']['meta'] = meta0

    if self.optionFile != "":
      print( "### ILDProdBase default parameters were overraid by those obtained from " + self.optionFile )
      opt = json.load(open(self.optionFile))
      for k1,v1 in opt.items():
        if not isinstance(v1, dict):
          params[str(k1)] = v1
          print( "=== params[\"%s\"] = %s" % (str(k1), v1) )
        else:
          for k2,v2 in v1.items():
            if not isinstance(v2, dict):
              params[str(k1)][str(k2)] = v2
              print( "=== params[\"%s\"][\"%s\"] = %s" % (str(k1), str(k2), v2) )
            else:
              for k3,v3 in v2.items():
                if not isinstance(v3, dict):
                  params[str(k1)][str(k2)][str(k3)] = v3
                  print( "=== params[\"%s\"][\"%s\"][\"%s\"] = %s" % (str(k1), str(k2), str(k3), v3) )
                else:
                  for k4,v4 in v3.items():
                    if not isinstance(v4, dict):
                      params[str(k1)][str(k2)][str(k3)][str(k4)] = v4
                      print( "=== params[\"%s\"][\"%s\"][\"%s\"][\"%s\"] = %s" % (str(k1), str(k2), str(k3), str(k4), v4) )
                    else:
                      print( "### ERROR : too deep level of parameter dict was specified.  optionFile was " + self.optionFile )
                      pprint.pprint(opt)
                      exit(-1)


    return params

# ######################################################################################
  def load_OverlayInput_Data(self, csvfile):
    import csv
    fcsv = open(csvfile)
    reader = csv.reader(fcsv, delimiter=',')
    header = next(reader)
    # csv file format:
    #   ecm,sim_detector,processor_name,evttype,expBG,ProdID,subdir 
    ovldata = {}
    for row in reader:
      if row[0] not in ovldata:
        ovldata[row[0]] = {}
      if row[1] not in ovldata[row[0]]:
        ovldata[row[0]][row[1]] = {}
      if row[2] not in ovldata[row[0]][row[1]]:
        ovldata[row[0]][row[1]][row[2]] = {}
      ovldata[row[0]][row[1]][row[2]] = {"evttype":row[3], "expBG":row[4], "ProdID":row[5], "subdir":row[6]}

    fcsv.close()

    # pprint.pprint(ovldata)

    return ovldata

# ######################################################################################
  def start_Production(self, prodtype, applications, params):
    from ILCDIRAC.Interfaces.API.NewInterface.ILDProductionJobOpt2017 import ILDProductionJobOpt2017
    ''' 
    Submit production consists of applications 
    nput: prodtype = sim, rec, recov
    # prodtype = MCSimulation_ILD, MCReconstruction_ILD, MCReconstruction_Overlay_ILD
    '''
    prodTypeName={"sim":{"name":"MCSimulation_ILD"},
	          "rec":{"name":"MCReconstruction_ILD"},
                  "recov":{"name":"MCReconstruction_Overlay_ILD"}}

    print( "### Start production ..." )

    prod = ILDProductionJobOpt2017()
    prkey = "rec"
    outkey = "dstm"
    if prodtype == "sim":
      prod.basepath = params["sim"]["basepath"]
      prod.setUseSoftTagInPath(True)
      prkey = "sim"
      outkey = "rec"
    else:
      prod.setReconstructionBasePaths(params["rec"]["recPath"], params["rec"]["dstPath"])
      params[outkey]={}
     
    if "detector_basename" in params[prkey]:
      prod.setDetectorForBasename( params[prkey]["detector_basename"] )
    prod.matchToInput = params[prkey]["matchToInput"]
    prod.setDryRun(params["dryrun"])
    prod.setILDConfig(params[prkey]["ILDConfig"]) ###
    prod.setEvtClass(params["my_evtclass"])
    prod.setEvtType(params["my_evttype"])
    prod.setLogLevel("verbose")
    prod.setProdType(prodTypeName[prodtype]["name"])
    # prod.setBannedSites(params["banned_sites"])
    banned_sites = params[prkey]["banned_sites"]
    if isinstance(banned_sites,str):
       for seprat in [",",";",":"]:
          if seprat in banned_sites:
             banned_sites = params[prkey]["banned_sites"].split(seprat)
    prod.setBannedSites(banned_sites)
    prod.setInputSandbox(params[prkey]["input_sand_box"])  ##
    prod.setOutputSandbox(params[prkey]["output_sand_box"]) ##
    prod.setProdPlugin("Limited")

    prod.setOutputSE(params[prkey]["SE"])  ##

    res = prod.setInputDataQuery(params[prkey]["meta"])  ##
    if not res['OK']:
      print( "### Error: start_Production returns with an error. Error message was " )
      print( res['Message'] )
      exit(1)
    prname = prodtype 
    if "rec" in prodtype and not self.keepRecFile:
        prname = prodtype.replace("rec", "dst")
    wname = params["analysis"]+"_"+prname+"_"+str(params["energy"])+params["additional_name"]
    prod.setWorkflowName(wname)
    prod.setProdGroup(params["analysis"]+"_"+str(params["energy"]))

    #Add the application
    for appli in applications:
      res = prod.append(appli)
      if not res['OK']:
        print( "## Failed to add "+appli.appname+" to the production." )
        print( res['Message'] )
        exit(1)
    prod.addFinalization(True, True, True, True)
    descrp = "%s, %s" % (params["detectorModel"], prodtype)

    if "additional_name" in params:
      descrp += ", %s"% params["additional_name"]
    prod.setDescription(descrp)
    res = prod.createProduction()
    if not res['OK']:
      print( res['Message'] )

    res = prod.setProcessIDInFinalPath()
    if not res['OK']:
      print( res['Message'] )

    prod.setNbOfTasks(params[prkey]["nbtasks"]) ###

    res = prod.finalizeProd()
    if not res['OK']:
      print( res['Message'] )
      exit(1)
    print( "### "+prodtype+" production was created. " )
    print( res )
    meta = prod.getMetadata()

    print( "### Done With a creation of "+prodtype+" transformation")
    print( "\n\n\n\n\n" )
    
    # Save status information
    params[outkey]["meta"] = meta
    if outkey == "dstm":
      params[outkey]["meta_orig"] = params[outkey]["meta"]
      if params[outkey]["meta"]["Datatype"] != "DST":
         print( "### WARNING : getMetaData of the production returns with %s in Datatype field.  It was modified to DST to avoid a dst-merge problem." % params[outkey]["meta"]["Datatype"] )
         params[outkey]["meta"]["Datatype"] = "DST"

    prodid = meta["ProdID"]
    now = datetime.datetime.now()
    cwd = os.getcwd()
    hostname = os.environ["HOSTNAME"]
    status = {"step":"Production_Submitted", "date":"%4d-%2.2d-%2.2d" % ( now.year, now.month, now.day), 
              "time":"%2.2d:%2.2d:%2.2d" % ( now.hour, now.minute, now.second ),
              "ProdID":prodid, "ProdType":prodtype,  "ProdTypeName":prodTypeName[prodtype]["name"], 
              "hostname":hostname, "cwd":cwd}
    params["status"] = status
    detname = params["detectorModel"] if prkey == "sim" else params["rec"]["detector_basename"]
    model_short = detname.replace("ILD_","")
    outjson = prkey + "prod_%s_%s.json" % (str(prodid), model_short)
    json.dump(params, open(outjson, "w"))
    print( "### %s params and meta were written to %s." % (prkey, outjson) )

    return params

# ######################################################################################
  def create_Reconstruction(self, params):
    from ILCDIRAC.Interfaces.API.NewInterface.Applications import Mokka, Marlin, OverlayInput, DDSim
    doOverlay = params["doOverlay"]
    dd4hep = params["dd4hep"]

    app = Marlin()
    app.setDebug()
    app.setVersion(params["rec"]["MarlinVersion"])   ##
    app.setSteeringFile(params["rec"]["marlin_steering_file"])
    if "ILCSoft-01" in params["rec"]["MarlinVersion"]:
      if self.older_ILCSoft_Version(params["rec"]["MarlinVersion"],"ILCSoft-01-19-05_gcc49"):
        app.setGearFile(params["rec"]["gear_file"])
    if not doOverlay:
      app.setEnergy(params["energy"])
    if dd4hep:
      app.setDetectorModel(params["rec"]["detectorModel"])
#       app.setDetectorModel(params["detectorModel_rec"])
      if "extraCLIArguments" not in params["rec"]:
         params["rec"]["extraCLIArguments"] = " --constant.DetectorModel=%s " % params["rec"]["detectorModel"].replace(" ","")
      elif "--constant.DetectorModel" not in params["rec"]["extraCLIArguments"]:
         params["rec"]["extraCLIArguments"] += " --constant.DetectorModel=%s " % params["rec"]["detectorModel"].replace(" ","")

    if not self.keepRecFile:
      app.setKeepRecFile(False)

    if self.simSelectedFile != '':
      params["rec"]["meta"].update({"SelectedFile":self.simSelectedFile})

    # pprint.pprint(params["rec"])

    if "extraCLIArguments" in params["rec"]:
      app.setExtraCLIArguments(params["rec"]["extraCLIArguments"])
    if "NumberOfEvents" in params["rec"]:
      app.setNumberOfEvents(int(params["rec"]["NumberOfEvents"]))

    return app

# ######################################################################################
  def older_ILCSoft_Version(self, thisVersion, refVersion):
    (refstr, dummy) = refVersion.split('_',1)
    refnum = int(refstr.replace('ILCSoft','').replace('-',''))
    (thisstr, dummy) = thisVersion.split('_',1)
    thisnum = int(thisstr.replace('ILCSoft','').replace('-',''))
    if thisnum < refnum:
      return True 
    else:
      return False


# ######################################################################################
  def create_Overlay(self, params):
    from ILCDIRAC.Interfaces.API.NewInterface.Applications import Mokka, Marlin, OverlayInput, DDSim
     
    app = OverlayInput()
    app.setMachine("ilc_dbd")       #Don't touch, this is how the system knows what files to get
    app.setEnergy(params["energy"]) #Don't touch, this is how the system knows what files to get
    app.setDetectorModel(params["sim"]["detectorModel"]) #Don't touch, this is how the system knows what files to get
    app.setBXOverlay(params["rec"]["BXOverlay"])
    app.setGGToHadInt(params["rec"]["GGToHadInt"])
    app.setBkgEvtType(params["rec"]["BkgEvtType"])
    
    return app 

# ######################################################################################
  def create_OptOverlay(self, params):
    from ILCDIRAC.Interfaces.API.NewInterface.Applications import Mokka, Marlin, OverlayInput, DDSim

    energy = params["energy"]
    energyMachinepara = params["energyMachinepara"]
    sim_detector = params["sim"]["detectorModel"]
    if "OverlayInput" not in params["rec"]:
        print( "### Data for OverlayInput is not defined." )
        exit(-1)
    elif sim_detector not in params["rec"]["OverlayInput"]:
        print( "### Sim_detector %s for OverlayInput is not found." % sim_detector )
        exit(-1)
    
    self.applications = []
    for procname, ovlparam in sorted(params["rec"]["OverlayInput"][sim_detector].items()):
        print( "### Creating OverlayInput for Procname=%s" % procname )
        EMpara = params["energyMachinepara"] if "EMpara" not in ovlparam else ovlparam["EMpara"]
        ovlpath = "%s/%s/%s/%s/%s/%8.8d  " % ( params["rec"]["OverlayInputBasePath"], EMpara,
            ovlparam["evttype"], sim_detector, params["rec"]["OverlayInputILDConfig"], int(ovlparam["ProdID"]) )
        print( "### OverlayInput path=%s" % ovlpath )
  
        app = OverlayInput()
        app.setMachine("ilc_dbd")       #Don't touch, this is how the system knows what files to get
        # app.setEnergy(params["energy"]) #Don't touch, this is how the system knows what files to get
        # app.setDetectorModel(params["sim"]["detectorModel"]) #Don't touch, this is how the system knows what files to get
  
        app.setProcessorName(procname)
        app.setBkgEvtType(ovlparam["evttype"])
        app.setPathToFiles(ovlpath)
        app.setGGToHadInt(float(ovlparam["expBG"]))
        # app.setProdID(int(ovlparam["ProdID"]))
        app.setBXOverlay(1) # Number of bunch crossings to be overlaid to a signal event
        self.applications.append(app)
          
    return

# ######################################################################################
  def create_Simulation(self, params):
    from ILCDIRAC.Interfaces.API.NewInterface.Applications import Mokka, Marlin, OverlayInput, DDSim
    dd4hep = params["dd4hep"]

    app = None
    if dd4hep:
      app = DDSim()
      app.setVersion(params["sim"]['SimVersion']) ##
      app.setDetectorModel(params["sim"]['detectorModel'])
      app.setSteeringFile(params["sim"]["sim_steering_file"])  ##
      if "extraCLIArguments" in params["sim"]:
         app.setExtraCLIArguments(params["sim"]["extraCLIArguments"])
      if "NumberOfEvents" in params["sim"]:
         app.setNumberOfEvents(int(params["sim"]["NumberOfEvents"]))
    else:
      app = Mokka()
      app = setVersion(params["sim"]["SimVersion"]) ##
      app.setDetectorModel(params["detectorModel"])
      app.setSteeringFile(params["sim"]["sim_steering_file"]) ##
      app.setDbSlice("mokka-08-00-dbdump.sql")

    return app

# =================================================================================
# =================================================================================

# ####################################################################################
  def sim_production(self, input_params, detector):
    '''
    Submit simulation and Reconstruction for v01-19-05 and later
    params: default_params
    params: detectors, {"sim":"sim_model", "rec":"rec_model"}
            "sim_model" is a model for simulation or a sim_model of input file
            to reconstruction.
    params: steps, Steps to be executed. steps=["sim", "rec", "recov"]

    '''
    import copy
    from DIRAC.Core.Utilities.PromptUser import promptUser
    from DIRAC.ConfigurationSystem.Client.Helpers.Operations    import Operations

    params = copy.deepcopy(input_params)
    params["detectorModel"] = detector
    params["sim"]["detectorModel"] = detector

    print( "### sim_and_rec_production will start Sim Production" )
    params["additional_name"] = '_' + '_'.join([params['my_evttype'],
      params["detectorModel"].replace('ILD_',''), str(self.clip.datestring), str(self.clip.number)])
    sim = self.create_Simulation(params)
    params = self.start_Production("sim", [sim], params)

    params["OK"]=True
    return params
#     exit(0)



# ####################################################################################
  def rec_production(self, default_params, rec_option):
    '''
    Submit Reconstruction for v01-19-05 and later
    params: default_params ( parameters after simulation )
    params: detectors, {"sim":"sim_model", "rec":"rec_model"}
            "sim_model" is a model for simulation or a sim_model of input file
            to reconstruction.
           if params["doOverlay"] = True, do background overlay
    rec_option : "o1", "o2", "o3", ...  
    '''

    import copy
    from DIRAC.Core.Utilities.PromptUser import promptUser
    from DIRAC.ConfigurationSystem.Client.Helpers.Operations    import Operations

    params = copy.deepcopy(default_params)
    sim_detector = params["sim"]["detectorModel"]
    simdkey = sim_detector.split('_')
    rec_detector = '_'.join([simdkey[0], simdkey[1], rec_option, simdkey[2]])
    params["detectorModel"] = rec_detector
    params["rec"]["detectorModel"] = rec_detector
    params["rec"]["detector_basename"] = rec_detector if params["doOverlay"] else rec_detector+"_nobg" 

    print( "### sim_and_rec_production will start Rec production. " )
    params["rec"]["matchToInput"] = "/".join([params["sim"]["basepath"][:-1], "sim", params["energyMachinepara"],
             params["my_evttype"], sim_detector, params["sim"]["ILDConfig"] ] )
    print( "### DoOverlay is "+str(params["doOverlay"]) )
    params["additional_name"] = '_' + '_'.join([params['my_evttype'],
      params["rec"]["detector_basename"].replace('ILD_',''), str(self.clip.datestring), str(self.clip.number)])

    if not params["doOverlay"]:
      rec = self.create_Reconstruction(params)
      params = self.start_Production("rec", [rec], params)
    else:
      print( "Overlay requested, but not implemented yet." )
      self.create_OptOverlay(params)
      rec = self.create_Reconstruction(params)
      self.applications.append(rec)
      pprint.pprint(self.applications)
      params = self.start_Production("recov", self.applications, params)

    params["OK"] = True
    return params
#     exit(0)

# ###########################################################
  def Submit(self, params):
    ''' Submit production, sim, ovl, or nobg depending on the input arguments '''

    from DIRAC.Core.Utilities.PromptUser import promptUser
    
    subtype = self.submitType
    if subtype not in ["sim", "ovl", "nobg"]:
      print( "ERROR : Undefined submit type, " + subtype, ", was requested." )
      exit(-1)

    if ( self.doOverlay and subtype == "nobg" ) or ( not self.doOverlay and subtype == "ovl" ) :
      print( "ERROR : subtype=" + subtype + " and doOverlay=" + str(self.doOverlay) + " is not consistent." )
      exit(-1)

    # ====================================================
    if subtype == "sim":
      pprint.pprint(params)
      print( "SimModels : " + str(self.simModels) )
      if self.doPrompt:
        res = promptUser('### Continue?', ['y', 'n'], 'n')
        if not res['OK'] or not res['Value'].lower() == 'y':
          exit(-1)

      for model in self.simModels:
        if self.development:
          print( "prod.sim_production(params, model=%s)" % model )
        else:
          simpara = prod.sim_production(params, model)
     
    # ===================================================
    elif subtype in ["nobg", "ovl"]:
      if self.doOverlay:
         pprint.pprint(params)
      print( "RecOptions : ", str(self.recOptions) )
      if self.doPrompt:
        res = promptUser('### Continue?', ['y', 'n'], 'n')
        if not res['OK'] or not res['Value'].lower() == 'y':
          exit(-1)

      simprod_json = glob.glob("simprod_*.json") if self.simProdJson == "" else [ self.simProdJson ]
      if len(simprod_json) == 0:
        print("NO reconstruction production was submitted because no simprod json file was found.")
      else:
        for jfile in simprod_json :
          (dtype, prodid, detector, vers) = jfile.split("_",3)
          if prodid  != "12345":
            parload = json.load(open(jfile))
            parused = copy.deepcopy(params)
            parused["sim"] = parload["sim"]
            parused["rec"]["meta"] = parload["rec"]["meta"]
            for recoption in self.recOptions:
              if self.development:
                print( "prod.rec_production(parused, recoption=%s)" % recoption )
              else:
                parprod = prod.rec_production(parused, recoption)
  
      return S_OK()

# ###########################################################
if __name__ == "__main__":
  ''' ILD Production main '''


  prod = ILDProdBase()

  params = prod.setDefault()

  prod.Submit(params)

