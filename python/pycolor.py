
class pycolor:
    BLACK = '\033[30m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    PURPLE = '\033[35m'
    CYAN = '\033[36m'
    WHITE = '\033[37m'

    DARK_GRAY = '\033[1;30m'
    LIGHT_RED = '\033[1;31m'
    LIGHT_GREEN = '\033[1;32m'
    BROWN = '\033[1;33m'
    LIGHT_BLUE = '\033[1;34m'
    LIGHT_PURPLE = '\033[1;35m'
    LIGHT_CYAN = '\033[1;36m'
    LIGHT_GRAY = '\033[1;37m'


    END = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    INVISIBLE = '\033[08m'
    REVERCE = '\033[07m'

    c = {"black":BLACK, "red":RED, "green":GREEN, \
            "yellow":YELLOW, "blue":BLUE, "purple":PURPLE, \
            "cyan":CYAN, "white":WHITE, \
            "end":END, \
            "dark gray":DARK_GRAY, "light red":LIGHT_RED, "light green":LIGHT_GREEN, \
            "brown":BROWN, "light blue":LIGHT_BLUE, "light purple":LIGHT_PURPLE, \
            "light cyan":LIGHT_CYAN, "light gray":LIGHT_GRAY, 
            "bold":BOLD, "underline":UNDERLINE,  \
            "invisible":INVISIBLE, "reverce":REVERCE}

    def __init__(self):
        ''' Initialize '''
        ''' print "Hello "  ''' 

    def cdump(self):
        ''' print colored text '''
        for k, v in sorted(self.c.items()):
            print( v + k + self.c["end"] )

if __name__ == "__main__":
    pyc = pycolor()
    pyc.cdump()
