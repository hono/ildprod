#
# Submit a job to merge DST files produced by USERJob based production.
# 


from DIRAC.Core.Base import Script
Script.parseCommandLine()
from ILCDIRAC.Interfaces.API.NewInterface.UserJob import UserJob
from ILCDIRAC.Interfaces.API.NewInterface.Applications import GenericApplication
from ILCDIRAC.Interfaces.API.DiracILC import DiracILC
import pprint
import sys
import os
import json


# ######################################################
def submit_userdstm_job(jobmeta, debug=False):

    diracilc = DiracILC(True, "job.repo")
  
    dstlist = []
    for line in open(jobmeta["list_of_dstfiles"]):
        dstlist.append(line.replace("\n",""))    
  
    j = UserJob()
    j.setJobGroup(jobmeta["jobgroup"])
    j.setName(jobmeta["jobname"])
    j.setInputData(dstlist)
    j.setInputSandbox(jobmeta["inputSandbox"])
   
    if "Destination" in jobmeta:
        j.setDestination(jobmeta["Destination"])
  
    #  Define application     
    app = GenericApplication()
    app.setScript(jobmeta["jobScript"])
    res = j.append(app)
    if not res['OK']:
        print( "Failed to append %s to job %s" % (jobmeta["jobScript"], jobname) )
        print( res['Message'] )
        exit(1)
  
    j.setOutputSandbox( jobmeta["outputSandbox"])
    j.dontPromptMe()
    
    res = None
    if debug:
       res = j.submit(diracilc, mode='local')
  #    res={"OK":True, "Value":0000}
    else: 
  #    res={"OK":True, "Value":0000}
       res = j.submit(diracilc)
    if not res['OK']:
       print( "Failed to submit job %s " % (jobname) )
       pprint.pprint(res)
       exit(1)
  
    print( "A job %s submitted. jobid=%s" % (jobname, str(res['Value'])) )
  
    jobinfo = {"JobGroup":jobmeta["jobgroup"], "JobName":jobmeta["jobname"], "jobid":res['Value'] } 
    jobmeta.update({"submit":jobinfo})
  
    fjson = open("submit.json","w")
    json.dump(jobmeta, fjson)
    fjson.close()
    print( "Job info. was written to submit.json" )
  
    del j
  
    return {"OK":True, "Value":jobmeta}
  
# #################################################################
def set_params():

    inputSandbox=["dirac-ildapp-dstmerge.sh"]
    inputSandbox.extend(["dstlist.txt", "jobinfo.txt", "marlin_merge.xml.in"])
    prodtools = os.getenv("MYPROD_TOOLS")
    inputSandbox.extend([prodtools])
    outputSandbox=["*.out","*.log","*.xml","*.sh","*.py","*.txt", "*.cli"]
    jobDestination=["LCG.DESY-HH.de", "LCG.CERN.ch"]
    # jobDestination=[]
    jobscript="dirac-ildapp-dstmerge.sh"
    
    # dstmjson = json.load(open(jobtop+'/'+dstmjson))
    # prodid = dstmjson['dstm']['meta']['ProdID'] 
   
    # topdir=os.getcwd()
    jobmeta = {"inputSandbox":inputSandbox, "outputSandbox":outputSandbox, 
               "jobScript":jobscript,
               "Destination":jobDestination, 
               "list_of_dstfiles":"dstlist.txt"}
    
    return jobmeta
  
###################################################################
# Start of main part
####################################################################
if __name__ == '__main__':
 
    prodid="undef"
    if len(sys.argv) > 1:
       prodid=sys.argv[1]

    jobmeta = set_params()


    subdir=os.getcwd()
    jobnkey=os.path.basename(subdir)
    jobname=".".join(reversed(os.path.basename(subdir).split('.')))

    jobmeta["jobgroup"] = "ILDDSTM-"+prodid
    jobmeta["jobname"] = jobname
    # pprint.pprint(jobmeta)

    submit_userdstm_job(jobmeta)
    
