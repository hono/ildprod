# ildprod : Collection of tools for ILD MC Production

## Production manual

- [Get started](docs/GetStarted.md)
- [Production Manual](docs/ProductionManual.md)
- [ChangeLog](CHANGELOG.md)

## Releated links

- [ELOG data base for ILD samples](https://ild.ngt.ndu.ac.jp/elog)
 - [Generator meta data](https://ild.ngt.ndu.ac.jp/elog/genmeta)
 - [Production samples by this tool, post DBD and ILD optimization](https://ild.ngt.ndu.ac.jp/elog/dbd-prod)
 - [Optimization samples, process ID based info. of DST-merged files](https://ild.ngt.ndu.ac.jp/elog/opt-data/)

- [A copy of generator meta data](https://ild/ngt.ndu.ac.jp/CDS/mc-dbd.log)
- [ILD Confluence, MC production WG](https://confluence.desy.de/display/ILD/Monte+Carlo+Production)


- LC Generator group convention defined for DBD study  
 - [Documents lcgentools server at CERN](https://svnweb.cern.ch/trac/lcgentools/browser/tags/v2r2/ILC/documents/generator-conventions.docx)  
 - [PDF file at KEK Confluence](https://wiki.kek.jp/display/~miyamoto/ILC+Software+Common+Task?preview=%2F6496081%2F12058650%2Fgenerator-conventions.pdf)  

