# Tools for automated MC production

`starter` is a collection of tools, which are waked up by cron 
and calls scripts in `worker` directory.  It is assumed that 
`starter` tools are executed at a client host like `ild.ngt.ndu` and 
`worker` tools are executed at a server with large resource like KEKCC.

## Set up of a starter host.

Following is an example to checkout only `starter` directory to a starter host.
```
% mkdir starter
% cd starter
% git init 
% git config core.sparsecheckout true 
% echo "auto/starter/" >> .git/info/sparse-checkout
% git remote add origin https://gitlab.cern.ch/amiyamot/ildprod.git
% git pull origin master
% git read-tree -m -u HEAD
```

For rebasing,
```
% git rebase origin/master master
```


