#!/bin/bash 
#
#
###HELP_BEGIN###
# A script to check errors in ILD simulation and reconstruction production.
# This script should be initiated by a cron script, start_exec_and_cp.sh, in the starter directory.
# It runs at ${MCPROD_TOP} directory and execute error_chk.sh. Resultant error log file is copied
# to the starter host for web display.
#
# Command syntax is
#     errormon.sh <init_script>
#
#     <init_script> is h, help, -h, or --help to get help
#
# See help of starter/start_exec_and_cp.sh for how to write the crontab file at the starter
# host.  <init_script> should exist at the worker host. It is sourced at the begining
# in order to initialize ILCDirac/ILDProd environments and cd to ${MCPROD_TOP}
# directory.
#
###HELP_END###


# Print help
if [ -z "$1" -o  "$1" == "help" -o "$1" == "h" -o "$1" == "-h" -o "$1" == "--help" ] ; then
  sed -e '/###HELP_END###/,$d' -e '1,/###HELP_BEGIN###/d' -e "s/^# / /g" -e "s/^#//" $0
  exit
fi


echo "Executing $0"
echo "init_script is $1"

cron_script=$0
init_script=$1
if [ -z ${init_script} ] ; then
  echo "ERROR: A script to initialize environment is not defined."
  exit 1
fi

curdir=$(cd $(dirname $0) ; pwd )
echo "Worker directory  is ${curdir}"
source ${init_script}

if [ -z "${MCPROD_TOP}" ] ; then
  echo "ERROR: MCPROD_TOP environment parameter is not defined."
  exit 1
fi
if [ ! -e ${MCPROD_TOP} ] ; then
  echo "ERROR: MCPROD_TOP, ${MCPROD_TOP}, does not exist."
  exit 1
fi



logdir=$( pwd )/logs
mkdir -p ${logdir}
dstr=$(date +%Y%m%d-%H%M%S)
logfile=${logdir}/errormon-${dstr}.log

echo "========= Current directory : `pwd` " | tee -a ${logfile}
echo "Hostname `hostname` at ${dstr} " | tee -a ${logfile}
echo "" | tee -a ${logfile}
echo "Execute `which error_chk.sh ` " | tee -a ${logfile}

error_chk.sh 2>&1 | tee -a ${logfile}

