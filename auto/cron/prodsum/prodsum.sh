#!/bin/bash 

if [ -z ${PRODSUM_CONF} ] ; then
  echo "ERROR: Environment parameter, PRODSUM_CONF, is undefined."
  exit
fi
if [ ! -e ${PRODSUM_CONF} ] ; then
  echo "ERROR: ${PRODSUM_CONF} does not exist."
  exit
fi

source ${PRODSUM_CONF}

dstr=`date +%Y%m%d-%H%M%S`
logdir="${workdir}/logs"
logfile=${logdir}/prodsum-${dstr}.log

[ ! -e ${workdir} ] && mkdir -p ${workdir}
[ ! -e ${logdir} ] && mkdir ${logdir}

echo "### ${dstr} : prodsum.py has started" | tee -a ${logfile}

which python 2>&1 | tee -a ${logfile}

echo "###########" | tee -a ${logfile}
echo "prodsum is at ${WEB_ADDRESS}/prodsum-mc2020.html" | tee -a ${logfile}
echo "###########" | tee -a ${logfile}

rm -fv ${workdir}/elog_dbdprod.json  
rm -fv ${workdir}/elog_genmeta.json 

cd ${workdir}
(
    ${ILDPROD}/auto/cron/prodsum/prodsum.py 2>&1 | tee -a ${logfile} 
    
    ndiff=`diff prodsum-mc2020.html ${WEB_LOCALPATH}/prodsum-mc2020.html | grep -e "<" -e ">" | grep -v "( created on 20" | wc -l`
    echo "ndiff = $ndiff"
    
    if [ "x${ndiff}" != "x0" ] ; then 
      /usr/bin/cp -v prodsum-mc2020.html ${WEB_LOCALPATH}/ 2>&1 | tee -a ${logfile}
      chmod 664 ${WEB_LOCALPATH}/prodsum-mc2020.html
      dstr=`date +%Y%m%d-%H%M%S`
      /usr/bin/cp -v prodsum-mc2020.html ${logdir}/prodsum-mc-2020-${dstr}.html 2>&1 | tee -a ${logfile}
    fi
    
    dstr=`date +%Y%m%d-%H%M%S`
    echo "### ${dstr} : prodsum.py completed" | tee -a ${logfile}
    
    
    if [ "x${ndiff}" != "x0" ] ; then 
      # Send message to MAIL_RECIPIENTS
      if [ "x${MAIL_RECIPIENTS}" != "x" ] ; then 
        dstr=`date +%Y%m%d-%H%M%S`
        mailtext=${logdir}/prodmon-mail-${dstr}.txt
        echo "To:${MAIL_RECIPIENTS}" > ${mailtext}
        echo "Subject:prodsum-mc2020 updated: `date  +%Y%m%d-%H%M%S`" >> ${mailtext}
        echo "Mime-Version: 1.0" >> ${mailtext}
        echo "Content-Type: text" >> ${mailtext}
        cat ${logfile} >> ${mailtext}
        cat ${mailtext} | /usr/lib/sendmail -t
      fi
    fi
)
