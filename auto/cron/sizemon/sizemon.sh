#!/bin/bash
#
#  Create plots of storage space usage
#

if [ -z ${SIZEMON_CONF} ] ; then 
  echo "ERROR: Environment parameter, SIZEMON_CONF, is undefined."
  exit
fi
if [ ! -e ${SIZEMON_CONF} ] ; then 
  echo "ERROR: ${SIZEMON_CONF} does not exist." 
  exit
fi

source ${SIZEMON_CONF}

debug=$1
[ ! -e ${WORKDIR} ] && mkdir -p ${WORKDIR}
cd ${WORKDIR}

mcprod_out=${WORKDIR}/mcprod_out.html 

mkdir -p ${SIZEMON_DATA}
# ( ${ILDPROD}/auto/cron/sizemon/sizemon.sh ${debug} > ${workdir}/mcprod_out.html )

##########################################################################
export LANG=C
echo "<h1>mcprod monitor</h1>" > ${mcprod_out}
echo "<pre>" >> ${mcprod_out}
echo "### mcprod.sh is executing at `pwd` on `date` " >> ${mcprod_out}
initlog=${WORKDIR}/proxy-init.log

(
  ${INIT_ILCDIRAC_USER} > ${initlog} 2>&1 
  if [ $? -ne 0 ] ; then 
     echo "### Error : Failed to initialize proxy"
      chmod go-r ${initlog}
      exit -1
  fi
  chmod go-r ${initlog}

  pwd >> ${mcprod_out}
  outlog="${SIZEMON_DATA}/getsize-`date +%Y%m%d-%H%M%S`.log"
  if [ "x${debug}" == "x" ] ; then
    cat ${ILDPROD}/auto/cron/sizemon/getsize.cli | dirac-dms-filecatalog-cli > ${outlog}
    chmod 664 ${outlog}
  fi

  ( cd ${WORKDIR} && ${ILDPROD}/auto/cron/sizemon/sizelog2json.py )
  [ ! -e ${WORKDIR}/plots ] && mkdir -p ${WORKDIR}/plots 
  ( cd ${WORKDIR}/plots && ${ILDPROD}/auto/cron/sizemon/plotsize.py )

  pwd >> ${mcprod_out}
  ${GET_KEKCC_ILCQUOTA} 2>&1 | tee ilcquota.log
  echo "=== gfal-attr results ===" | tee -a ilcquota.log 
  ( mkdir -p ${WORKDIR}/spacetoken 
    cd ${WORKDIR}/spacetoken 
    ${ILDPROD}/auto/cron/sizemon/getusage.sh
   ) | tee -a ilcquota.log 
  echo "" | tee -a ilcquota.log 
  echo "(last update : `date +%Y%m%d-%H%M%S`)" | tee -a ilcquota.log

  [ ! -e ${WORKDIR}/plots2 ] && mkdir -p ${WORKDIR}/plots2
  ( source ${INIT_ILCSOFT}
    cd ${WORKDIR}/plots2 
    /usr/bin/python ${ILDPROD}/auto/cron/sizemon/mk_storage.py > mk_storage.log 2>&1 
  )

) >> ${mcprod_out}

#
# Copy result to WEB and send notice mail
#
(
  umasknow=`umask -p`
  umask 0002
  cp -p ${WORKDIR}/plots/mcprod.html ${WORKDIR}/plots2/size.png ${WORKDIR}/plots2/replicas.png ${WEB_LOCALPATH}
  [ -e ${WEB_LOCALPATH}/previous ] || mkdir ${WEB_LOCALPATH}/previous
  dstr=`date +%Y%m%d`
  cp -p ${WEB_LOCALPATH}/mcprod.html ${WEB_LOCALPATH}/previous/mcprod-${dstr}.html
  cp -p ${WORKDIR}/ilcquota.log ${WEB_LOCALPATH}
  ${umasknow}
)

( 
  echo "</pre>" 
  echo "Full plot at <a href=\"${WEB_ADDRESS}/mcprod.html\">${WEB_ADDRESS}</a><br>"
  echo "Figures at <a href=\"${WEB_ADDRESS}/size.png\">size</a> and <a href=\"${WEB_ADDRESS}/replicas.png\">replicas</a>" 
  cat ${WORKDIR}/plots/plotsize.html
) >> ${mcprod_out}

 

#################################################################
# Send mail to MAIL_RECIPIENTS
#################################################################

if [ "x${MAIL_RECIPIENTS}" != "x" ] ; then 
  mailtext=${WORKDIR}/mail.txt
  echo "To:${MAIL_RECIPIENTS}" > ${mailtext}
  echo "Subject:Server Monitor:${smontxt:0:10}: `date +%Y%m%d-%H%M%S`" >> ${mailtext}
  echo "Mime-Version: 1.0" >> ${mailtext}
  echo "Content-Type: text/html" >> ${mailtext}
  cat ${ILDPROD}/auto/cron/sizemon/header.html >> ${mailtext}

  cat ${mcprod_out} >> ${mailtext}

  cat ${mailtext} | /usr/lib/sendmail -t
fi


