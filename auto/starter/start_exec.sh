#!/bin/bash
#
###HELP_BEGIN###
# A script to run at a starter host
#
# start_exec is a script to start worker script at worker host and 
# copy the log log to a httpd directory at starter host.
# Command syntax is 
#    start_exec.sh [worker_script]
#
#    worker_script=h, help, -h or --help to get this help
# 
# An example description of this script in the crontab would be  
# 0-59/15 * * * * ( source <init_starter> && ${HOME}/cron/start_exec.sh <worker_script> )
#
# <init_starter> a file to define following environment parameters.
#    LOGIN_CMD : A command to login to a worker host.
#    WORKER_DIR: A worker script directory 
#    INIT_WORKER : Initialize environment parameters for worker script
#    PRODMON_DIR : A httpd directory at starter host to write log file.
#
# Optional variables.
#    PRODMON_FILE : A log file name written in ${PRODMON_DIR}
#                   default=cron-${worker_name}.log
#    CURDIR : A directory to run this script. 
#                   default=$(dirname $0), namely a directory of this script
#    LOGDIR : Cron log directory.
#                   default=a directory to save log files.
#    MAX_LOGFILE : If number of files in ${LOGDIR} exceeds this, tar gz file is created.
#                   default=2000
#
###HELP_END###

# Print help
if [ -z "$1" -o  "$1" == "help" -o "$1" == "h" -o "$1" == "-h" -o "$1" == "--help" ] ; then 
  sed -e '/###HELP_END/,$d' -e '1,/###HELP_BEGIN###/d' -e "s/^# / /g" -e "s/^#//" $0
  exit
fi


if [ -z "${LOGIN_CMD}" -o -z "${WORKER_DIR}" -o -z "${INIT_WORKER}" -o -z "${PRODMON_DIR}" ] ; then 
  echo "Some or all init_starter variables are not defined."
  exit 1
fi

dstr=$(date +%Y%m%d-%H%M%S)
thishome=${HOME}

worker_script=$1
worker_name=$(basename $1 .sh)
prodmon_file=${PRODMON_FILE:-cronlog_${LOGNAME}.log}
curdir=${CURDIR:-$(cd $(dirname $0) ; pwd)}
logdir=${LOGDIR:-logs}
max_logfile=${MAX_LOGFILE:-2000}
cronexe_list=${CRONEXE_LIST:-cronexe.list}

cd ${curdir}
mkdir -p ${logdir}
logfile="${logdir}/start-${worker_name}-${dstr}.log"
templog="cron-${worker_name}-`basename ${cronexe_list} .list`.log"

# #######################################################################
# execute a worker script
# #######################################################################
${LOGIN_CMD} ${WORKER_DIR}/${worker_script} ${INIT_WORKER} ${cronexe_list} > ${logfile} 2>&1 

echo "### start_exec.sh ${worker_name} DONE" >> ${logfile} 2>&1

# cp ${logfile} ${templog}
echo "" > ${templog}
echo "### ${dstr} ### " >> ${templog}

# Strip off ILCDirac initialization message and prepare a httpd log file.
if [ x`grep -c "### End of initialization" ${logfile}` == "x0" ] ; then
  cat ${logfile} >> ${templog}
else
  sed -e "0,/### End of initialization/D" ${logfile} >> ${templog}
fi
if [ ! -z ${PRODMON_DIR} ] ; then
  cp ${templog} ${PRODMON_DIR}/${prodmon_file} 2>&1 | /usr/bin/tee -a ${logfile} 
fi
ln -sf ${logfile} latest-${worker_name}.log

# Create a tar.gz file, if more than 2000 

nfiles=$(/bin/ls ${logdir} | wc -l)
if [ ${nfiles} -gt ${max_logfile} ] ; then
  (
    dstr=$(date +%Y%m%d)
    mv ${logdir} ${logdir}-${dstr}
    tar zcf ${logdir}-${dstr}.tar.gz ${logdir}-${dstr}
    mkdir -p ${logdir}
  ) 2>&1 | /usr/bin/tee -a ${templog} 2>&1
  echo "Log files are tar-gzed in ${logdir}-${dstr}.tar.gz" >> ${templog} 2>&1
fi

# Create html version of log file.
echo "Converting log file to html" >> ${templog} 2>&1 
python log2html.py >> ${templog} 2>&1 


