#!/bin/bash
#
###HELP_BEGIN###  
# A script to start script at worker host, then copy resultant file to
# the stater host.
#
# An example description of this script in the crontab is 
# 0-59/15 * * * * ( source <init_starter> && ${HOME}/cron/start_exec_and_cp.sh errormon.sh )
#
# Command syntax
#     start_exec_and_cp.sh [worker_script]"
#
#     worker_script = "h", "help", "-h", or "--help" to get this help
#
# <init_starter> a file to define following environment parameters.
#     LOGIN_CMD : A command to login to a worker host.
#     INIT_WORKER : Initialize environment parameters for worker script
#     WORKER_DIR  : A worker script at worker host 
#     PRODMON_DIR : A httpd directory at starter host to write log file.
#     SCP_CMD : A command for scp from a worker host.
# 
# Optional variables.
#     CURDIR : A directory to run this script. 
#              default=$(dirname $0), namely a directory of this script
#     LOGDIR : Cron log directory.
#              default=a directory to save log files.
#     MAX_LOGFILE : If number of files in ${LOGDIR} exceeds this, tar gz file is created.
#              default=2000
#
###HELP_END###

# Print help
if [ -z "$1" -o  "$1" == "help" -o "$1" == "h" -o "$1" == "-h" -o "$1" == "--help" ] ; then
  sed -e '/###HELP_END/,$d' -e '1,/###HELP_BEGIN###/d' -e "s/^# / /g" -e "s/^#//" $0
  exit
fi

if [ -z "${LOGIN_CMD}" -o -z "${WORKER_DIR}" -o -z "${INIT_WORKER}" -o -z "${PRODMON_DIR}" -o -z "${SCP_CMD}" ] ; then 
  echo "Some or all init_starter variables are not defined."
  exit 1
fi


dstr=$(date +%Y%m%d-%H%M%S)
thishome=${HOME}

worker_script=$1
worker_name=$(basename $1 .sh)
curdir=${CURDIR:-$(cd $(dirname $0) ; pwd)}
logdir=${LOGDIR:-${worker_name}-logs}

cd ${curdir}
mkdir -p ${logdir}
logfile="${logdir}/${worker_name}-${dstr}.log"

source ${LOGIN_CMD} ${WORKER_DIR}/${worker_script} ${INIT_WORKER} > ${logfile} 2>&1

tststr="### FILE_to_DOWNLOAD is "
dlfile=`grep ${tststr} ${logfile} | head -1 | sed -e "s|${tstr}||g" `
if [ ! -z ${dlfile} ] ; then 
  monpref=$(basename ${dlfile} .log)
  monfile=${monpref}_${LOGNAME}.log
  source ${SCP_CMD} ${dlfile} ${monfile} >> ${logfile} 2>&1 
  cp -v ${monfile} ${PRODMON_DIR} 2>&1 >> ${logfile}
fi

#################################

