# Documentation of ILD MC Production

## [GetStarted](GetStarted.md)
For install and setup up environments for ILDProduction

## [ProductionManual](ProductionManual.md)
Step-by-Step description of production work.

## [Useful ILCDIRAC commands](useful_ILDDIRAC_commands.md)

## [Configuration for useful tools](UsefulTools.md)

# Old documentation

## [Calibration files in DBD era](calibration.md)

## [Old memo of generator samples](GeneratorSamples.md)

## [Links to webs for old sim/rec files in DBD and SnowMass2013 era](SimAndRecSamples.md)


