# To get started

## Requirements

- GRID and ILCDirac certificates
- ILCDIRAC and python tools for production, such as elog and openpyxl

[`utils/scripts/my-install-cmd.sh`](https://gitlab.cern.ch/amiyamot/ildprod/blob/master/utils/scripts/my-install-cmd.sh)
would help installing ILCDIRAC and python tools.  For example, do as follows.
```
% wget https://gitlab.cern.ch/amiyamot/ildprod/raw/master/utils/scripts/my-install-cmd.sh
% chmod +x my-install-cmd.sh
% ./my-install-cmd.sh 2>&1 | tee my-install.log
```

## ILDProd environment

### Install the latest ILDProd as follows.  Note that a version used in MC production 
for ILD optimization in May-Aug, 2018 is v02-00-01-p1.
```
% git clone https://gitlab.cern.ch/amiyamot/ildprod.git --single-branch -b master ILDProd
% cd ILDProd
```

### Environment parameters to do ILD Production. 

Before starting the ILD production session, environment parameters,
*ILDPROD, ILCSOFT_VERSION, PRODDIR, PRODKEY_FILE*,
should be set. Then source `bin/setup.sh` to set other parameters.
An example is shown below.
```
# ILDPROD is a directory where ildprod is installed.
% export ILDPROD=~/ILDProd          

# ILCSOFT_INIT_SCRIPT is a script to initialize ILCSoft
% export ILCSOFT_INIT_SCRIPT=/cvmfs/ilc.desy.de/sw/x86_64_gcc49_sl6/v02-00-02/init_ilcsoft.sh

# PRODDIR is a Sub-directory in mcprod, to do production task.
% export PRODDIR=v02-00-02
       
# A file to set keys for the production task. Ask production manager for details.
% export PRODKEY_FILE= ...          

# Source bin/setup.sh to initialize other environment
% source ${ILDPROD}/bin/setup.sh 

# cd to PRODDIR directory and create other directories for actual production work
% cd ${ILDPROD}/mcprod/${PRODDIR} 
% mkdir -p ProdTask # If ProdTask is not created yet.

```

For a test production, following setting will be useful.
```
# PRODDIR is a Sub-directory in mcprod, to do test production task.
% export PRODDIR=testprod

# If define, use ELOG_SUBDIR, instead of default dbd-prod
export ELOG_SUBDIR="test-prod"
echo "###NOTICE####  ELOG_SUBDIR=${ELOG_SUBDIR} is used."
```

**IMPORTANT** 
Make sure that parameters defined in `${ILDPROD}/mcprod/${PRODDIR}/mcprod_config.py`
is consistent with what you want to produce.

### Do production
See [Production Manual](ProductionManual.md) for details of production task.

See [Cron Configuration](CronConfiguration.md) for configurations to use cron tools,
which are necessary to monitor production tasks and execute scripts for dst merge jobs,
log upload, etc.
