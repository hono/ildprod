#!/usr/bin/env python 
# Modify a elog record in genmeta logbook
#
# Warning 
# #Events and xsect-error fields are not filled. Not know why.
#
# 8-March-2018 A.Miyamoto
#

import os
import elog
import argparse
import pycolor
from urllib.request import urlopen
import ssl
import pprint
import json
import copy

PASSINFO = "MY_ELOG_KEY"
LOGBOOK = "https://ild.ngt.ndu.ac.jp/elog/genmeta/"
# LOGBOOK = "https://ndubel01.ngt.ndu.ac.jp/elog/genmeta/"
WORKER = "A.Miyamoto"
pyc = pycolor.pycolor()
G2EMAP_URL = "https://ild.ngt.ndu.ac.jp/mc-prod/files2017/genmeta/genmeta-to-elog.map"


###################################################################
def getG2EMAP():
    ''' Get a map to convert generator meta data to elog attributes field '''
    # if hasattr(ssl, '_create_unverified_context'):
    #        ssl._create_default_https_context = ssl._create_unverified_context

    g2econv = {}
    res = urlopen(G2EMAP_URL).readlines()
    for liner in res:
        line = liner.decode("utf-8")
        if line[0:1] != "#":
            (gen, elog) = line.replace("\n","").split('=')
            g2econv[gen] = elog

    return g2econv

###################################################################
def getE2GMAP(g2emap):
    e2gmap = {}
    for k, v in g2emap.items():
        e2gmap[v] = k
    return e2gmap

G2EMAP = getG2EMAP()

E2GMAP = getE2GMAP(G2EMAP)
# pprint.pprint(G2EMAP)

###################################################################
def getPIDfromURL(url):
    pid="blank"
    for tag in url.split("<"):
        # print tag
        if tag[0:4] == "span":
           pid = tag.split(">",1)[1]
        
    return pid    

###################################################################
def getAttributes(metafile):
    ''' Get ELOG attributes from generator meta file '''
    # pprint.pprint(G2EMAP)
    
    if not os.path.exists(metafile):
        print( pyc.c["red"] + "ERROR : " + pyc.c["end"] +
               "meta file, %s does not exist." % metafile )
        exit(-1)

    attrib = {}
    genmeta = {}
    nline = 0
    for inline in open(metafile):
        line = inline.replace("\n","").replace("\r","")
        nline += 1
        if len(line.replace(" ","").replace("\n","")) == 0 :
           print( "Empty line found in line#" + str(nline) )
           continue
        print( "len="+str(len(line.replace(" ","").replace("\n",""))) )
        print( "  Line " + str(line.replace(" ","").replace("\n","")) + " #line="+str(nline) )
        if "=" not in line:
           print( "No = in " + line )
        ( key, value ) = line.replace("\n","").split("=",1)
        if value != "":
            genmeta[key] = value

    for key, value in genmeta.items():
        if key not in G2EMAP:
            print( pyc.c["green"] + "WARNING : " + pyc.c["end"] +
                   "generator meta key, %s, is not defined for elog. Probably wrong generator meta." % key )
        else:
            attrib[G2EMAP[key]] = value     
   
    if ";" in genmeta["file_names"]:  
        filenames = genmeta["file_names"].split(";")
        (fprefix, nser, ftype) = filenames[0].rsplit(".",2)
        lser = len(nser)
        if len(filenames) != int(genmeta["number_of_files"]):
            print( pyc.c["red"] + "ERROR : " + pyc.c["end"] +
                   "number_of_files ("+genmeta["number_of_files"]+") is not consistent" +
                   " with files defined in file_names field." )
            print( genmeta["file_names"] )
            exit(-1)

        attrib["file_names"] = "%s.[1-%d].stdhep" % ( fprefix, int(genmeta["number_of_files"]) ) 

    attrib["Polarization"] = "e"+genmeta["polarization1"]+"."+ \
                             "p"+genmeta["polarization2"]
    attrib["Polarization"] = attrib["Polarization"].replace(" ","")
    attrib["NbEvents"] = int(genmeta["total_number_of_events"])
    attrib["xsect_error"] = float(genmeta["cross_section_error_in_fb"])
    attrib["xsect"] = float(genmeta["cross_section_in_fb"])
    attrib["metaname"] = metafile.rsplit('.',1)[0]
    attrib["metapath"] = metafile
    attrib["Energy"] = int(genmeta["CM_energy_in_GeV"])
    datestr=genmeta["job_date_time"].split('-')[0]
    if len(datestr) == 8:
        attrib["JobDate"] = datestr[0:4] + "-"+datestr[4:6] + "-" + datestr[6:8]
    elif len(datestr) == 6:
        attrib["JobDate"] = "20"+datestr[0:2] + "-"+datestr[2:4] + "-" + datestr[4:6]
    else:
        print( pyc.c["light cyan"] + "WARNING : " + pyc.c["end"] + 
               "Date part of job_date_time value is neither 6 nor 8 characters. It was " + datestr )
        print( "JobDate field of elog will be empty" )

    attrib["status"] = "OK"
    procid = genmeta["process_id"]
    attrib["process_id"] = "<a href=\"http://www-jlc.kek.jp/~miyamoto/CDS/mc-dbd.log/generated/metainfo-id/" + \
	                   "I%s.txt\"><span style=\"color:green\">%s</span></a>" % (procid, procid)
    attrib["logurl"] = "<a href=\"%s\"><span style=\"color:green\">%s</span></a>" % ( genmeta["logurl"], genmeta["logurl"] )

    return {"OK":True, "Attribute":attrib}




###################################################################
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Add a new entry to elog genmeta from generator meta file.")
    parser.add_argument("elogid", help="elog ID to be modifed")
    parser.add_argument("modkeys", help=", separated list of elog key name to be updated")
    parser.add_argument("--json",help="genmetaByID file",dest="genmeta_json",action="store",default="/group/ilc/users/miyamoto/optprod/json/mc-dbd.log.genmeta/genmetaByID.json")
    parser.add_argument("-n", help="Dry run, do not write to elog", dest="isDry", action="store_true", default=False)
    parser.add_argument("-s", help="Silent mode. Do not dump elog data before upload to ELOG", dest="silent", action="store_true", default=False)

    args = parser.parse_args()
    elogid = int(args.elogid)
    genmeta_data = json.load(open(args.genmeta_json))
    modkeys = args.modkeys.split(',')

    isdry = args.isDry
    silent = args.silent

    print( "Updating elogid :"+str(elogid) )
    lines = open(os.environ[PASSINFO]).readlines()
    (auser, apass) = lines[0].replace("\n","").split(' ')
    logbook = elog.open(LOGBOOK, user=auser, password=apass)
    
    ( message, attributes, attachements ) = logbook.read(elogid)

    print("===Message == ID=" + str(elogid))
    pid = getPIDfromURL(attributes["process_id"])
    if pid not in genmeta_data:
        print( "ProcessID="+pid+" not found in "+args.genmeta_json )
        exit(-1)
    genmeta = genmeta_data[pid]

    # print(message)
    # print("-- attrebutes ------")
    # pprint.pprint(attributes)
    # print("-- attachment ------")
    # print(attachements)

    newatt = {}
    for k in modkeys:
       if k == "file_names":
          newatt[k] = genmeta[E2GMAP[k]]
          (metaname, nser, ftype ) = newatt[k].rsplit(".",2)
          newatt["metaname"] = metaname
          newatt["metapath"] = metaname.replace(".txt","")
       else:
          newatt[k] = genmeta[E2GMAP[k]]

    for k, v in newatt.items():
       if attributes[k] != newatt[k]:
          print( k+":"+attributes[k] + "  <== " + newatt[k] )

    msgtext = message
    newid = logbook.post(msgtext, msg_id=elogid, attributes = newatt, encoding="HTML")

    print( "elogID " + str(elogid) + " was modified" )
   
    exit(0)

