#!/usr/bin/env python 
# Add a record to elog genmeta
#
# Warning 
# #Events and xsect-error fields are not filled. Not know why.
#
# 8-March-2018 A.Miyamoto
#

from GenmetaUtils import *

import os
import elog
import argparse
import pycolor
from urllib.request import urlopen
import ssl
import pprint

PASSINFO = "MY_ELOG_KEY"
LOGBOOK = "https://ild.ngt.ndu.ac.jp/elog/genmeta/"
# LOGBOOK = "https://ndubel01.ngt.ndu.ac.jp/elog/genmeta/"
WORKER = "A.Miyamoto"
pyc = pycolor.pycolor()
G2EMAP_URL = "https://ild.ngt.ndu.ac.jp/mc-prod/files2017/genmeta/genmeta-to-elog.map"


#####################################################################################3

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Add a new entry to elog genmeta from generator meta file.")
    parser.add_argument("meta", help="generator meta file.")
    parser.add_argument("-n", help="Dry run, do not write to elog", dest="isDry", action="store_true", default=False)
    parser.add_argument("-s", help="Silent mode. Do not dump elog data before upload to ELOG", dest="silent", action="store_true", default=False)

    args = parser.parse_args()   
    metafile = args.meta
    isdry = args.isDry
    silent = args.silent

    ret = getAttributes(metafile)
    if not ret["OK"]:
        print( pyc.c["red"] + "Failed" + pyc.c["end"] + " to readin metafile." )
        exit(-1)

    attrib = ret["Attribute"]
    
    if not silent:
       pprint.pprint(attrib)

    if isdry:
        exit(0)
    
    lines = open(os.environ[PASSINFO]).readlines()
    (auser, apass) = lines[0].replace("\n","").split(' ')
    
    logbook = elog.open(LOGBOOK, user=auser, password=apass)
    msgtext=" " 

    if "undefkeys" in ret and len(ret["undefkeys"]) > 0:
       msgtext = "\n".join(ret["undefkeys"])

    newid = logbook.post(msgtext, attributes = attrib, encoding="HTML")

    print( metafile + " was added to " + pyc.c["cyan"] + " elog with ID=" + str(newid) + pyc.c["end"] )
   
    exit(0)

