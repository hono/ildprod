#!/bin/bash 
#
#  Count number of running jobs and return with -1 if exceeds threshold
# 


test_readiness()
{ 
  
  procname=$1
  nthresh=$2

  logname=`logname`
  OK=1
  ntest=`ps -ef 2>&1 | grep ${logname} | grep ${procname} `
  nfound=`ps -ef 2>&1 | grep ${logname} | grep ${procname} | wc -l` 

  
  if [ ${nfound} -gt ${nthresh} ] ; then 
    echo "### runtest_by_load.sh found too many processes at `hostname`: ${nfound} ${procname}"
    return 10
  fi
  return 0
}

( test_readiness ILD_DSTM_Job_Maker.py 2 ) || exit 21
( test_readiness dirac-dms-add-file 20 ) ||  exit 22
( test_readiness prodlog_cleanup.py 2 ) || exit 23
( test_readiness gensplit-upload-addmeta.sh 1 ) || exit 21
( test_readiness setMetaGenSplit 3 ) || exit 24
( test_readiness task_step3.sh 2 ) || exit 24
# ( test_readiness task_step6.sh 2 ) || exit 24
# ( test_readiness task_step7.sh 2 ) || exit 24
( test_readiness startprod.sh 1 ) || exit 24
( test_readiness dstm-step1.sh 1 ) || exit 24
( test_readiness dstm-step2.sh 1 ) || exit 24

exit 0
