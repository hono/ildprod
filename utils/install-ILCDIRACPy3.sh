#!/bin/bash 
# A script to install ILCDIRACPy3, based on the information at 
#  https://ilcdirac-doc.web.cern.ch/DOC/Files/UserGuide/ilcdiracclient.html#client-python-version
#  2022-02-18
#
# For Clean install of ILCDIRAC in CentOS7. 
# 
# The default ILCDIRAC is installed following the instruction described at 
# https://ilcdirac-doc.web.cern.ch/DOC/Files/UserGuide/ilcdiracclient.html#id5
# GRID certificates, usercert.pem and userkey.pem are not updated.
# grid-security/vomses, etc are obtained using your grid certificate. 
# For this, password input is promped when "dirac-proxy-init -x -N" is issued.
#
# Then ILCDIRAC is removed, then git clone-ed from gitlab.cern.
# Then installation of additional python packages, such as openpyxl, elog, etc follow.
#
# Usage:
#   mkdir <install_dir>
#   cd <install_dir>
#   cp <ILDProd directory>/utils/install-ILCDIRACPy3.sh .
#   ./install-ILCDIRACPy3.sh 2>&1 | tee my-install-cmd.log
#      
#   input GRID password for dirac-proxy-init
#

curl -LO https://github.com/DIRACGrid/DIRACOS2/releases/latest/download/DIRACOS-Linux-$(uname -m).sh
bash DIRACOS-Linux-$(uname -m).sh
rm -f DIRACOS-Linux-$(uname -m).sh
source diracos/diracosrc
pip install ILCDIRAC

# ILCDirac configure. Password is promped
dirac-proxy-init -x -N
dirac-configure -S ILC-Production -C dips://voilcdiracconfig.cern.ch:9135/Configuration/Server --SkipCAChecks

# Install ILCDIRAC by git clone

# rm -rf ILCDIRAC
# git clone  https://gitlab.cern.ch/CLICdp/iLCDirac/ILCDIRAC.git ILCDIRAC
# . bashrc
# dirac-deploy-scripts
# unset REQUESTS_CA_BUNDLE


# Add developper's packages.
# cd ~/ILDDirac
#pip install --upgrade pip
#pip uninstall -y distribute
#pip install --upgrade setuptools
#pip install --upgrade setuptools_scm
#pip install --upgrade pylint mock MySQL-python pytest-cov pytest-randomly flake8 psutil \
#            flake8-docstrings flake8-commas

# Install tools for ILD
pip install openpyxl
pip install xlrd
pip install lxml
pip install braceexpand
pip install xlsxwriter

pip install passlib
pip install requests

(
  echo "Installing python_elog"
  mkdir -p install_temp-elog-$$
  pushd install_temp-elog-$$
    wget https://github.com/paulscherrerinstitute/py_elog/archive/refs/tags/1.3.11.tar.gz
    tar zxf 1.3.11.tar.gz
    pushd py_elog-1.3.11
      python setup.py install
      ret=$?
    popd
   popd
   [ "x${ret}" == "x0" ] && rm -rf install_temp-elog-$$
)

