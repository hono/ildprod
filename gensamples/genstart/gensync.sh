#!/bin/bash

#

###########################################
print_help()
{
  echo "syncdesy.sh"
  echo "<Function>"
  echo "  Rsync generator meta file at desy. If new files found, check format and if good, create genmeta json files"
  echo "<Command format>"
  echo "  ./syncdesy.sh [option]"
  echo "  Option"
  echo "    -n : Proceed without checking the genmeta format."
  echo "    -h : print his help"
  echo "    --no_rsync : Proceed without RSYNC with DESY."
  echo "    --no_check : Proceed without checking the genmeta format."
  echo "    --no_web   : Proceed without updating web contents."
  echo "    --no_json  : Do not update genmeta json file."
  echo "    --no_mail  : Do not send mail to AM."
  echo "    --debug    : Print debug message."
  exit
}

# #########################################
# Start main
# #########################################
export LANG=en_US.UTF-8

dorsync="YES"
docheck="YES"
doweb="YES"
dojson="YES"
domail="YES"
dodebug="NO"

# dorsync="NO"
# docheck="YES"
# doweb="YES"
# dojson="NO"
# domail="NO"

while [ $# -ne 0 ]
do
  case "$1" in
    -n) docheck="NO" ;;
    -h) print_help ;;
    --no_rsync) dorsync="NO" ;;
    --no_check) docheck="NO" ;;
    --no_web) doweb="No" ;; 
    --no_json) dojson="No" ;;
    --no_mail) domail="No" ;;
    --debug) dodebug="YES" ;;
    *) print_help
  esac
  shift
done

# #########################################



source ${HOME}/cron/genstart.conf

curdir=${WORKDIR}
logdir=${curdir}/synclogs
[ ! -e ${logdir} ] && mkdir -p ${logdir}

cd ${WORKDIR}

dstr=`date +%Y%m%d-%H%M%S`

GENMETA_CHECK=${ILDPROD}/gensamples/genmeta-check.py

cd ${curdir}
mkdir -p ${logdir}
logfile="${logdir}/gensync-${dstr}.log"


unset SSH_AGENT_PID
unset SSH_AUTH_SOCK
export RSYNC_RSH="${rsync_cmd}"
opt=" "


echklog=${WEB_BASEPATH}/CDS/files/genmeta-check/genmeta-check-${dstr}.log

#
# dest and src pair should be kept.

# srcbase="login.cc.kek.jp:/group/ilc/grid/storm/prod/ilc"
# destbase="/home/miyamoto/CDS/desy-pnfs"

tdirs=( "mc-dbd.log/generated" 
        "mc-opt.dsk/generated"
        "mc-2020/generatorlog" )

exclude_list=${ILDPROD}/gensamples/genstart/gensync.exclude
opt="${opt} --exclude-from=${exclude_list}"


## Rsync with KEK
if [ "${dorsync}" == "YES" ] ; then 
   for tdir in ${tdirs[@]}; do
       # echo ${tdir}
       ( cd ${destbase}/${tdir}
         rsync -auvz ${opt} ${srcbase}/${tdir}/ . >> ${logfile} 2>&1 
        )
   done

   loglen=`grep -v "receiving incremental" ${logfile} | grep -v " bytes  received " | grep -v "total size is "  |  grep -v '^\s*$' | wc -w `
   if [ "x${loglen}" == "x0"  ] ; then 
      # rm -f ${logfile}
       exit 0     
   fi

fi

# Initialize ILCSoft environment.

${INIT_ILCSOFT}

# Report trivial errors in meta file.
if [ "${docheck}" == "YES" ] ; then 
  ( 
    [ "${dorsync}" != "YES" ] && logfile=${logdir}/gensync-test.log
    for tdir0 in ${tdirs[@]}; do
      ( 
         tdir=${destbase}/${tdir0}
         echo ${tdir}
         echo ${logfile}
         grep "/E" ${logfile} | grep "\.txt" | grep -v " -> " | grep -v "###" | while read f ; do 
           echo "=== ${tdir}/${f} ==="
           if [ -e ${tdir}/${f} ] ; then 
             (
               echo "### genmeta-check ${tdir}/$f" 
               ${GENMETA_CHECK} ${tdir}/$f
             )
           fi
         done
      )
     done > ${echklog}
     [ "${dodebug}" == "YES" ] && cat ${echklog} 
  )

  numerror=`grep -e WARNING -e FATAL -e ERROR ${echklog} | wc -l`
  if [ "x${numerror}" != "x0" ] ; then
     echo "### genmeta-check detected following errors. " >> ${logfile}
     grep -e "### genmeta-check" -e WARNING -e FATAL -e ERROR ${echklog} \
         | sed -e "s/\[1;31m//g" -e "s/\[35m//g" -e "s/\[0m//g" >> ${logfile}
     echo ">>> see full error log at ${WEB_ADDRESS}/CDS/files/genmeta-check/genmeta-check-${dstr}.log" >> ${logfile}
  else
     echo "### No error found by genmeta-check." >> ${logfile}
     echo ">>> see full log at ${WEB_ADDRESS}/CDS/files/genmeta-check/genmeta-check-${dstr}.log" >> ${logfile}
  fi
fi

# ##########################################################################
# Recreate index file of web
# ##########################################################################
#

if [ "${doweb}" == "YES" ] ; then 
  # UPdate web links
  umasknow=`umask -p`
  umask 0002
  echo "### finding web files newer than index.html" >> ${logfile}
  CDSDIR=${WEB_BASEPATH}/CDS
  reffile="${CDSDIR}/mc-dbd.log/generated/metainfo-id/index.html"

  tdir=${CDSDIR}/desy-pnfs
  numnew=`/bin/find ${tdir}/ -type f -newer ${reffile} -print | grep -v index.html 2>/dev/null | wc -l`
  echo "Found ${numnew} files in ${tdir}, modified later than ${reffile}."
  if [ "x${numnew}" != "x0" ] ; then
      (
        cd ${CDSDIR}/mc-dbd.log/generated
        echo "Updating index file of ${tdir}/generated" >> ${logfile}
        ( cd metainfo-id && ./makelink-all.sh ) >> ${logfile} 2>&1
        ./make_all_html.sh > all-metainfo-sort-by-id.html 2>> ${logfile}
        echo "### Updating index.html files" >> ${logfile} 2>&1 
        ../make_index | grep -v "No news in " >> ${logfile} 2>&1
      )
  fi
  ${umasknow}
fi

###########################################################
# Step 4.
# Create josn files of meta files.
#
###########################################################
if [ "${dojson}" == "YES" ] ; then 
# Create genmeta json file.
( 
  cd ${WEB_BASEPATH}/CDS/files
  echo "### Creating genmetaByID.json, genmetaByFile.json" >> ${logfile}
  ${ILDPROD}/gensamples/genstart/make_json.py -v 1 -d ${WEB_BASEPATH}/CDS/mc-dbd.log/generated/metacopy >> ${logfile}
  echo "### Completed creating genmetaByID.json and genmetaByFile.json" >> ${logfile}
  /usr/bin/jq -r .[].process_names genmetaByID.json | /bin/sort | /usr/bin/uniq > process_names.txt
  /usr/bin/jq -r .[].process_type genmetaByID.json | /bin/sort | /usr/bin/uniq > process_type.txt
  echo "### List of process_names and process_types are created" >> ${logfile} 
  chmod g+w genmetaByFile.json
  chmod g+w genmetaByID.json
  chmod g+w process_names.txt
  chmod g+w process_type.txt
  # ( 

) 
fi

if [ "${domail}" == "YES" ] ; then 
# Send message to MAIL_RECIPIENTS
mailtext=${logdir}/gensync-mail-${dstr}.txt
echo "To:${MAIL_RECIPIENTS}" > ${mailtext}
echo "Subject:ild-ngt syncdesy log: `date  +%Y%m%d-%H%M%S`" >> ${mailtext}
echo "Mime-Version: 1.0" >> ${mailtext}
echo "Content-Type: text" >> ${mailtext}
cat ${logfile} >> ${mailtext}

cat ${mailtext} | /usr/lib/sendmail -t

fi
