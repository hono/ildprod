#!/bin/env python 


from GenmetaUtils import *

import os
import glob
import pprint
import json
import argparse

# GENMETA_DIR=""
#_metaByID = {}
#_metaByFile = {}
_print_level=0


# ==============================================================
if __name__ == "__main__":

  parser = argparse.ArgumentParser(description="Create genmetaByID.json and genmetaByFile.json")
  # GENMETA_DIR="/group/ilc/users/miyamoto/optprod/json/mc-dbd.log.genmeta"
  # GENMETA_DIR="/home/miyamoto/CDS/mc-dbd.log/generated/metacopy/"
  parser.add_argument("-d",help="Directory  of generator meta files",dest="genmeta_dir", action="store",
                      default="/group/ilc/users/miyamoto/optprod/json/mc-dbd.log.genmeta")
  parser.add_argument("-v",help="Verversity level(0=no print;1=warning;2=message",dest="print_level",action="store",
                      default="0")
  
  args = parser.parse_args()
  GENMETA_DIR=args.genmeta_dir
  _print_level=int(args.print_level)
  
  metajson = make_clean_genmeta(GENMETA_DIR)

  json.dump(metajson["byid"], open("genmetaByID.json","w"))
  json.dump(metajson["byfile"], open("genmetaByFile.json", "w"))

