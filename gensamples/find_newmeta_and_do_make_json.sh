#!/bin/bash 

metadir=/group/ilc/users/miyamoto/optprod/json/mc-dbd.log.genmeta

dirac-dms-find-lfns Path=/ilc/prod/ilc/mc-dbd.log/generated "CreationDate>20180401" | grep "\.txt" > newmeta.list 

nummeta=`cat newmeta.list | wc -l`

if [ "x${nummeta}" != "x0" ] ; then 
   rm -rf metadir
   mkdir -p metadir
   ( cd metadir
     dirac-dms-get-file ../newmeta.list 
   ) 
fi

( 
cd metadir
for f in *.txt ; do 
  chmod -x ${f}

  if [ -e ${metadir}/newMeta/$f ] ; then 
     echo "$f exists already and skipped."
  fi
  cp -v $f ${metadir}/newMeta
done
)

./make_json.py 

echo "New genmeta*.json was created."
echo "Copy them to ${metadir} after makeing a proper backup."


