#!/bin/bash 


PROGNAME=$(basename $0)
usage() {
    echo "Usage: ${PROGNAME} [options] date_since"
    echo "   This script downloads meta file registered in DIRAC."
    echo "   Output directory is created if not exist, otherewise data is over-written."
    echo 
    echo "date_newer : Meta files newer than date_since are downloaded."
    echo "             format: YYYYMMDD. Should be the last arguments. " 
    echo "[Options:]"
    echo "  -d : Output directory. Default=meta. "
    echo 
}

date_newer=""
outdir="meta"

for opt in "$@" ; do 
  case "${opt}" in 
    "-h") usage ; exit ;;
    "-d") shift ; outdir=$1 ;;
    *) date_newer=$opt ;;
  esac
  shift
done

if [ "x${date_newer}" == "x" ] ; then 
  usage
  exit
fi


echo "Meta data newer than ${date_newer} is written to ${outdir}"
date -d ${date_newer} > /dev/null
if [ $? -ne 0 ] ; then
  echo "date_newer is not a correct date format."
  exit
fi


metadir=/group/ilc/users/miyamoto/optprod/json/mc-dbd.log.genmeta

dirac-dms-find-lfns Path=/ilc/prod/ilc/mc-dbd.log/generated "CreationDate>${date_newer}" | grep "\.txt" > newmeta.list 


nummeta=`cat newmeta.list | wc -l`

if [ "x${nummeta}" != "x0" ] ; then 

   echo "${nummeta} meta files are being downloaded ${outdir}."
   mkdir -pv ${outdir}
   ( cd ${outdir}
     dirac-dms-get-file ../newmeta.list 
   ) 
fi

