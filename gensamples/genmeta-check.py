#!/usr/bin/env python 
# Add a record to elog genmeta
#
# Warning 
# #Events and xsect-error fields are not filled. Not know why.
#
# 8-March-2018 A.Miyamoto
#

from GenmetaUtils import * 

import os
import elog
import argparse


###################################################################
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Check genmeta file.")
    parser.add_argument("meta", help="generator meta file.")
    parser.add_argument("-w", help="No Dry run, WRITE to elog", dest="doWrite", action="store_true", default=False)
    parser.add_argument("-s", help="Silent mode. Do not dump elog data before upload to ELOG", dest="silent", action="store_true", default=False)

    args = parser.parse_args()   
    metafile = args.meta
    doWrite = args.doWrite
    silent = args.silent

    print( "## Checking metafile "+metafile )

    ret = getAttributes(metafile)
    if not ret["OK"]:
        print( pyc.c["red"] + "Failed" + pyc.c["end"] + " to readin metafile." )
        exit(-1)

    attrib = ret["Attribute"]
    genmeta = ret["genmeta"]
    msgtext = ret["message"]
 
    # if not silent:
    #   pprint.pprint(genmeta)


    check_genmeta(genmeta)

    if doWrite:  
    
        lines = open(os.environ[PASSINFO]).readlines()
        (auser, apass) = lines[0].replace("\n","").split(' ')
    
        logbook = elog.open(LOGBOOK, user=auser, password=apass)
        newid = logbook.post(msgtext, attributes = attrib, encoding="HTML")

        print( metafile + " was added to " + pyc.c["cyan"] + " elog with ID=" + str(newid) + pyc.c["end"] )
   
    exit(0)

