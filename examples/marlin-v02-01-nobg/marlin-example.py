# A test for multiple-overlay files.


from DIRAC.Core.Base import Script
Script.parseCommandLine()
from ILCDIRAC.Interfaces.API.NewInterface.UserJob import UserJob
from ILCDIRAC.Interfaces.API.NewInterface.Applications import Marlin, OverlayInput
from ILCDIRAC.Interfaces.API.DiracILC import DiracILC
import os
from datetime import datetime


localjob = False

now = datetime.now()
simfile="lfn:/ilc/user/a/amiyamot/testjob/sv02-01.mILD_l5_v05.E1-calib.I1110082.n1.dsim_20200318-113709.slcio"
outdst = "rv02-01-sv02-01.mILD_l5_o1_v02_nobg.E1-calib.I1110082.n1.ddst_%s.slcio" % now.strftime("%Y%m%d-%H%M%S")
outrec = "rv02-01-sv02-01.mILD_l5_o1_v02_nobg.E1-calib.I1110082.n1.drec_%s.slcio" % now.strftime("%Y%m%d-%H%M%S")
outdir = "testjob"
detectorModel = "ILD_l5_o1_v02"

banned_sites = ["OSG.BNL.us", "LCG.UKI-NORTHGRID-LIV-HEP.uk", "OSG.UCSDT2.us",
                "LCG.SCOTGRIDDURHAM.uk", "LCG.NIKHEF.nl",
                "LCG.UKI-SOUTHGRID-RALPP.uk", "LCG.GRIF.fr", "LCG.Manchester.uk",
                "LCG.UKI-LT2-IC-HEP.uk", "LCG.Weizmann.il"]



d= DiracILC(True,"repo.rep")

j = UserJob()
j.setJobGroup("Tutorial")
j.setName("MarlinExample")
# j.setInputData(simfile)
# j.setInputSandbox([ildconfig_tgz])
# j.setInputSandbox(simfile)
j.setILDConfig("v02-01")
j.setBannedSites(banned_sites)
j.setOutputSandbox(["*.log","MarlinStdReco.xml","MarlinStdRecoParsed.xml","marlin*.xml", "*.sh"])
j.dontPromptMe()

ma = Marlin()
ma.setDebug(True)
ma.setVersion("ILCSoft-02-01_gcc82")
ma.setDetectorModel(detectorModel)
ma.setSteeringFile("MarlinStdReco.xml")
extracli = "--constant.DetectorModel=%s --global.MaxRecordNumber=10 " % detectorModel

ma.setExtraCLIArguments(extracli)

#simfile4xml=os.path.basename(simfile.replace("LFN:","").replace("lfn:/","/"))
#print "simfile4xml="+simfile4xml
# ma.setInputFile(simfile4xml)
ma.setInputFile(simfile)
ma.setOutputDstFile(outdst)
ma.setOutputRecFile(outrec)


res = j.append(ma)
if not res['OK']:
    print( res['Message'] )
    exit(1)
  
# j.submit(d, mode='local')
# j.submit(d)

if localjob :
  j.submit(d, mode="local")

else:
  simdir="testjob"
  j.setOutputData([outdst, outrec],outdir,"KEK-SRM")
  res=j.submit(d)
  if res['OK']:
    print( "Dirac job, "+str(res["Value"])+", was submitted." )
  else:
    print( "Failed to submit dirac job. " )
    print( res )


