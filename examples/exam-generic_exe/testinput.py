from DIRAC.Core.Base import Script
Script.parseCommandLine()
from ILCDIRAC.Interfaces.API.NewInterface.UserJob import UserJob
from ILCDIRAC.Interfaces.API.NewInterface.Applications import Marlin
from ILCDIRAC.Interfaces.API.NewInterface.Applications import GenericApplication
from ILCDIRAC.Interfaces.API.DiracILC import DiracILC


# KEK-Disk
infile="/ilc/user/a/amiyamot/testjob/sv02-02.mILD_l5_v05.E1-calib.I1110082.n1.dsim_20200910-110948.slcio"
# KEK-SRM
infile="/ilc/user/a/amiyamot/testjob/rv02-01-sv02-01.mILD_l5_o1_v02_nobg.E1-calib.I1110082.n1.ddst_20200318-134900.slcio"
infile="/ilc/user/a/amiyamot/testjob/sv01-14-01-p00.mILD_o1_v05.E350-TDR_ws.I37906.Pea_eee.eL.pB-00003.slcio"

d= DiracILC(True,"repo.rep")

j = UserJob()
j.setJobGroup("MyTest")
j.setName("Tape SandBox")#%i)
#j.setInputData([infile])
#j.setInputSandbox(["myscript.sh"])
j.setInputSandbox(["myscript.sh", "lfn:"+infile])
j.setCPUTime(5)

################################################
appre = GenericApplication()
appre.setScript("myscript.sh")
res=j.append(appre)
if not res['OK']:
  print( res['Message'] )
  exit(1)

j.setOutputSandbox(["*.log","*.sh"])
j.dontPromptMe()
#j.submit(d, mode="local")
j.submit(d)

