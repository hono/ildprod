#!/bin/env python

### #!/bin/env python3 

import elog
import os
import urllib3
import argparse

if __name__ == "__main__":


  parser = argparse.ArgumentParser(description="Delete a record from elog worklog")
  parser.add_argument("elogid",help="elogid to be deleted.")

  args = parser.parse_args()
  elogid = int(args.elogid)

  print("Are you sure to remove elogid %d of elog worklog ?" % elogid) 
  ans = input("Enter y or Y to delete : ").upper()
  if ans != "Y":
     exit(0)

  urllib3.disable_warnings()
  print("HTTPS SSL Warnings for the certificate verification is DISBALED.")
  print("See https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings InsecureRequestWarning")

  print("elogid %d will be removed." % elogid )

  my_elog_key_file=os.environ["MY_ELOG_KEY"]
  lines = open(my_elog_key_file).readlines()
  # lines = open(os.environ["MY_PASSWD_FILE"]).readlines()
  auser, apass = lines[0][:-1].split(' ') 
  
  logbook = elog.open("https://ild.ngt.ndu.ac.jp/elog/worklog/", user=auser, password=apass)

  logbook.delete(elogid)
  print("elog "+str(elogid)+" was deleted.")

