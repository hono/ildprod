#!/usr/bin/env python

from urllib.request import urlopen
import json
import pprint

KNOWN_EVTTYPE_URL = "https://ild.ngt.ndu.ac.jp/mc-prod/files2017/genmeta/known-evttype.txt"
GENMETA_URL="https://ild.ngt.ndu.ac.jp/CDS/files/genmetaByID.json"


# with urlopen(KNOWN_EVTTYPE_URL) as res:
#   body = res.read().decode("utf-8")
#   print(body)

#uread = urlopen(KNOWN_EVTTYPE_URL)
#for bline in uread.readlines():
#    line = bline.decode("utf-8")
#    print(line)

#with urlopen(KNOWN_EVTTYPE_URL) as res:
#    for bline in res.readlines():
#        line = bline.decode("utf-8")
#        print(line)

#res = urlopen(KNOWN_EVTTYPE_URL).readlines()
#for line in res:
#   print(line.decode("utf-8"))

# for line in urlopen(GENMETA_URL).readlines():
#   print(line.decode("utf-8").replace("\n",""))

with urlopen(GENMETA_URL) as res:
    body = res.read().decode("utf-8")
    jread = json.loads(body)

json.dump(jread, open("readweb.json","w"))
print("Got a file from " + GENMETA_URL)
# pprint.pprint(jread)


