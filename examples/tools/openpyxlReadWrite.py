# -*- coding: utf-8 -*-

from openpyxl import Workbook
from openpyxl import load_workbook
from openpyxl.styles import Font, Color, colors

import os

if not os.path.exists("empty_book.xlsx"):
   print("empty_book.xlsx does not exist.")
   print("empty_book.xlsx is created by \"python openpyxlWrite.py\".")
   exit()


wb = load_workbook("empty_book.xlsx")
print("Read empty_book.xlsx")

ws1 = wb["Data"]

ws1.cell(row=4, column=5).value = "Hello world"

bold18 = Font(size=18, bold=True, color="ff0000")

ws2 = wb.create_sheet(title="Sheet_added")
ws2.cell(row=3, column=1).value = "This sheet was added."
ws2.cell(row=3, column=1).font = bold18


ws2.cell(row=4, column=1).value = u"日本語"
ws2.cell(row=4, column=1).font = bold18


wb.save("empty_book_new.xlsx")
print("Write empty_book_new.xlsx")

