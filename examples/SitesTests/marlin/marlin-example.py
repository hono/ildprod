# 

import os
from datetime import datetime
from DIRAC.Core.Base import Script
from DIRAC import gLogger, S_OK, S_ERROR

# ######################################################
class ArgParse(object):
    ''' Parameter class '''
    def __init__(self):
       self.dest = ""
       self.localjob = True

    def setDestination(self, opt):
       self.dest = opt
       return S_OK()

    def setSubmit(self, dummy_opt):
       self.localjob = False
       return S_OK()

    def registerSwitches(self):
       Script.registerSwitch('','nodry','No dry run. Submit job',self.setSubmit )
       Script.registerSwitch('S:','dest=', 'Job destination', self.setDestination )

       msg = '\n'.join([ '%s [options] ' % Script.scriptName,
                         'Function: Submit ddsim job to DIRAC'])
       Script.setUsageMessage(msg)

# #######################################################

parser = ArgParse()
parser.registerSwitches()
Script.parseCommandLine()

# #######################################################

dest = parser.dest
localjob = parser.localjob

# print(" dest="+dest)
# print(" localjob="+str(localjob))

from ILCDIRAC.Interfaces.API.NewInterface.UserJob import UserJob
from ILCDIRAC.Interfaces.API.NewInterface.Applications import Marlin, OverlayInput
from ILCDIRAC.Interfaces.API.DiracILC import DiracILC


# localjob = False

now = datetime.now()
simfile="/ilc/user/a/amiyamot/testjob/sv02-02.mILD_l5_v05.E1-calib.I1110082.n1.dsim_20200910-111134.slcio"
outdst = "rv02-02.sv02-02.mILD_l5_o1_v02_nobg.E1-calib.I1110082.n1.ddst_%s.slcio" % now.strftime("%Y%m%d-%H%M%S")
outrec = "rv02-02.sv02-02.mILD_l5_o1_v02_nobg.E1-calib.I1110082.n1.drec_%s.slcio" % now.strftime("%Y%m%d-%H%M%S")
outdir = "testjob"
detectorModel = "ILD_l5_o1_v02"

# banned_sites = ["OSG.BNL.us", "LCG.UKI-NORTHGRID-LIV-HEP.uk", "OSG.UCSDT2.us",
#                 "LCG.SCOTGRIDDURHAM.uk", "LCG.NIKHEF.nl",
#                 "LCG.UKI-SOUTHGRID-RALPP.uk", "LCG.GRIF.fr", "LCG.Manchester.uk",
#                 "LCG.UKI-LT2-IC-HEP.uk", "LCG.Weizmann.il"]
banned_sites = ""
destination = dest.split(',') if ',' in dest else dest
print( "destination=" + str(destination) )

d= DiracILC(True,"repo.rep")

j = UserJob()
j.setJobGroup("testsite-marlin")
j.setName("MarlinExample")
j.setInputData(simfile)
# j.setInputSandbox([ildconfig_tgz])
# j.setInputSandbox(simfile)
j.setILDConfig("v02-02")
j.setBannedSites(banned_sites)
j.setDestination(destination)
j.setOutputSandbox(["*.log","MarlinStdReco.xml","MarlinStdRecoParsed.xml","marlin*.xml", "*.sh"])
j.dontPromptMe()

ma = Marlin()
ma.setDebug(True)
ma.setVersion("ILCSoft-02-02_cc7")
ma.setDetectorModel(detectorModel)
ma.setSteeringFile("MarlinStdReco.xml")
extracli = "--constant.DetectorModel=%s --global.MaxRecordNumber=10 " % detectorModel

ma.setExtraCLIArguments(extracli)

#simfile4xml=os.path.basename(simfile.replace("LFN:","").replace("lfn:/","/"))
#print "simfile4xml="+simfile4xml
# ma.setInputFile(simfile4xml)
ma.setInputFile(simfile)
ma.setOutputDstFile(outdst)
ma.setOutputRecFile(outrec)


res = j.append(ma)
if not res['OK']:
    print( res['Message'] )
    exit(1)
  
# j.submit(d, mode='local')
# j.submit(d)

if localjob :
  j.submit(d, mode="local")

else:
  simdir="testjob"
  # j.setOutputData([outdst, outrec],outdir,"KEK-SRM")
  res=j.submit(d)
  if res['OK']:
    print( "Dirac job, "+str(res["Value"])+", was submitted." )
  else:
    print( "Failed to submit dirac job. " )
    print( res )


