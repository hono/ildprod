# 
# A script to submit DDSIM jobs to many sites, for testing ddsim availability at each sites.
# An example command to do 

cat site_ddsimall.list | while read f ; do ( python ddsim-example.py -S $f --nodry ) ; done 2>&1 | tee subddsim.log
