#!/bin/bash
#
# A script for step4 tasks, submit production
# (Rec+Dst production)

selected_file_recdst=2
selected_file_dstonly=3

stfile="status.log"
if [ -e ../3_gensplit ] ; then 
  setselectedfile.sh --selected_file_recdst ${selected_file_recdst} \
                   --selected_file_dstonly ${selected_file_dstonly} >> ${stfile} 2>&1 &
fi
