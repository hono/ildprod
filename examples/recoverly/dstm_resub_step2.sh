#!/bin/sh
#---------------------------------------------------------------
## Usage : dstm_resub.sh <jobNames> (jobName are obtained from DIRAC job monitor)
##
## Usage   : subdstm.py <prodid> <processID> <fileno> <jobno> 
## Function: Submit one DST merge job.
##  
## <prodid> : Production ID
## <processID> : Process ID
## <fileno> : file number in string (incl. 0)
## <jobno>  : job number
#---------------------------------------------------------------

. ${MYPROD_USERPROD}

##logfile="dstm_resub_step2_submit.log"
logfile="dstm-step2.log"
# dstmjobs-16171-ILD_l5_o1_v02/dstm.I405018.n003.j19
if [ -f ${logfile} ]; then
  cp ${logfile} ${logfile}.`date +%Y%m%d%s`
fi

for dir in `/bin/ls -d dstmjobs-*/dstm.I*`; do
    if [ -f ${dir}/submit.json ]; then
	echo "*** ${dir}/submit.json exist! Skip this event!" 2>&1 | tee -a ${logfile}
    else
	prodID=`echo ${dir} | cut -d '-' -f 2`
	processID=`echo ${dir} | cut -d '/' -f 2 | cut -d '.' -f 2 | sed s/I//g`
	fileno=`echo ${dir} | cut -d '/' -f 2 | cut -d '.' -f 3 | sed s/n//g`
	jobno=`echo ${dir} | cut -d '/' -f 2 | cut -d '.' -f 4 | sed s/j//g`
	#echo "prodID=${prodID} processID=${processID} fileno=${fileno} jobno=${jobno}"
        echo "subdstm.py ${prodID} ${processID} ${fileno} ${jobno} 2>&1 | tee -a ${logfile}"
        subdstm.py ${prodID} ${processID} ${fileno} ${jobno} 2>&1 | tee -a ${logfile}
    fi
done

nfailed=`cat ${logfile} | grep Failed -B2  | grep job_group | wc -l`

if [ -n "${nfailed}" ]; then
  if [ ${nfailed} -gt 0 ]; then
    echo "${nfailed} job submission have failed and dstm_resub_step2_failed.list is created..."
    cat ${logfile} | grep "Failed" -B2 | grep "job_group" > dstm_resub_step2_failed.list
  fi
fi
