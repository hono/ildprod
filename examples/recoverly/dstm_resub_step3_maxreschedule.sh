#!/bin/sh
#---------------------------------------------------------------
## Usage : dstm_resub.sh <jobNames> (jobName are obtained from DIRAC job monitor)
##
## Usage   : subdstm.py <prodid> <processID> <fileno> <jobno> 
## Function: Submit one DST merge job.
##  
## <prodid> : Production ID
## <processID> : Process ID
## <fileno> : file number in string (incl. 0)
## <jobno>  : job number
#---------------------------------------------------------------

. ${MYPROD_USERPROD}

for jobGroup in `cat jobgroup.list`; do

    prodID=`echo ${jobGroup} | sed s/ILD-DSTM-prod//g`
    echo "Checking failed jobs in jobGroup ${jobGroup} with Maximum of reschedule..."
    dirac-wms-job-status -g ${jobGroup} | grep "Failed" | grep "Maximum" | cut -d ' ' -f 1 | cut -d '=' -f 2 2>&1 | tee ${prodID}_maxfailed.list
    echo "${prodID}_maxfailed.list is created."

    for failedjob in `cat ${prodID}_maxfailed.list`; do
        jsonFile=`grep -ri ${failedjob} dstmjobs-${prodID}-ILD_l5_o1_v02/dstm.I*/ | cut -d ':' -f 1 `
        JobName=`cat ${jsonFile}| jq -r ".submit.JobName"`
        JobGroup=`cat ${jsonFile}| jq -r ".submit.JobGroup"`
        jobid=`cat ${jsonFile}| jq -r ".submit.jobid"`
        processID=`echo ${JobName} | cut -d '-' -f 2 | sed s/I//g`
        fileno=`echo ${JobName} | cut -d '-' -f 3 | sed s/n//g`
        jobno=`echo ${JobName} | cut -d '-' -f 4 | sed s/j//g`
        
        if [ ${failedjob} -ne ${jobid} ]; then
	   echo "Something wrong for jobID ${jobid} and Failedjob ${failedjob}"
        else
           if [ ! -d resubmit ]; then mkdir -p resubmit; fi
	   echo "Backup ${jsonFile} to resubmit/${prodID}_${JobName}_${failedjob}_submit.json"
           cp ${jsonFile} resubmit/${prodID}_${JobName}_${failedjob}_submit.json
           echo "Failed jobID=${jobid} : prodID=${prodID} processID=${processID} fileno=${fileno} jobno=${jobno}"
           echo "subdstm.py ${prodID} ${processID} ${fileno} ${jobno}"
           subdstm.py ${prodID} ${processID} ${fileno} ${jobno}
        fi
    done 
done
